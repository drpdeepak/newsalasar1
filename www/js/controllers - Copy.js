angular.module("salasar.controllers", [])



// TODO: indexCtrl --|-- 
.controller("indexCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	// TODO: indexCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "-" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: indexCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: indexCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: indexCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: indexCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: indexCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: indexCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// code 

	// TODO: indexCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
			
		} catch(e){
			console.log("%cerror: %cPage: `index` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: side_menusCtrl --|-- 
.controller("side_menusCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: side_menusCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "-" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: side_menusCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: side_menusCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: side_menusCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: side_menusCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: side_menusCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: side_menusCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// code 
	
	var popover_template = "";
	popover_template += "<ion-popover-view class=\"fit\">";
	popover_template += "	<ion-content>";
	popover_template += "		<ion-list>";
	popover_template += "			<a  class=\"item dark-ink\" ng-href=\"#/salasar/about_us\" ng-click=\"popover.hide()\">";
	popover_template += "			About Us";
	popover_template += "			</a>";
	popover_template += "			<a  class=\"item dark-ink\" ng-href=\"#/salasar/history\" ng-click=\"popover.hide()\">";
	popover_template += "			History";
	popover_template += "			</a>";
	popover_template += "			<a  class=\"item dark-ink\" ng-href=\"#/salasar/privacy_policy\" ng-click=\"popover.hide()\">";
	popover_template += "			Privacy Policy";
	popover_template += "			</a>";
	popover_template += "			<a  class=\"item dark-ink\" ng-href=\"#/salasar/contact_us\" ng-click=\"popover.hide()\">";
	popover_template += "			Contact Us";
	popover_template += "			</a>";
	popover_template += "		</ion-list>";
	popover_template += "	</ion-content>";
	popover_template += "</ion-popover-view>";
	
	
	$scope.popover = $ionicPopover.fromTemplate(popover_template,{
		scope: $scope
	});
	
	$scope.closePopover = function(){
		$scope.popover.hide();
	};
	
	$scope.$on("$destroy", function(){
		$scope.popover.remove();
	});

	// TODO: side_menusCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
			
		} catch(e){
			console.log("%cerror: %cPage: `side_menus` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: about_usCtrl --|-- 
.controller("about_usCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: about_usCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "menu" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: about_usCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: about_usCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: about_usCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: about_usCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: about_usCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: about_usCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// code 

	// TODO: about_usCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
			
		} catch(e){
			console.log("%cerror: %cPage: `about_us` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: albumsCtrl --|-- 
.controller("albumsCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: albumsCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "table (albums)" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: albumsCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: albumsCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: albumsCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: albumsCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: albumsCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: albumsCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// TODO: albumsCtrl --|-- $scope.showAuthentication
	$scope.showAuthentication  = function(){
		var authPopup = $ionicPopup.show({
			template: ' This page required login',
			title: "Authorization",
			subTitle: "Authorization is required",
			scope: $scope,
			buttons: [
				{text:"Cancel",onTap: function(e){
					$state.go("salasar.home");
				}},
			],
		}).then(function(form){
		},function(err){
		},function(msg){
		});
	};
	
	// set default parameter http
	var http_params = {};
	
	// set HTTP Header 
	var http_header = {
		headers: {
		},
		params: http_params
	};
	var targetQuery = ""; //default param
	var raplaceWithQuery = "";
	//fix url Albums
	targetQuery = "json=albums"; //default param
	raplaceWithQuery = "json=albums";
	
	
	// TODO: albumsCtrl --|-- $scope.splitArray
	$scope.splitArray = function(items,cols,maxItem) {
		var newItems = [];
		if(maxItem == 0){
			maxItem = items.length;
		}
		if(items){
			for (var i=0; i < maxItem; i+=cols) {
				newItems.push(items.slice(i, i+cols));
			}
		}
		return newItems;
	}
	$scope.gmapOptions = {options: { scrollwheel: false }};
	
	var fetch_per_scroll = 1;
	// animation loading 
	$ionicLoading.show();
	
	
	// TODO: albumsCtrl --|-- $scope.fetchURL
	$scope.fetchURL = "https://salasarbalaji.000webhostapp.com/rest-api.php?json=albums";
	// TODO: albumsCtrl --|-- $scope.fetchURLp
	$scope.fetchURLp = "https://salasarbalaji.000webhostapp.com/rest-api.php?json=albums&callback=JSON_CALLBACK";
	// TODO: albumsCtrl --|-- $scope.hashURL
	$scope.hashURL = md5.createHash( $scope.fetchURL.replace(targetQuery,raplaceWithQuery));
	
	
	$scope.noMoreItemsAvailable = false; //readmore status
	var lastPush = 0;
	var data_albumss = [];
	
	localforage.getItem("data_albumss_" + $scope.hashURL, function(err, get_albumss){
		if(get_albumss === null){
			data_albumss =[];
		}else{
			data_albumss = JSON.parse(get_albumss);
			$scope.data_albumss =JSON.parse( get_albumss);
			$timeout(function() {
				$ionicLoading.hide();
				if(data_albumss){
					$scope.chunked_albumss = $scope.splitArray(data_albumss, $rootScope.grid128,0);
				}
				controller_by_user();
			},200);
		}
	}).then(function(value){
	}).catch(function (err){
	})
	if(data_albumss === null ){
		data_albumss =[];
	}
	if(data_albumss.length === 0 ){
		$timeout(function() {
			var url_request = $scope.fetchURL.replace(targetQuery,raplaceWithQuery);
			// overwrite HTTP Header 
			http_header = {
				headers: {
				},
				params: http_params
			};
			// TODO: albumsCtrl --|-- $http.get
			console.log("%cRetrieving JSON: %c" + url_request,"color:blue;font-size:18px","color:red;font-size:18px");
			$http.get(url_request,http_header).then(function(response) {
				data_albumss = response.data;
				console.log("%cSuccessfully","color:blue;font-size:18px");
				console.dir(data_albumss);
				$scope.data_albumss = response.data;
				// TODO: albumsCtrl --|---------- set:localforage
				localforage.setItem("data_albumss_" + $scope.hashURL, JSON.stringify(data_albumss));
			},function(response) {
			
				$timeout(function() {
					var url_request = $scope.fetchURLp.replace(targetQuery,raplaceWithQuery);
					// overwrite HTTP Header 
					http_header = {
						headers: {
						},
						params: http_params
					};
					console.log("%cRetrieving again: %c" + url_request,"color:blue;font-size:18px","color:red;font-size:18px");
					// TODO: albumsCtrl --|------ $http.jsonp
					$http.jsonp(url_request,http_header).success(function(data){
						data_albumss = data;
						$scope.data_albumss = data;
						$ionicLoading.hide();
						// TODO: albumsCtrl --|---------- set:localforage
						localforage.setItem("data_albumss_" + $scope.hashURL,JSON.stringify(data_albumss));
						controller_by_user();
					}).error(function(data){
					if(response.status ===401){
						// TODO: albumsCtrl --|------------ error:Unauthorized
						$scope.showAuthentication();
					}else{
						// TODO: albumsCtrl --|------------ error:Message
						var data = { statusText:response.statusText, status:response.status };
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
						$timeout(function() {
							alertPopup.close();
						}, 2000);
					}
					});
				}, 200);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
				$scope.chunked_albumss = $scope.splitArray(data_albumss, $rootScope.grid128,0);
			}, 200);
		});
	
		}, 200);
	}
	
	
	// TODO: albumsCtrl --|-- $scope.doRefresh
	$scope.doRefresh = function(){
		var url_request = $scope.fetchURL.replace(targetQuery,raplaceWithQuery);
		// retry retrieving data
		// overwrite http_header 
		http_header = {
			headers: {
			},
			params: http_params
		};
		// TODO: albumsCtrl --|------ $http.get
		$http.get(url_request,http_header).then(function(response) {
			data_albumss = response.data;
			$scope.data_albumss = response.data;
			// TODO: albumsCtrl --|---------- set:localforage
			localforage.setItem("data_albumss_" + $scope.hashURL,JSON.stringify(data_albumss));
		},function(response){
			
		// retrieving data with jsonp
			$timeout(function() {
			var url_request =$scope.fetchURLp.replace(targetQuery,raplaceWithQuery);
				// overwrite http_header 
				http_header = {
					headers: {
					},
					params: http_params
				};
				// TODO: albumsCtrl --|---------- $http.jsonp
				$http.jsonp(url_request,http_header).success(function(data){
					data_albumss = data;
					$scope.data_albumss = data;
					$ionicLoading.hide();
					controller_by_user();
					// TODO: albumsCtrl --|---------- set:localforage
					localforage.setItem("data_albumss_"+ $scope.hashURL,JSON.stringify(data_albumss));
				}).error(function(resp){
					if(response.status ===401){
					// TODO: albumsCtrl --|------------ error:Unauthorized
					$scope.showAuthentication();
					}else{
						// TODO: albumsCtrl --|------------ error:Message
						var data = { statusText:response.statusText, status:response.status };
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
					};
				});
			}, 200);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
				if(data_albumss){
					$scope.chunked_albumss = $scope.splitArray(data_albumss, $rootScope.grid128,0);
				}
			}, 500);
		});
	
	};
	// code 

	// TODO: albumsCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
//debug: all data
//console.log(data_albumss);
$ionicConfig.backButton.text("");
			
		} catch(e){
			console.log("%cerror: %cPage: `albums` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: book_servicesCtrl --|-- 
.controller("book_servicesCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: book_servicesCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "menu" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: book_servicesCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: book_servicesCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: book_servicesCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: book_servicesCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: book_servicesCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: book_servicesCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// code 

	// TODO: book_servicesCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
			
		} catch(e){
			console.log("%cerror: %cPage: `book_services` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: brandsCtrl --|-- 
.controller("brandsCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: brandsCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "menu" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: brandsCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: brandsCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: brandsCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: brandsCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: brandsCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: brandsCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// code 

	// TODO: brandsCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
			
		} catch(e){
			console.log("%cerror: %cPage: `brands` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: cabsCtrl --|-- 
.controller("cabsCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: cabsCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "page_builder" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: cabsCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: cabsCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: cabsCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: cabsCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: cabsCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: cabsCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// code 

	// TODO: cabsCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
			
		} catch(e){
			console.log("%cerror: %cPage: `cabs` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: cabs_cartCtrl --|-- 
.controller("cabs_cartCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: cabs_cartCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "table (cabs)" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: cabs_cartCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: cabs_cartCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: cabs_cartCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: cabs_cartCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: cabs_cartCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: cabs_cartCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// TODO: cabs_cartCtrl --|-- Database for Cart/Bookmark
	// TODO: cabs_cartCtrl --|-- $scope.loadDbVirtualCabs
	 
	$scope.loadDbVirtualCabs = function(){
		$ionicLoading.show();
		//cabs_cart
		$scope.cabs_cart = []; 
		$scope.cabs_cost = 0;
		localforage.getItem("cabs_cart", function(err,dbVirtual){
			if(dbVirtual === null){
				$scope.cabs_cart = []; 
			}else{
				try{
					$scope.cabs_cart = JSON.parse(dbVirtual); 
		var total_cost = 0;
		angular.forEach($scope.cabs_cart, function(item, key) {
			var cost = item._qty * item.none;
			total_cost += cost;
		});
		$scope.cabs_cost = total_cost;
				}catch (e){
					$scope.cabs_cart = []; 
				}
			}
		}).then(function(value){
			$timeout(function(){
				$ionicLoading.hide();
			},200);
		}).catch(function(err){
			console.log(err);
			$timeout(function(){
				$ionicLoading.hide();
			},200);
		});
	};
	$scope.$on("$ionicView.enter", function (){
		$scope.loadDbVirtualCabs();
	});
	// TODO: cabs_cartCtrl --|-- $scope.clearDbVirtualCabs
	$scope.clearDbVirtualCabs = function(){
		localforage.setItem("cabs_cart",[]);
		$timeout(function(){
			$scope.loadDbVirtualCabs();
		},200);
	};
	// TODO: cabs_cartCtrl --|-- $scope.removeDbVirtualCabs
	$scope.removeDbVirtualCabs = function(itemId){
		var virtual_items = [];
		var last_items = $scope.cabs_cart;
		for(var z=0;z<last_items.length;z++){
			if(itemId!==last_items[z].id){
				virtual_items.push(last_items[z]);
			}
		}
		localforage.setItem("cabs_cart",JSON.stringify(virtual_items));
		$timeout(function(){
			$scope.loadDbVirtualCabs();
		},200);
	};
	// TODO: cabs_cartCtrl --|-- $scope.updateDbVirtual();
	$scope.updateDbVirtual = function(){
		var virtual_items = [];
		var total_cost = 0;
		angular.forEach($scope.cabs_cart, function(item, key) {
			var cost = item._qty * item.none;
			item._sum = cost;
			this.push(item);
			total_cost += cost;
		},virtual_items);
		localforage.setItem("cabs_cart",JSON.stringify(virtual_items));
		$scope.cabs_cost = total_cost;
	};
	
	// TODO: cabs_cartCtrl --|-- Database for Cart/Bookmark
	// TODO: cabs_cartCtrl --|-- $scope.loadDbVirtualHotels
	 
	$scope.loadDbVirtualHotels = function(){
		$ionicLoading.show();
		//cabs_cart
		$scope.hotels_cart = []; 
		$scope.hotels_cost = 0;
		localforage.getItem("hotels_cart", function(err,dbVirtual){
			if(dbVirtual === null){
				$scope.hotels_cart = []; 
			}else{
				try{
					$scope.hotels_cart = JSON.parse(dbVirtual); 
		var total_cost = 0;
		angular.forEach($scope.hotels_cart, function(item, key) {
			var cost = item._qty * item.price;
			total_cost += cost;
		});
		$scope.hotels_cost = total_cost;
				}catch (e){
					$scope.hotels_cart = []; 
				}
			}
		}).then(function(value){
			$timeout(function(){
				$ionicLoading.hide();
			},200);
		}).catch(function(err){
			console.log(err);
			$timeout(function(){
				$ionicLoading.hide();
			},200);
		});
	};
	$scope.$on("$ionicView.enter", function (){
		$scope.loadDbVirtualHotels();
	});
	// TODO: cabs_cartCtrl --|-- $scope.clearDbVirtualHotels
	$scope.clearDbVirtualHotels = function(){
		localforage.setItem("hotels_cart",[]);
		$timeout(function(){
			$scope.loadDbVirtualHotels();
		},200);
	};
	// TODO: cabs_cartCtrl --|-- $scope.removeDbVirtualHotels
	$scope.removeDbVirtualHotels = function(itemId){
		var virtual_items = [];
		var last_items = $scope.hotels_cart;
		for(var z=0;z<last_items.length;z++){
			if(itemId!==last_items[z].id){
				virtual_items.push(last_items[z]);
			}
		}
		localforage.setItem("hotels_cart",JSON.stringify(virtual_items));
		$timeout(function(){
			$scope.loadDbVirtualHotels();
		},200);
	};
	// TODO: cabs_cartCtrl --|-- $scope.updateDbVirtual();
	$scope.updateDbVirtual = function(){
		var virtual_items = [];
		var total_cost = 0;
		angular.forEach($scope.hotels_cart, function(item, key) {
			var cost = item._qty * item.price;
			item._sum = cost;
			this.push(item);
			total_cost += cost;
		},virtual_items);
		localforage.setItem("hotels_cart",JSON.stringify(virtual_items));
		$scope.hotels_cost = total_cost;
	};
	
	// TODO: cabs_cartCtrl --|-- Database for Cart/Bookmark
	// TODO: cabs_cartCtrl --|-- $scope.loadDbVirtualProducts
	 
	$scope.loadDbVirtualProducts = function(){
		$ionicLoading.show();
		//cabs_cart
		$scope.products_cart = []; 
		$scope.products_cost = 0;
		localforage.getItem("products_cart", function(err,dbVirtual){
			if(dbVirtual === null){
				$scope.products_cart = []; 
			}else{
				try{
					$scope.products_cart = JSON.parse(dbVirtual); 
		var total_cost = 0;
		angular.forEach($scope.products_cart, function(item, key) {
			var cost = item._qty * item.price;
			total_cost += cost;
		});
		$scope.products_cost = total_cost;
				}catch (e){
					$scope.products_cart = []; 
				}
			}
		}).then(function(value){
			$timeout(function(){
				$ionicLoading.hide();
			},200);
		}).catch(function(err){
			console.log(err);
			$timeout(function(){
				$ionicLoading.hide();
			},200);
		});
	};
	$scope.$on("$ionicView.enter", function (){
		$scope.loadDbVirtualProducts();
	});
	// TODO: cabs_cartCtrl --|-- $scope.clearDbVirtualProducts
	$scope.clearDbVirtualProducts = function(){
		localforage.setItem("products_cart",[]);
		$timeout(function(){
			$scope.loadDbVirtualProducts();
		},200);
	};
	// TODO: cabs_cartCtrl --|-- $scope.removeDbVirtualProducts
	$scope.removeDbVirtualProducts = function(itemId){
		var virtual_items = [];
		var last_items = $scope.products_cart;
		for(var z=0;z<last_items.length;z++){
			if(itemId!==last_items[z].id){
				virtual_items.push(last_items[z]);
			}
		}
		localforage.setItem("products_cart",JSON.stringify(virtual_items));
		$timeout(function(){
			$scope.loadDbVirtualProducts();
		},200);
	};
	// TODO: cabs_cartCtrl --|-- $scope.updateDbVirtual();
	$scope.updateDbVirtual = function(){
		var virtual_items = [];
		var total_cost = 0;
		angular.forEach($scope.products_cart, function(item, key) {
			var cost = item._qty * item.price;
			item._sum = cost;
			this.push(item);
			total_cost += cost;
		},virtual_items);
		localforage.setItem("products_cart",JSON.stringify(virtual_items));
		$scope.products_cost = total_cost;
	};
	
	// code 

	// TODO: cabs_cartCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
	$scope.gotoCheckout = function(){
		alert("you must create payment gateway manual");
	}
			
		} catch(e){
			console.log("%cerror: %cPage: `cabs_cart` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: cabs_singlesCtrl --|-- 
.controller("cabs_singlesCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: cabs_singlesCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "table (cabs)" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: cabs_singlesCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: cabs_singlesCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: cabs_singlesCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: cabs_singlesCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: cabs_singlesCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: cabs_singlesCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// TODO: cabs_singlesCtrl --|-- $scope.showAuthentication
	$scope.showAuthentication  = function(){
		var authPopup = $ionicPopup.show({
			template: ' This page required login',
			title: "Authorization",
			subTitle: "Authorization is required",
			scope: $scope,
			buttons: [
				{text:"Cancel",onTap: function(e){
					$state.go("salasar.home");
				}},
			],
		}).then(function(form){
		},function(err){
		},function(msg){
		});
	};
	// TODO: cabs_singlesCtrl --|-- $scope.addToVirtual(); //data single
	$scope.addToDbVirtual = function(newItem){
		var is_already_exist = false ;
		// animation loading 
		$ionicLoading.show();
		var virtual_items = []; 
		localforage.getItem("cabs_cart", function(err,dbVirtual){
			if(dbVirtual === null){
				virtual_items = [];
			}else{
				try{
					var last_items = JSON.parse(dbVirtual); 
				}catch(e){
					var last_items = [];
				}
				for(var z=0;z<last_items.length;z++){
					virtual_items.push(last_items[z]);
					if(newItem.id ==  last_items[z].id){
						is_already_exist = true;
					}
				}
			}
		}).then(function(value){
			if(is_already_exist === false){
				newItem["_qty"]=1;
				newItem["_sum"]=newItem.none;
				virtual_items.push(newItem);
			}
			localforage.setItem("cabs_cart",JSON.stringify(virtual_items));
			$timeout(function(){
				$ionicLoading.hide();
			},200);
		}).catch(function(err){
			virtual_items = [];
			virtual_items.push(newItem);
			localforage.setItem("cabs_cart",JSON.stringify(virtual_items));
			$timeout(function(){
				$ionicLoading.hide();
			},200);
		})
	};
	
	
	// set default parameter http
	var http_params = {};

	// set HTTP Header 
	var http_header = {
		headers: {
		},
		params: http_params
	};
	// animation loading 
	$ionicLoading.show();
	
	// Retrieving data
	var itemID = $stateParams.id;
	// TODO: cabs_singlesCtrl --|-- $scope.fetchURL
	$scope.fetchURL = "https://salasarbalaji.000webhostapp.com/rest-api.php?json=cabs&id=" + itemID;
	// TODO: cabs_singlesCtrl --|-- $scope.fetchURLp
	$scope.fetchURLp = "https://salasarbalaji.000webhostapp.com/rest-api.php?json=cabs&id=" + itemID + "&callback=JSON_CALLBACK";
	// TODO: cabs_singlesCtrl --|-- $scope.hashURL
	$scope.hashURL = md5.createHash($scope.fetchURL);
	
	var current_item = [];
	localforage.getItem("data_cabs_single_" + $scope.hashURL, function(err, get_datas){
		if(get_datas === null){
			current_item = [];
		}else{
			if(get_datas !== null){
				current_item = JSON.parse(get_datas);
				$timeout(function(){
					$ionicLoading.hide();
					$scope.cabs = current_item ;
					controller_by_user();
				}, 500);
			};
		};
	}).then(function(value){
	}).catch(function (err){
	})
	if( current_item.length === 0 ){
		var itemID = $stateParams.id;
		var current_item = [];
	
		// set HTTP Header 
		http_header = {
			headers: {
			},
			params: http_params
		};
		// TODO: cabs_singlesCtrl --|-- $http.get
		$http.get($scope.fetchURL,http_header).then(function(response) {
			// Get data single
			var datas = response.data;
			// TODO: cabs_singlesCtrl --|---------- set:localforage
			localforage.setItem("data_cabs_single_" + $scope.hashURL,JSON.stringify(datas));
			current_item = datas ;
		},function(data) {
					// Error message
					var alertPopup = $ionicPopup.alert({
						title: "Network Error" + " (" + data.status + ")",
						template: "An error occurred while collecting data.",
					});
					$timeout(function() {
						alertPopup.close();
					}, 2000);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				$scope.cabs = current_item ;
				controller_by_user();
			}, 500);
		});
	}
	
	
		// TODO: cabs_singlesCtrl --|-- $scope.doRefresh
	$scope.doRefresh = function(){
		// Retrieving data
		var itemID = $stateParams.id;
		var current_item = [];
		// overwrite http_header 
		http_header = {
			headers: {
			},
			params: http_params
		};
		// TODO: cabs_singlesCtrl --|------ $http.get
		$http.get($scope.fetchURL,http_header).then(function(response) {
			// Get data single
			var datas = response.data;
			// TODO: cabs_singlesCtrl --|---------- set:localforage
			localforage.setItem("data_cabs_single_" + $scope.hashURL,JSON.stringify(datas));
			current_item = datas ;
		},function(data) {
			// Error message
		// TODO: cabs_singlesCtrl --|---------- $http.jsonp
				$http.jsonp($scope.fetchURLp,http_header).success(function(response){
					// Get data single
					var datas = response;
			// TODO: cabs_singlesCtrl --|---------- set:localforage
			localforage.setItem("data_cabs_single_" + $scope.hashURL,JSON.stringify(datas));
					current_item = datas ;
						$scope.$broadcast("scroll.refreshComplete");
						// event done, hidden animation loading
						$timeout(function() {
							$ionicLoading.hide();
							$scope.cabs = current_item ;
							controller_by_user();
						}, 500);
					}).error(function(resp){
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
					});
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				$scope.cabs = current_item ;
				controller_by_user();
			}, 500);
		});
	};
	// code 

	// TODO: cabs_singlesCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
$ionicConfig.backButton.text("");			
		} catch(e){
			console.log("%cerror: %cPage: `cabs_singles` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: categoryCtrl --|-- 
.controller("categoryCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: categoryCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "menu" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: categoryCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: categoryCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: categoryCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: categoryCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: categoryCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: categoryCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// code 

	// TODO: categoryCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
			
		} catch(e){
			console.log("%cerror: %cPage: `category` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: coCtrl --|-- 
.controller("coCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: coCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "menu" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: coCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: coCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: coCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: coCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: coCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: coCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// code 

	// TODO: coCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
			
		} catch(e){
			console.log("%cerror: %cPage: `co` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: contactCtrl --|-- 
.controller("contactCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: contactCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "menu" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: contactCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: contactCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: contactCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: contactCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: contactCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: contactCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// code 

	// TODO: contactCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
			
		} catch(e){
			console.log("%cerror: %cPage: `contact` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: contact_usCtrl --|-- 
.controller("contact_usCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: contact_usCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "-" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: contact_usCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: contact_usCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: contact_usCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: contact_usCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: contact_usCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: contact_usCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// TODO: contact_usCtrl --|-- $scope.showAuthentication
	$scope.showAuthentication  = function(){
		var authPopup = $ionicPopup.show({
			template: ' This page required login',
			title: "Authorization",
			subTitle: "Authorization is required",
			scope: $scope,
			buttons: [
				{text:"Cancel",onTap: function(e){
					$state.go("salasar.home");
				}},
			],
		}).then(function(form){
		},function(err){
		},function(msg){
		});
	};
	
	// set default parameter http
	var http_params = {};
	
	// set HTTP Header 
	var http_header = {
		headers: {
		},
		params: http_params
	};
	var targetQuery = ""; //default param
	var raplaceWithQuery = "";
	
	// TODO: contact_usCtrl --|-- $scope.splitArray
	$scope.splitArray = function(items,cols,maxItem) {
		var newItems = [];
		if(maxItem == 0){
			maxItem = items.length;
		}
		if(items){
			for (var i=0; i < maxItem; i+=cols) {
				newItems.push(items.slice(i, i+cols));
			}
		}
		return newItems;
	}
	$scope.gmapOptions = {options: { scrollwheel: false }};
	
	var fetch_per_scroll = 1;
	// animation loading 
	$ionicLoading.show();
	
	
	// TODO: contact_usCtrl --|-- $scope.fetchURL
	$scope.fetchURL = "data/tables/contact_leads.json";
	// TODO: contact_usCtrl --|-- $scope.fetchURLp
	$scope.fetchURLp = "data/tables/contact_leads.json?callback=JSON_CALLBACK";
	// TODO: contact_usCtrl --|-- $scope.hashURL
	$scope.hashURL = md5.createHash( $scope.fetchURL.replace(targetQuery,raplaceWithQuery));
	
	
	$scope.noMoreItemsAvailable = false; //readmore status
	var lastPush = 0;
	var data_contact_leadss = [];
	
	localforage.getItem("data_contact_leadss_" + $scope.hashURL, function(err, get_contact_leadss){
		if(get_contact_leadss === null){
			data_contact_leadss =[];
		}else{
			data_contact_leadss = JSON.parse(get_contact_leadss);
			$scope.data_contact_leadss =JSON.parse( get_contact_leadss);
			$scope.contact_leadss = [];
			for(lastPush = 0; lastPush < 50; lastPush++) {
				if (angular.isObject(data_contact_leadss[lastPush])){
					$scope.contact_leadss.push(data_contact_leadss[lastPush]);
				};
			}
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
			},200);
		}
	}).then(function(value){
	}).catch(function (err){
	})
	if(data_contact_leadss === null ){
		data_contact_leadss =[];
	}
	if(data_contact_leadss.length === 0 ){
		$timeout(function() {
			var url_request = $scope.fetchURL.replace(targetQuery,raplaceWithQuery);
			// overwrite HTTP Header 
			http_header = {
				headers: {
				},
				params: http_params
			};
			// TODO: contact_usCtrl --|-- $http.get
			console.log("%cRetrieving JSON: %c" + url_request,"color:blue;font-size:18px","color:red;font-size:18px");
			$http.get(url_request,http_header).then(function(response) {
				data_contact_leadss = response.data;
				console.log("%cSuccessfully","color:blue;font-size:18px");
				console.dir(data_contact_leadss);
				$scope.data_contact_leadss = response.data;
				// TODO: contact_usCtrl --|---------- set:localforage
				localforage.setItem("data_contact_leadss_" + $scope.hashURL, JSON.stringify(data_contact_leadss));
				$scope.contact_leadss = [];
				for(lastPush = 0; lastPush < 50; lastPush++) {
					if (angular.isObject(data_contact_leadss[lastPush])){
						$scope.contact_leadss.push(data_contact_leadss[lastPush]);
					};
				}
			},function(response) {
			
				$timeout(function() {
					var url_request = $scope.fetchURLp.replace(targetQuery,raplaceWithQuery);
					// overwrite HTTP Header 
					http_header = {
						headers: {
						},
						params: http_params
					};
					console.log("%cRetrieving again: %c" + url_request,"color:blue;font-size:18px","color:red;font-size:18px");
					// TODO: contact_usCtrl --|------ $http.jsonp
					$http.jsonp(url_request,http_header).success(function(data){
						data_contact_leadss = data;
						$scope.data_contact_leadss = data;
						$ionicLoading.hide();
						// TODO: contact_usCtrl --|---------- set:localforage
						localforage.setItem("data_contact_leadss_" + $scope.hashURL,JSON.stringify(data_contact_leadss));
						controller_by_user();
						$scope.contact_leadss = [];
						for(lastPush = 0; lastPush < 50; lastPush++) {
							if (angular.isObject(data_contact_leadss[lastPush])){
								$scope.contact_leadss.push(data_contact_leadss[lastPush]);
							};
						}
					}).error(function(data){
					if(response.status ===401){
						// TODO: contact_usCtrl --|------------ error:Unauthorized
						$scope.showAuthentication();
					}else{
						// TODO: contact_usCtrl --|------------ error:Message
						var data = { statusText:response.statusText, status:response.status };
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
						$timeout(function() {
							alertPopup.close();
						}, 2000);
					}
					});
				}, 200);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
			}, 200);
		});
	
		}, 200);
	}
	
	
	// TODO: contact_usCtrl --|-- $scope.doRefresh
	$scope.doRefresh = function(){
		var url_request = $scope.fetchURL.replace(targetQuery,raplaceWithQuery);
		// retry retrieving data
		// overwrite http_header 
		http_header = {
			headers: {
			},
			params: http_params
		};
		// TODO: contact_usCtrl --|------ $http.get
		$http.get(url_request,http_header).then(function(response) {
			data_contact_leadss = response.data;
			$scope.data_contact_leadss = response.data;
			// TODO: contact_usCtrl --|---------- set:localforage
			localforage.setItem("data_contact_leadss_" + $scope.hashURL,JSON.stringify(data_contact_leadss));
			$scope.contact_leadss = [];
			for(lastPush = 0; lastPush < 50; lastPush++) {
				if (angular.isObject(data_contact_leadss[lastPush])){
					$scope.contact_leadss.push(data_contact_leadss[lastPush]);
				};
			}
		},function(response){
			
		// retrieving data with jsonp
			$timeout(function() {
			var url_request =$scope.fetchURLp.replace(targetQuery,raplaceWithQuery);
				// overwrite http_header 
				http_header = {
					headers: {
					},
					params: http_params
				};
				// TODO: contact_usCtrl --|---------- $http.jsonp
				$http.jsonp(url_request,http_header).success(function(data){
					data_contact_leadss = data;
					$scope.data_contact_leadss = data;
					$ionicLoading.hide();
					controller_by_user();
					// TODO: contact_usCtrl --|---------- set:localforage
					localforage.setItem("data_contact_leadss_"+ $scope.hashURL,JSON.stringify(data_contact_leadss));
					$scope.contact_leadss = [];
					for(lastPush = 0; lastPush < 50; lastPush++) {
						if (angular.isObject(data_contact_leadss[lastPush])){
							$scope.contact_leadss.push(data_contact_leadss[lastPush]);
						};
					}
				}).error(function(resp){
					if(response.status ===401){
					// TODO: contact_usCtrl --|------------ error:Unauthorized
					$scope.showAuthentication();
					}else{
						// TODO: contact_usCtrl --|------------ error:Message
						var data = { statusText:response.statusText, status:response.status };
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
					};
				});
			}, 200);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
			}, 500);
		});
	
	};
	if (data_contact_leadss === null){
		data_contact_leadss = [];
	};
	// animation readmore
	var fetchItems = function() {
		for(var z=0;z<fetch_per_scroll;z++){
			if (angular.isObject(data_contact_leadss[lastPush])){
				$scope.contact_leadss.push(data_contact_leadss[lastPush]);
				lastPush++;
			}else{;
				$scope.noMoreItemsAvailable = true;
			}
		}
		$scope.$broadcast("scroll.infiniteScrollComplete");
	};
	
	// event readmore
	$scope.onInfinite = function() {
		$timeout(fetchItems, 500);
	};
	
	// create animation fade slide in right (ionic-material)
	$scope.fireEvent = function(){
		ionicMaterialInk.displayEffect();
	};
	// code 

	// TODO: contact_usCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
$ionicConfig.backButton.text("");			
		} catch(e){
			console.log("%cerror: %cPage: `contact_us` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: contact_us_singlesCtrl --|-- 
.controller("contact_us_singlesCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: contact_us_singlesCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "table (contact_us)" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: contact_us_singlesCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: contact_us_singlesCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: contact_us_singlesCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: contact_us_singlesCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: contact_us_singlesCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: contact_us_singlesCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// TODO: contact_us_singlesCtrl --|-- $scope.showAuthentication
	$scope.showAuthentication  = function(){
		var authPopup = $ionicPopup.show({
			template: ' This page required login',
			title: "Authorization",
			subTitle: "Authorization is required",
			scope: $scope,
			buttons: [
				{text:"Cancel",onTap: function(e){
					$state.go("salasar.home");
				}},
			],
		}).then(function(form){
		},function(err){
		},function(msg){
		});
	};
	
	// set default parameter http
	var http_params = {};

	// set HTTP Header 
	var http_header = {
		headers: {
		},
		params: http_params
	};
	// animation loading 
	$ionicLoading.show();
	
	// Retrieving data
	var itemID = $stateParams.feedback_id;
	// TODO: contact_us_singlesCtrl --|-- $scope.fetchURL
	$scope.fetchURL = "";
	// TODO: contact_us_singlesCtrl --|-- $scope.fetchURLp
	$scope.fetchURLp = "?callback=JSON_CALLBACK";
	// TODO: contact_us_singlesCtrl --|-- $scope.hashURL
	$scope.hashURL = md5.createHash($scope.fetchURL);
	
	var current_item = [];
	localforage.getItem("data_contact_uss_" + $scope.hashURL, function(err, get_datas){
		if(get_datas === null){
			current_item = [];
		}else{
			if(get_datas !== null){
				var datas = JSON.parse(get_datas);
				for (var i = 0; i < datas.length; i++) {
					if((datas[i].feedback_id ===  parseInt(itemID)) || (datas[i].feedback_id === itemID.toString())) {
						current_item = datas[i] ;
					}
				}
			}
			// event done, hidden animation loading
			$timeout(function(){
				$ionicLoading.hide();
				$scope.contact_us = current_item ;
				controller_by_user();
			}, 100);
		};
	}).then(function(value){
	}).catch(function (err){
	})
	if( current_item.length === 0 ){
		var itemID = $stateParams.feedback_id;
		var current_item = [];
	
		// set HTTP Header 
		http_header = {
			headers: {
			},
			params: http_params
		};
		// TODO: contact_us_singlesCtrl --|-- $http.get
		$http.get($scope.fetchURL,http_header).then(function(response) {
			// Get data single
			var datas = response.data;
			// TODO: contact_us_singlesCtrl --|---------- set:localforage
			localforage.setItem("data_contact_uss_"+ $scope.hashURL,JSON.stringify(datas));
			for (var i = 0; i < datas.length; i++) {
				if((datas[i].feedback_id ===  parseInt(itemID)) || (datas[i].feedback_id === itemID.toString())) {
					current_item = datas[i] ;
				}
			}
		},function(data) {
					// Error message
					var alertPopup = $ionicPopup.alert({
						title: "Network Error" + " (" + data.status + ")",
						template: "An error occurred while collecting data.",
					});
					$timeout(function() {
						alertPopup.close();
					}, 2000);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				$scope.contact_us = current_item ;
				controller_by_user();
			}, 500);
		});
	}
	
	
		// TODO: contact_us_singlesCtrl --|-- $scope.doRefresh
	$scope.doRefresh = function(){
		// Retrieving data
		var itemID = $stateParams.feedback_id;
		var current_item = [];
		// overwrite http_header 
		http_header = {
			headers: {
			},
			params: http_params
		};
		// TODO: contact_us_singlesCtrl --|------ $http.get
		$http.get($scope.fetchURL,http_header).then(function(response) {
			// Get data single
			var datas = response.data;
			// TODO: contact_us_singlesCtrl --|---------- set:localforage
			localforage.setItem("data_contact_uss_"+ $scope.hashURL,JSON.stringify(datas));
			for (var i = 0; i < datas.length; i++) {
				if((datas[i].feedback_id ===  parseInt(itemID)) || (datas[i].feedback_id === itemID.toString())) {
					current_item = datas[i] ;
				}
			}
		},function(data) {
			// Error message
		// TODO: contact_us_singlesCtrl --|---------- $http.jsonp
				$http.jsonp($scope.fetchURLp,http_header).success(function(response){
					// Get data single
					var datas = response;
			// TODO: contact_us_singlesCtrl --|---------- set:localforage
			localforage.setItem("data_contact_uss_"+ $scope.hashURL,JSON.stringify(datas));
					for (var i = 0; i < datas.length; i++) {
						if((datas[i].feedback_id ===  parseInt(itemID)) || (datas[i].feedback_id === itemID.toString())) {
							current_item = datas[i] ;
						}
					}
						$scope.$broadcast("scroll.refreshComplete");
						// event done, hidden animation loading
						$timeout(function() {
							$ionicLoading.hide();
							$scope.contact_us = current_item ;
							controller_by_user();
						}, 500);
					}).error(function(resp){
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
					});
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				$scope.contact_us = current_item ;
				controller_by_user();
			}, 500);
		});
	};
	// code 

	// TODO: contact_us_singlesCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
$ionicConfig.backButton.text("");			
		} catch(e){
			console.log("%cerror: %cPage: `contact_us_singles` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: dashboardCtrl --|-- 
.controller("dashboardCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: dashboardCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "menu" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: dashboardCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: dashboardCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: dashboardCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: dashboardCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: dashboardCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: dashboardCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// code 

	// TODO: dashboardCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
			
		} catch(e){
			console.log("%cerror: %cPage: `dashboard` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: form_book_panditCtrl --|-- 
.controller("form_book_panditCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: form_book_panditCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "forms" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: form_book_panditCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: form_book_panditCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: form_book_panditCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: form_book_panditCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: form_book_panditCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: form_book_panditCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	
	// Form Request
	//book_pandit
	$scope.form_book_pandit= {};
	// TODO: form_book_panditCtrl --|-- $scope.submitBook_pandit
	$scope.submitBook_pandit = function(){
		// animation loading 
		$ionicLoading.show();
	
		var $messages, $title = null;
		$http({
				method:"POST",
				url: "#",
				data: $httpParamSerializer($scope.form_book_pandit),  // pass in data as strings
				headers: {"Content-Type":"application/x-www-form-urlencoded"}  // set the headers so angular passing info as form data (not request payload)
			})
			.then(function(response) {
				$messages = response.data.message;
				$title = response.data.title;
			},function(response){
				$messages = response.statusText;
				$title = response.status;
			}).finally(function(){
				// event done, hidden animation loading
				$timeout(function() {
					$ionicLoading.hide();
					if($messages !== null){
						// message
					var alertPopup = $ionicPopup.alert({
						title: $title,
						template: $messages,
					});
						// clear input
						$scope.form_book_pandit.firstname = "";
						$scope.form_book_pandit.email = "";
						$scope.form_book_pandit.phone = "";
						$scope.form_book_pandit.city = "";
						$scope.form_book_pandit.address1 = "";
						$scope.form_book_pandit.productinfo = "";
						$scope.form_book_pandit.puja_location = "";
					}
			}, 500);
		});
	};
	// code 

	// TODO: form_book_panditCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
			
		} catch(e){
			console.log("%cerror: %cPage: `form_book_pandit` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})



// TODO: form_contact_usCtrl --|-- 
.controller("form_contact_usCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: form_contact_usCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "table (contact_us)" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: form_contact_usCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: form_contact_usCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: form_contact_usCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: form_contact_usCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: form_contact_usCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: form_contact_usCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// TODO: form_contact_usCtrl --|-- $scope.showAuthentication
	$scope.showAuthentication  = function(){
		var authPopup = $ionicPopup.show({
			template: ' This page required login',
			title: "Authorization",
			subTitle: "Authorization is required",
			scope: $scope,
			buttons: [
				{text:"Cancel",onTap: function(e){
					$state.go("salasar.home");
				}},
			],
		}).then(function(form){
		},function(err){
		},function(msg){
		});
	};
	
	// set default parameter http
	var http_params = {};
	
	// set HTTP Header 
	var http_header = {
		headers: {
		},
		params: http_params
	};
	var targetQuery = ""; //default param
	var raplaceWithQuery = "";
	
	// TODO: form_contact_usCtrl --|-- $scope.splitArray
	$scope.splitArray = function(items,cols,maxItem) {
		var newItems = [];
		if(maxItem == 0){
			maxItem = items.length;
		}
		if(items){
			for (var i=0; i < maxItem; i+=cols) {
				newItems.push(items.slice(i, i+cols));
			}
		}
		return newItems;
	}
	$scope.gmapOptions = {options: { scrollwheel: false }};
	
	var fetch_per_scroll = 1;
	// animation loading 
	$ionicLoading.show();
	
	
	// TODO: form_contact_usCtrl --|-- $scope.fetchURL
	$scope.fetchURL = "";
	// TODO: form_contact_usCtrl --|-- $scope.fetchURLp
	$scope.fetchURLp = "?callback=JSON_CALLBACK";
	// TODO: form_contact_usCtrl --|-- $scope.hashURL
	$scope.hashURL = md5.createHash( $scope.fetchURL.replace(targetQuery,raplaceWithQuery));
	
	
	$scope.noMoreItemsAvailable = false; //readmore status
	var lastPush = 0;
	var data_contact_uss = [];
	
	localforage.getItem("data_contact_uss_" + $scope.hashURL, function(err, get_contact_uss){
		if(get_contact_uss === null){
			data_contact_uss =[];
		}else{
			data_contact_uss = JSON.parse(get_contact_uss);
			$scope.data_contact_uss =JSON.parse( get_contact_uss);
			$scope.contact_uss = [];
			for(lastPush = 0; lastPush < 50; lastPush++) {
				if (angular.isObject(data_contact_uss[lastPush])){
					$scope.contact_uss.push(data_contact_uss[lastPush]);
				};
			}
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
			},200);
		}
	}).then(function(value){
	}).catch(function (err){
	})
	if(data_contact_uss === null ){
		data_contact_uss =[];
	}
	if(data_contact_uss.length === 0 ){
		$timeout(function() {
			var url_request = $scope.fetchURL.replace(targetQuery,raplaceWithQuery);
			// overwrite HTTP Header 
			http_header = {
				headers: {
				},
				params: http_params
			};
			// TODO: form_contact_usCtrl --|-- $http.get
			console.log("%cRetrieving JSON: %c" + url_request,"color:blue;font-size:18px","color:red;font-size:18px");
			$http.get(url_request,http_header).then(function(response) {
				data_contact_uss = response.data;
				console.log("%cSuccessfully","color:blue;font-size:18px");
				console.dir(data_contact_uss);
				$scope.data_contact_uss = response.data;
				// TODO: form_contact_usCtrl --|---------- set:localforage
				localforage.setItem("data_contact_uss_" + $scope.hashURL, JSON.stringify(data_contact_uss));
				$scope.contact_uss = [];
				for(lastPush = 0; lastPush < 50; lastPush++) {
					if (angular.isObject(data_contact_uss[lastPush])){
						$scope.contact_uss.push(data_contact_uss[lastPush]);
					};
				}
			},function(response) {
			
				$timeout(function() {
					var url_request = $scope.fetchURLp.replace(targetQuery,raplaceWithQuery);
					// overwrite HTTP Header 
					http_header = {
						headers: {
						},
						params: http_params
					};
					console.log("%cRetrieving again: %c" + url_request,"color:blue;font-size:18px","color:red;font-size:18px");
					// TODO: form_contact_usCtrl --|------ $http.jsonp
					$http.jsonp(url_request,http_header).success(function(data){
						data_contact_uss = data;
						$scope.data_contact_uss = data;
						$ionicLoading.hide();
						// TODO: form_contact_usCtrl --|---------- set:localforage
						localforage.setItem("data_contact_uss_" + $scope.hashURL,JSON.stringify(data_contact_uss));
						controller_by_user();
						$scope.contact_uss = [];
						for(lastPush = 0; lastPush < 50; lastPush++) {
							if (angular.isObject(data_contact_uss[lastPush])){
								$scope.contact_uss.push(data_contact_uss[lastPush]);
							};
						}
					}).error(function(data){
					if(response.status ===401){
						// TODO: form_contact_usCtrl --|------------ error:Unauthorized
						$scope.showAuthentication();
					}else{
						// TODO: form_contact_usCtrl --|------------ error:Message
						var data = { statusText:response.statusText, status:response.status };
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
						$timeout(function() {
							alertPopup.close();
						}, 2000);
					}
					});
				}, 200);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
			}, 200);
		});
	
		}, 200);
	}
	
	
	// TODO: form_contact_usCtrl --|-- $scope.doRefresh
	$scope.doRefresh = function(){
		var url_request = $scope.fetchURL.replace(targetQuery,raplaceWithQuery);
		// retry retrieving data
		// overwrite http_header 
		http_header = {
			headers: {
			},
			params: http_params
		};
		// TODO: form_contact_usCtrl --|------ $http.get
		$http.get(url_request,http_header).then(function(response) {
			data_contact_uss = response.data;
			$scope.data_contact_uss = response.data;
			// TODO: form_contact_usCtrl --|---------- set:localforage
			localforage.setItem("data_contact_uss_" + $scope.hashURL,JSON.stringify(data_contact_uss));
			$scope.contact_uss = [];
			for(lastPush = 0; lastPush < 50; lastPush++) {
				if (angular.isObject(data_contact_uss[lastPush])){
					$scope.contact_uss.push(data_contact_uss[lastPush]);
				};
			}
		},function(response){
			
		// retrieving data with jsonp
			$timeout(function() {
			var url_request =$scope.fetchURLp.replace(targetQuery,raplaceWithQuery);
				// overwrite http_header 
				http_header = {
					headers: {
					},
					params: http_params
				};
				// TODO: form_contact_usCtrl --|---------- $http.jsonp
				$http.jsonp(url_request,http_header).success(function(data){
					data_contact_uss = data;
					$scope.data_contact_uss = data;
					$ionicLoading.hide();
					controller_by_user();
					// TODO: form_contact_usCtrl --|---------- set:localforage
					localforage.setItem("data_contact_uss_"+ $scope.hashURL,JSON.stringify(data_contact_uss));
					$scope.contact_uss = [];
					for(lastPush = 0; lastPush < 50; lastPush++) {
						if (angular.isObject(data_contact_uss[lastPush])){
							$scope.contact_uss.push(data_contact_uss[lastPush]);
						};
					}
				}).error(function(resp){
					if(response.status ===401){
					// TODO: form_contact_usCtrl --|------------ error:Unauthorized
					$scope.showAuthentication();
					}else{
						// TODO: form_contact_usCtrl --|------------ error:Message
						var data = { statusText:response.statusText, status:response.status };
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
					};
				});
			}, 200);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
			}, 500);
		});
	
	};
	if (data_contact_uss === null){
		data_contact_uss = [];
	};
	// animation readmore
	var fetchItems = function() {
		for(var z=0;z<fetch_per_scroll;z++){
			if (angular.isObject(data_contact_uss[lastPush])){
				$scope.contact_uss.push(data_contact_uss[lastPush]);
				lastPush++;
			}else{;
				$scope.noMoreItemsAvailable = true;
			}
		}
		$scope.$broadcast("scroll.infiniteScrollComplete");
	};
	
	// event readmore
	$scope.onInfinite = function() {
		$timeout(fetchItems, 500);
	};
	
	// create animation fade slide in right (ionic-material)
	$scope.fireEvent = function(){
		ionicMaterialInk.displayEffect();
	};
	// code 

	// TODO: form_contact_usCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
//debug: all data
//console.log(data_contact_uss);
$ionicConfig.backButton.text("");
			
		} catch(e){
			console.log("%cerror: %cPage: `form_contact_us` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: form_loginCtrl --|-- 
.controller("form_loginCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: form_loginCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "page_builder" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: form_loginCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: form_loginCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: form_loginCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: form_loginCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: form_loginCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: form_loginCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	
	// Form Request
	// code 

	// TODO: form_loginCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
 
$scope.submitUser = function(form){
    
    $ionicLoading.show({
		template: '<div class="loader"><svg class="circular"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div>'
	});
    
    if( angular.isDefined(form)){
        
		var uname = form.email || "demo";
		var pwd = form.password || "demo";
		var http_value = "Basic " + base64.encode(uname + ":" + pwd);
		$http.defaults.headers.common["X-Authorization"] = http_value;
        $http.get("https://salasarbalaji.000webhostapp.com/rest-api.php?json=auth").then(function(resp){
            if(resp.data.data.status == 401){
    			var alertPopup = $ionicPopup.alert({
    				title: resp.data.title,
    				template: resp.data.message,
    			});
            }else{
           	    $ionicHistory.nextViewOptions({
            		disableAnimate: true,
            		disableBack: true
            	});
                $state.go("salasar.profile");
            }
        },function(resp){   
            
			$timeout(function() {
				$ionicLoading.hide();
                
        		var alertPopup = $ionicPopup.alert({
        			title: "Ops, error!",
        			template: "Problem with crossdomain or Invalid JSON URL.",
        		});
                            
			}, 500); 
                        
            
        }).finally(function(){   
            
			$timeout(function() {
				$ionicLoading.hide();
			}, 500);          
              
        });    
    }
    
	$timeout(function() {
	   $ionicLoading.hide();
	}, 1000);  
    
}    
			
		} catch(e){
			console.log("%cerror: %cPage: `form_login` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: form_userCtrl --|-- 
.controller("form_userCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: form_userCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "page_builder" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: form_userCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: form_userCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: form_userCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: form_userCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: form_userCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: form_userCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	
	// Form Request
	//user
	$scope.form_user= {};
	// TODO: form_userCtrl --|-- $scope.submitUser
	$scope.submitUser = function(){
		// animation loading 
		$ionicLoading.show();
	
		var $messages, $title = null;
		$http({
				method:"POST",
				url: "https://salasarbalaji.000webhostapp.com/rest-api.php?json=submit&form=user",
				data: $httpParamSerializer($scope.form_user),  // pass in data as strings
				headers: {"Content-Type":"application/x-www-form-urlencoded"}  // set the headers so angular passing info as form data (not request payload)
			})
			.then(function(response) {
				$messages = response.data.message;
				$title = response.data.title;
			},function(response){
				$messages = response.statusText;
				$title = response.status;
			}).finally(function(){
				// event done, hidden animation loading
				$timeout(function() {
					$ionicLoading.hide();
					if($messages !== null){
						// message
					var alertPopup = $ionicPopup.alert({
						title: $title,
						template: $messages,
					});
						// clear input
						$scope.form_user.fullname = "";
						$scope.form_user.uname = "";
						$scope.form_user.pwd = "";
						$scope.form_user.address = "";
						$scope.form_user.type_ic = "";
						$scope.form_user.no_ic = "";
						$scope.form_user.phone = "";
						$scope.form_user.email = "";
					}
			}, 500);
		});
	};
	// code 

	// TODO: form_userCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
$ionicConfig.backButton.text("");			
		} catch(e){
			console.log("%cerror: %cPage: `form_user` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: form_user_registerCtrl --|-- 
.controller("form_user_registerCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: form_user_registerCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "table (user)" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: form_user_registerCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: form_user_registerCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: form_user_registerCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: form_user_registerCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: form_user_registerCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: form_user_registerCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// TODO: form_user_registerCtrl --|-- $scope.showAuthentication
	$scope.showAuthentication  = function(){
		var authPopup = $ionicPopup.show({
			template: ' This page required login',
			title: "Authorization",
			subTitle: "Authorization is required",
			scope: $scope,
			buttons: [
				{text:"Cancel",onTap: function(e){
					$state.go("salasar.home");
				}},
			],
		}).then(function(form){
		},function(err){
		},function(msg){
		});
	};
	
	// set default parameter http
	var http_params = {};
	
	// set HTTP Header 
	var http_header = {
		headers: {
		},
		params: http_params
	};
	var targetQuery = ""; //default param
	var raplaceWithQuery = "";
	
	// TODO: form_user_registerCtrl --|-- $scope.splitArray
	$scope.splitArray = function(items,cols,maxItem) {
		var newItems = [];
		if(maxItem == 0){
			maxItem = items.length;
		}
		if(items){
			for (var i=0; i < maxItem; i+=cols) {
				newItems.push(items.slice(i, i+cols));
			}
		}
		return newItems;
	}
	$scope.gmapOptions = {options: { scrollwheel: false }};
	
	var fetch_per_scroll = 1;
	// animation loading 
	$ionicLoading.show();
	
	
	// TODO: form_user_registerCtrl --|-- $scope.fetchURL
	$scope.fetchURL = "https://salasarbalaji.000webhostapp.com/rest-api.phpform=user_register&json=submit";
	// TODO: form_user_registerCtrl --|-- $scope.fetchURLp
	$scope.fetchURLp = "https://salasarbalaji.000webhostapp.com/rest-api.phpform=user_register&json=submit?callback=JSON_CALLBACK";
	// TODO: form_user_registerCtrl --|-- $scope.hashURL
	$scope.hashURL = md5.createHash( $scope.fetchURL.replace(targetQuery,raplaceWithQuery));
	
	
	$scope.noMoreItemsAvailable = false; //readmore status
	var lastPush = 0;
	var data_users = [];
	
	localforage.getItem("data_users_" + $scope.hashURL, function(err, get_users){
		if(get_users === null){
			data_users =[];
		}else{
			data_users = JSON.parse(get_users);
			$scope.data_users =JSON.parse( get_users);
			$scope.users = [];
			for(lastPush = 0; lastPush < 50; lastPush++) {
				if (angular.isObject(data_users[lastPush])){
					$scope.users.push(data_users[lastPush]);
				};
			}
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
			},200);
		}
	}).then(function(value){
	}).catch(function (err){
	})
	if(data_users === null ){
		data_users =[];
	}
	if(data_users.length === 0 ){
		$timeout(function() {
			var url_request = $scope.fetchURL.replace(targetQuery,raplaceWithQuery);
			// overwrite HTTP Header 
			http_header = {
				headers: {
				},
				params: http_params
			};
			// TODO: form_user_registerCtrl --|-- $http.get
			console.log("%cRetrieving JSON: %c" + url_request,"color:blue;font-size:18px","color:red;font-size:18px");
			$http.get(url_request,http_header).then(function(response) {
				data_users = response.data;
				console.log("%cSuccessfully","color:blue;font-size:18px");
				console.dir(data_users);
				$scope.data_users = response.data;
				// TODO: form_user_registerCtrl --|---------- set:localforage
				localforage.setItem("data_users_" + $scope.hashURL, JSON.stringify(data_users));
				$scope.users = [];
				for(lastPush = 0; lastPush < 50; lastPush++) {
					if (angular.isObject(data_users[lastPush])){
						$scope.users.push(data_users[lastPush]);
					};
				}
			},function(response) {
			
				$timeout(function() {
					var url_request = $scope.fetchURLp.replace(targetQuery,raplaceWithQuery);
					// overwrite HTTP Header 
					http_header = {
						headers: {
						},
						params: http_params
					};
					console.log("%cRetrieving again: %c" + url_request,"color:blue;font-size:18px","color:red;font-size:18px");
					// TODO: form_user_registerCtrl --|------ $http.jsonp
					$http.jsonp(url_request,http_header).success(function(data){
						data_users = data;
						$scope.data_users = data;
						$ionicLoading.hide();
						// TODO: form_user_registerCtrl --|---------- set:localforage
						localforage.setItem("data_users_" + $scope.hashURL,JSON.stringify(data_users));
						controller_by_user();
						$scope.users = [];
						for(lastPush = 0; lastPush < 50; lastPush++) {
							if (angular.isObject(data_users[lastPush])){
								$scope.users.push(data_users[lastPush]);
							};
						}
					}).error(function(data){
					if(response.status ===401){
						// TODO: form_user_registerCtrl --|------------ error:Unauthorized
						$scope.showAuthentication();
					}else{
						// TODO: form_user_registerCtrl --|------------ error:Message
						var data = { statusText:response.statusText, status:response.status };
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
						$timeout(function() {
							alertPopup.close();
						}, 2000);
					}
					});
				}, 200);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
			}, 200);
		});
	
		}, 200);
	}
	
	
	// TODO: form_user_registerCtrl --|-- $scope.doRefresh
	$scope.doRefresh = function(){
		var url_request = $scope.fetchURL.replace(targetQuery,raplaceWithQuery);
		// retry retrieving data
		// overwrite http_header 
		http_header = {
			headers: {
			},
			params: http_params
		};
		// TODO: form_user_registerCtrl --|------ $http.get
		$http.get(url_request,http_header).then(function(response) {
			data_users = response.data;
			$scope.data_users = response.data;
			// TODO: form_user_registerCtrl --|---------- set:localforage
			localforage.setItem("data_users_" + $scope.hashURL,JSON.stringify(data_users));
			$scope.users = [];
			for(lastPush = 0; lastPush < 50; lastPush++) {
				if (angular.isObject(data_users[lastPush])){
					$scope.users.push(data_users[lastPush]);
				};
			}
		},function(response){
			
		// retrieving data with jsonp
			$timeout(function() {
			var url_request =$scope.fetchURLp.replace(targetQuery,raplaceWithQuery);
				// overwrite http_header 
				http_header = {
					headers: {
					},
					params: http_params
				};
				// TODO: form_user_registerCtrl --|---------- $http.jsonp
				$http.jsonp(url_request,http_header).success(function(data){
					data_users = data;
					$scope.data_users = data;
					$ionicLoading.hide();
					controller_by_user();
					// TODO: form_user_registerCtrl --|---------- set:localforage
					localforage.setItem("data_users_"+ $scope.hashURL,JSON.stringify(data_users));
					$scope.users = [];
					for(lastPush = 0; lastPush < 50; lastPush++) {
						if (angular.isObject(data_users[lastPush])){
							$scope.users.push(data_users[lastPush]);
						};
					}
				}).error(function(resp){
					if(response.status ===401){
					// TODO: form_user_registerCtrl --|------------ error:Unauthorized
					$scope.showAuthentication();
					}else{
						// TODO: form_user_registerCtrl --|------------ error:Message
						var data = { statusText:response.statusText, status:response.status };
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
					};
				});
			}, 200);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
			}, 500);
		});
	
	};
	if (data_users === null){
		data_users = [];
	};
	// animation readmore
	var fetchItems = function() {
		for(var z=0;z<fetch_per_scroll;z++){
			if (angular.isObject(data_users[lastPush])){
				$scope.users.push(data_users[lastPush]);
				lastPush++;
			}else{;
				$scope.noMoreItemsAvailable = true;
			}
		}
		$scope.$broadcast("scroll.infiniteScrollComplete");
	};
	
	// event readmore
	$scope.onInfinite = function() {
		$timeout(fetchItems, 500);
	};
	
	// create animation fade slide in right (ionic-material)
	$scope.fireEvent = function(){
		ionicMaterialInk.displayEffect();
	};
	// code 

	// TODO: form_user_registerCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
//debug: all data
//console.log(data_users);
$ionicConfig.backButton.text("");
			
		} catch(e){
			console.log("%cerror: %cPage: `form_user_register` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: galleries_singlesCtrl --|-- 
.controller("galleries_singlesCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: galleries_singlesCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "table (galleries)" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: galleries_singlesCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: galleries_singlesCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: galleries_singlesCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: galleries_singlesCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: galleries_singlesCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: galleries_singlesCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// TODO: galleries_singlesCtrl --|-- $scope.showAuthentication
	$scope.showAuthentication  = function(){
		var authPopup = $ionicPopup.show({
			template: ' This page required login',
			title: "Authorization",
			subTitle: "Authorization is required",
			scope: $scope,
			buttons: [
				{text:"Cancel",onTap: function(e){
					$state.go("salasar.home");
				}},
			],
		}).then(function(form){
		},function(err){
		},function(msg){
		});
	};
	
	// set default parameter http
	var http_params = {};

	// set HTTP Header 
	var http_header = {
		headers: {
		},
		params: http_params
	};
	// animation loading 
	$ionicLoading.show();
	
	// Retrieving data
	var itemID = $stateParams.id;
	// TODO: galleries_singlesCtrl --|-- $scope.fetchURL
	$scope.fetchURL = "https://salasarbalaji.000webhostapp.com/rest-api.php?json=galleries&id=" + itemID;
	// TODO: galleries_singlesCtrl --|-- $scope.fetchURLp
	$scope.fetchURLp = "https://salasarbalaji.000webhostapp.com/rest-api.php?json=galleries&id=" + itemID + "&callback=JSON_CALLBACK";
	// TODO: galleries_singlesCtrl --|-- $scope.hashURL
	$scope.hashURL = md5.createHash($scope.fetchURL);
	
	var current_item = [];
	localforage.getItem("data_galleries_single_" + $scope.hashURL, function(err, get_datas){
		if(get_datas === null){
			current_item = [];
		}else{
			if(get_datas !== null){
				current_item = JSON.parse(get_datas);
				$timeout(function(){
					$ionicLoading.hide();
					$scope.galleries = current_item ;
					controller_by_user();
				}, 500);
			};
		};
	}).then(function(value){
	}).catch(function (err){
	})
	if( current_item.length === 0 ){
		var itemID = $stateParams.id;
		var current_item = [];
	
		// set HTTP Header 
		http_header = {
			headers: {
			},
			params: http_params
		};
		// TODO: galleries_singlesCtrl --|-- $http.get
		$http.get($scope.fetchURL,http_header).then(function(response) {
			// Get data single
			var datas = response.data;
			// TODO: galleries_singlesCtrl --|---------- set:localforage
			localforage.setItem("data_galleries_single_" + $scope.hashURL,JSON.stringify(datas));
			current_item = datas ;
		},function(data) {
					// Error message
					var alertPopup = $ionicPopup.alert({
						title: "Network Error" + " (" + data.status + ")",
						template: "An error occurred while collecting data.",
					});
					$timeout(function() {
						alertPopup.close();
					}, 2000);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				$scope.galleries = current_item ;
				controller_by_user();
			}, 500);
		});
	}
	
	
		// TODO: galleries_singlesCtrl --|-- $scope.doRefresh
	$scope.doRefresh = function(){
		// Retrieving data
		var itemID = $stateParams.id;
		var current_item = [];
		// overwrite http_header 
		http_header = {
			headers: {
			},
			params: http_params
		};
		// TODO: galleries_singlesCtrl --|------ $http.get
		$http.get($scope.fetchURL,http_header).then(function(response) {
			// Get data single
			var datas = response.data;
			// TODO: galleries_singlesCtrl --|---------- set:localforage
			localforage.setItem("data_galleries_single_" + $scope.hashURL,JSON.stringify(datas));
			current_item = datas ;
		},function(data) {
			// Error message
		// TODO: galleries_singlesCtrl --|---------- $http.jsonp
				$http.jsonp($scope.fetchURLp,http_header).success(function(response){
					// Get data single
					var datas = response;
			// TODO: galleries_singlesCtrl --|---------- set:localforage
			localforage.setItem("data_galleries_single_" + $scope.hashURL,JSON.stringify(datas));
					current_item = datas ;
						$scope.$broadcast("scroll.refreshComplete");
						// event done, hidden animation loading
						$timeout(function() {
							$ionicLoading.hide();
							$scope.galleries = current_item ;
							controller_by_user();
						}, 500);
					}).error(function(resp){
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
					});
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				$scope.galleries = current_item ;
				controller_by_user();
			}, 500);
		});
	};
	// code 

	// TODO: galleries_singlesCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
$ionicConfig.backButton.text("");			
		} catch(e){
			console.log("%cerror: %cPage: `galleries_singles` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: galleryCtrl --|-- 
.controller("galleryCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: galleryCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "table (galleries)" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: galleryCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: galleryCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: galleryCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: galleryCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: galleryCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: galleryCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// TODO: galleryCtrl --|-- $scope.showAuthentication
	$scope.showAuthentication  = function(){
		var authPopup = $ionicPopup.show({
			template: ' This page required login',
			title: "Authorization",
			subTitle: "Authorization is required",
			scope: $scope,
			buttons: [
				{text:"Cancel",onTap: function(e){
					$state.go("salasar.home");
				}},
			],
		}).then(function(form){
		},function(err){
		},function(msg){
		});
	};
	
	// set default parameter http
	var http_params = {};
	
	// set HTTP Header 
	var http_header = {
		headers: {
		},
		params: http_params
	};
	var targetQuery = ""; //default param
	var raplaceWithQuery = "";
	//fix url Gallery
	targetQuery = "album_id=1"; //default param
	raplaceWithQuery = "album_id=1";
		
	$scope.first_param = {};
	$scope.first_param.album_id = "1";
	if(typeof $stateParams.album_id !== 'undefined'){
		raplaceWithQuery = "album_id=" + $stateParams.album_id;
		$scope.first_param.album_id = $stateParams.album_id;
	}
	if(typeof $rootScope.galleryQueryParam !== "undefined"){
		raplaceWithQuery = "album_id=" +  $rootScope.galleryQueryParam ;
	}
	if($scope.first_param.album_id=="-1"){
		$scope.first_param.album_id = "";
	}

	
	// TODO: galleryCtrl --|-- $scope.splitArray
	$scope.splitArray = function(items,cols,maxItem) {
		var newItems = [];
		if(maxItem == 0){
			maxItem = items.length;
		}
		if(items){
			for (var i=0; i < maxItem; i+=cols) {
				newItems.push(items.slice(i, i+cols));
			}
		}
		return newItems;
	}
	$scope.gmapOptions = {options: { scrollwheel: false }};
	
	var fetch_per_scroll = 1;
	// animation loading 
	$ionicLoading.show();
	
	
	// TODO: galleryCtrl --|-- $scope.fetchURL
	$scope.fetchURL = "https://salasarbalaji.000webhostapp.com/rest-api.php?album_id=1&json=galleries";
	// TODO: galleryCtrl --|-- $scope.fetchURLp
	$scope.fetchURLp = "https://salasarbalaji.000webhostapp.com/rest-api.php?album_id=1&json=galleries&callback=JSON_CALLBACK";
	// TODO: galleryCtrl --|-- $scope.hashURL
	$scope.hashURL = md5.createHash( $scope.fetchURL.replace(targetQuery,raplaceWithQuery));
	
	
	$scope.noMoreItemsAvailable = false; //readmore status
	var lastPush = 0;
	var data_galleriess = [];
	
	localforage.getItem("data_galleriess_" + $scope.hashURL, function(err, get_galleriess){
		if(get_galleriess === null){
			data_galleriess =[];
		}else{
			data_galleriess = JSON.parse(get_galleriess);
			$scope.data_galleriess =JSON.parse( get_galleriess);
			$timeout(function() {
				$ionicLoading.hide();
				$scope.galleriess = data_galleriess;
				$ionicSlideBoxDelegate.update();
				controller_by_user();
			},200);
		}
	}).then(function(value){
	}).catch(function (err){
	})
	if(data_galleriess === null ){
		data_galleriess =[];
	}
	if(data_galleriess.length === 0 ){
		$timeout(function() {
			var url_request = $scope.fetchURL.replace(targetQuery,raplaceWithQuery);
			// overwrite HTTP Header 
			http_header = {
				headers: {
				},
				params: http_params
			};
			// TODO: galleryCtrl --|-- $http.get
			console.log("%cRetrieving JSON: %c" + url_request,"color:blue;font-size:18px","color:red;font-size:18px");
			$http.get(url_request,http_header).then(function(response) {
				data_galleriess = response.data;
				console.log("%cSuccessfully","color:blue;font-size:18px");
				console.dir(data_galleriess);
				$scope.data_galleriess = response.data;
				// TODO: galleryCtrl --|---------- set:localforage
				localforage.setItem("data_galleriess_" + $scope.hashURL, JSON.stringify(data_galleriess));
			},function(response) {
			
				$timeout(function() {
					var url_request = $scope.fetchURLp.replace(targetQuery,raplaceWithQuery);
					// overwrite HTTP Header 
					http_header = {
						headers: {
						},
						params: http_params
					};
					console.log("%cRetrieving again: %c" + url_request,"color:blue;font-size:18px","color:red;font-size:18px");
					// TODO: galleryCtrl --|------ $http.jsonp
					$http.jsonp(url_request,http_header).success(function(data){
						data_galleriess = data;
						$scope.data_galleriess = data;
						$ionicLoading.hide();
						// TODO: galleryCtrl --|---------- set:localforage
						localforage.setItem("data_galleriess_" + $scope.hashURL,JSON.stringify(data_galleriess));
						controller_by_user();
					}).error(function(data){
					if(response.status ===401){
						// TODO: galleryCtrl --|------------ error:Unauthorized
						$scope.showAuthentication();
					}else{
						// TODO: galleryCtrl --|------------ error:Message
						var data = { statusText:response.statusText, status:response.status };
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
						$timeout(function() {
							alertPopup.close();
						}, 2000);
					}
					});
				}, 200);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
				$scope.galleriess = data_galleriess;
				$ionicSlideBoxDelegate.update();
			}, 200);
		});
	
		}, 200);
	}
	
	
	// TODO: galleryCtrl --|-- $scope.doRefresh
	$scope.doRefresh = function(){
		var url_request = $scope.fetchURL.replace(targetQuery,raplaceWithQuery);
		// retry retrieving data
		// overwrite http_header 
		http_header = {
			headers: {
			},
			params: http_params
		};
		// TODO: galleryCtrl --|------ $http.get
		$http.get(url_request,http_header).then(function(response) {
			data_galleriess = response.data;
			$scope.data_galleriess = response.data;
			// TODO: galleryCtrl --|---------- set:localforage
			localforage.setItem("data_galleriess_" + $scope.hashURL,JSON.stringify(data_galleriess));
		},function(response){
			
		// retrieving data with jsonp
			$timeout(function() {
			var url_request =$scope.fetchURLp.replace(targetQuery,raplaceWithQuery);
				// overwrite http_header 
				http_header = {
					headers: {
					},
					params: http_params
				};
				// TODO: galleryCtrl --|---------- $http.jsonp
				$http.jsonp(url_request,http_header).success(function(data){
					data_galleriess = data;
					$scope.data_galleriess = data;
					$ionicLoading.hide();
					controller_by_user();
					// TODO: galleryCtrl --|---------- set:localforage
					localforage.setItem("data_galleriess_"+ $scope.hashURL,JSON.stringify(data_galleriess));
				}).error(function(resp){
					if(response.status ===401){
					// TODO: galleryCtrl --|------------ error:Unauthorized
					$scope.showAuthentication();
					}else{
						// TODO: galleryCtrl --|------------ error:Message
						var data = { statusText:response.statusText, status:response.status };
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
					};
				});
			}, 200);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
				if(data_galleriess){
					$scope.galleriess = data_galleriess;
				}
				$ionicSlideBoxDelegate.update();
			}, 500);
		});
	
	};
	// code 

	// TODO: galleryCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
//debug: all data
//console.log(data_galleriess);
$ionicConfig.backButton.text("");
			
		} catch(e){
			console.log("%cerror: %cPage: `gallery` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: historyCtrl --|-- 
.controller("historyCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: historyCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "page_builder" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: historyCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: historyCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: historyCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: historyCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: historyCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: historyCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// code 

	// TODO: historyCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
			
		} catch(e){
			console.log("%cerror: %cPage: `history` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: homeCtrl --|-- 
.controller("homeCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: homeCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "page_builder" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: homeCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: homeCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: homeCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: homeCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: homeCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: homeCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up",
		});
	}, 300);
	// code 

	// TODO: homeCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
			
		} catch(e){
			console.log("%cerror: %cPage: `home` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: hotel_catsCtrl --|-- 
.controller("hotel_catsCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: hotel_catsCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "table (hotel_categories)" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: hotel_catsCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: hotel_catsCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: hotel_catsCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: hotel_catsCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: hotel_catsCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: hotel_catsCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// TODO: hotel_catsCtrl --|-- $scope.showAuthentication
	$scope.showAuthentication  = function(){
		var authPopup = $ionicPopup.show({
			template: ' This page required login',
			title: "Authorization",
			subTitle: "Authorization is required",
			scope: $scope,
			buttons: [
				{text:"Cancel",onTap: function(e){
					$state.go("salasar.home");
				}},
			],
		}).then(function(form){
		},function(err){
		},function(msg){
		});
	};
	
	// set default parameter http
	var http_params = {};
	
	// set HTTP Header 
	var http_header = {
		headers: {
			"Accept": "application/json",
		},
		params: http_params
	};
	var targetQuery = ""; //default param
	var raplaceWithQuery = "";
	//fix url Hotel Category
	targetQuery = "json=hotel_categories"; //default param
	raplaceWithQuery = "json=hotel_categories";
	
	
	// TODO: hotel_catsCtrl --|-- $scope.splitArray
	$scope.splitArray = function(items,cols,maxItem) {
		var newItems = [];
		if(maxItem == 0){
			maxItem = items.length;
		}
		if(items){
			for (var i=0; i < maxItem; i+=cols) {
				newItems.push(items.slice(i, i+cols));
			}
		}
		return newItems;
	}
	$scope.gmapOptions = {options: { scrollwheel: false }};
	
	var fetch_per_scroll = 4;
	// animation loading 
	$ionicLoading.show();
	
	
	// TODO: hotel_catsCtrl --|-- $scope.fetchURL
	$scope.fetchURL = "https://salasarbalaji.000webhostapp.com/rest-api.php?json=hotel_categories";
	// TODO: hotel_catsCtrl --|-- $scope.fetchURLp
	$scope.fetchURLp = "https://salasarbalaji.000webhostapp.com/rest-api.php?json=hotel_categories&callback=JSON_CALLBACK";
	// TODO: hotel_catsCtrl --|-- $scope.hashURL
	$scope.hashURL = md5.createHash( $scope.fetchURL.replace(targetQuery,raplaceWithQuery));
	
	
	$scope.noMoreItemsAvailable = false; //readmore status
	var lastPush = 0;
	var data_hotel_categoriess = [];
	
	localforage.getItem("data_hotel_categoriess_" + $scope.hashURL, function(err, get_hotel_categoriess){
		if(get_hotel_categoriess === null){
			data_hotel_categoriess =[];
		}else{
			data_hotel_categoriess = JSON.parse(get_hotel_categoriess);
			$scope.data_hotel_categoriess =JSON.parse( get_hotel_categoriess);
			$scope.hotel_categoriess = [];
			for(lastPush = 0; lastPush < 9; lastPush++) {
				if (angular.isObject(data_hotel_categoriess[lastPush])){
					$scope.hotel_categoriess.push(data_hotel_categoriess[lastPush]);
				};
			}
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
			},200);
		}
	}).then(function(value){
	}).catch(function (err){
	})
	if(data_hotel_categoriess === null ){
		data_hotel_categoriess =[];
	}
	if(data_hotel_categoriess.length === 0 ){
		$timeout(function() {
			var url_request = $scope.fetchURL.replace(targetQuery,raplaceWithQuery);
			// overwrite HTTP Header 
			http_header = {
				headers: {
					"Accept": "application/json",
				},
				params: http_params
			};
			// TODO: hotel_catsCtrl --|-- $http.get
			console.log("%cRetrieving JSON: %c" + url_request,"color:blue;font-size:18px","color:red;font-size:18px");
			$http.get(url_request,http_header).then(function(response) {
				data_hotel_categoriess = response.data;
				console.log("%cSuccessfully","color:blue;font-size:18px");
				console.dir(data_hotel_categoriess);
				$scope.data_hotel_categoriess = response.data;
				// TODO: hotel_catsCtrl --|---------- set:localforage
				localforage.setItem("data_hotel_categoriess_" + $scope.hashURL, JSON.stringify(data_hotel_categoriess));
				$scope.hotel_categoriess = [];
				for(lastPush = 0; lastPush < 9; lastPush++) {
					if (angular.isObject(data_hotel_categoriess[lastPush])){
						$scope.hotel_categoriess.push(data_hotel_categoriess[lastPush]);
					};
				}
			},function(response) {
			
				$timeout(function() {
					var url_request = $scope.fetchURLp.replace(targetQuery,raplaceWithQuery);
					// overwrite HTTP Header 
					http_header = {
						headers: {
							"Accept": "application/json",
						},
						params: http_params
					};
					console.log("%cRetrieving again: %c" + url_request,"color:blue;font-size:18px","color:red;font-size:18px");
					// TODO: hotel_catsCtrl --|------ $http.jsonp
					$http.jsonp(url_request,http_header).success(function(data){
						data_hotel_categoriess = data;
						$scope.data_hotel_categoriess = data;
						$ionicLoading.hide();
						// TODO: hotel_catsCtrl --|---------- set:localforage
						localforage.setItem("data_hotel_categoriess_" + $scope.hashURL,JSON.stringify(data_hotel_categoriess));
						controller_by_user();
						$scope.hotel_categoriess = [];
						for(lastPush = 0; lastPush < 9; lastPush++) {
							if (angular.isObject(data_hotel_categoriess[lastPush])){
								$scope.hotel_categoriess.push(data_hotel_categoriess[lastPush]);
							};
						}
					}).error(function(data){
					if(response.status ===401){
						// TODO: hotel_catsCtrl --|------------ error:Unauthorized
						$scope.showAuthentication();
					}else{
						// TODO: hotel_catsCtrl --|------------ error:Message
						var data = { statusText:response.statusText, status:response.status };
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
						$timeout(function() {
							alertPopup.close();
						}, 2000);
					}
					});
				}, 200);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
			}, 200);
		});
	
		}, 200);
	}
	
	
	// TODO: hotel_catsCtrl --|-- $scope.doRefresh
	$scope.doRefresh = function(){
		var url_request = $scope.fetchURL.replace(targetQuery,raplaceWithQuery);
		// retry retrieving data
		// overwrite http_header 
		http_header = {
			headers: {
				"Accept": "application/json",
			},
			params: http_params
		};
		// TODO: hotel_catsCtrl --|------ $http.get
		$http.get(url_request,http_header).then(function(response) {
			data_hotel_categoriess = response.data;
			$scope.data_hotel_categoriess = response.data;
			// TODO: hotel_catsCtrl --|---------- set:localforage
			localforage.setItem("data_hotel_categoriess_" + $scope.hashURL,JSON.stringify(data_hotel_categoriess));
			$scope.hotel_categoriess = [];
			for(lastPush = 0; lastPush < 9; lastPush++) {
				if (angular.isObject(data_hotel_categoriess[lastPush])){
					$scope.hotel_categoriess.push(data_hotel_categoriess[lastPush]);
				};
			}
		},function(response){
			
		// retrieving data with jsonp
			$timeout(function() {
			var url_request =$scope.fetchURLp.replace(targetQuery,raplaceWithQuery);
				// overwrite http_header 
				http_header = {
					headers: {
						"Accept": "application/json",
					},
					params: http_params
				};
				// TODO: hotel_catsCtrl --|---------- $http.jsonp
				$http.jsonp(url_request,http_header).success(function(data){
					data_hotel_categoriess = data;
					$scope.data_hotel_categoriess = data;
					$ionicLoading.hide();
					controller_by_user();
					// TODO: hotel_catsCtrl --|---------- set:localforage
					localforage.setItem("data_hotel_categoriess_"+ $scope.hashURL,JSON.stringify(data_hotel_categoriess));
					$scope.hotel_categoriess = [];
					for(lastPush = 0; lastPush < 9; lastPush++) {
						if (angular.isObject(data_hotel_categoriess[lastPush])){
							$scope.hotel_categoriess.push(data_hotel_categoriess[lastPush]);
						};
					}
				}).error(function(resp){
					if(response.status ===401){
					// TODO: hotel_catsCtrl --|------------ error:Unauthorized
					$scope.showAuthentication();
					}else{
						// TODO: hotel_catsCtrl --|------------ error:Message
						var data = { statusText:response.statusText, status:response.status };
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
					};
				});
			}, 200);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
			}, 500);
		});
	
	};
	if (data_hotel_categoriess === null){
		data_hotel_categoriess = [];
	};
	// animation readmore
	var fetchItems = function() {
		for(var z=0;z<fetch_per_scroll;z++){
			if (angular.isObject(data_hotel_categoriess[lastPush])){
				$scope.hotel_categoriess.push(data_hotel_categoriess[lastPush]);
				lastPush++;
			}else{;
				$scope.noMoreItemsAvailable = true;
			}
		}
		$scope.$broadcast("scroll.infiniteScrollComplete");
	};
	
	// event readmore
	$scope.onInfinite = function() {
		$timeout(fetchItems, 500);
	};
	
	// create animation fade slide in right (ionic-material)
	$scope.fireEvent = function(){
		ionicMaterialMotion.fadeSlideIn();
		ionicMaterialInk.displayEffect();
	};
	// code 

	// TODO: hotel_catsCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
//debug: all data
//console.log(data_hotel_categoriess);
$ionicConfig.backButton.text("");
			
		} catch(e){
			console.log("%cerror: %cPage: `hotel_cats` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: hotelsCtrl --|-- 
.controller("hotelsCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: hotelsCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "table (hotels)" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: hotelsCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: hotelsCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: hotelsCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: hotelsCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: hotelsCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: hotelsCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// TODO: hotelsCtrl --|-- $scope.showAuthentication
	$scope.showAuthentication  = function(){
		var authPopup = $ionicPopup.show({
			template: ' This page required login',
			title: "Authorization",
			subTitle: "Authorization is required",
			scope: $scope,
			buttons: [
				{text:"Cancel",onTap: function(e){
					$state.go("salasar.home");
				}},
			],
		}).then(function(form){
		},function(err){
		},function(msg){
		});
	};
	
	// set default parameter http
	var http_params = {};
	
	// set HTTP Header 
	var http_header = {
		headers: {
		},
		params: http_params
	};
	var targetQuery = ""; //default param
	var raplaceWithQuery = "";
	//fix url Hotels
	targetQuery = "hotel_categories_id=1"; //default param
	raplaceWithQuery = "hotel_categories_id=1";
		
	$scope.first_param = {};
	$scope.first_param.hotel_categories_id = "1";
	if(typeof $stateParams.hotel_categories_id !== 'undefined'){
		raplaceWithQuery = "hotel_categories_id=" + $stateParams.hotel_categories_id;
		$scope.first_param.hotel_categories_id = $stateParams.hotel_categories_id;
	}
	if(typeof $rootScope.hotelsQueryParam !== "undefined"){
		raplaceWithQuery = "hotel_categories_id=" +  $rootScope.hotelsQueryParam ;
	}
	if($scope.first_param.hotel_categories_id=="-1"){
		$scope.first_param.hotel_categories_id = "";
	}

	
	// TODO: hotelsCtrl --|-- $scope.splitArray
	$scope.splitArray = function(items,cols,maxItem) {
		var newItems = [];
		if(maxItem == 0){
			maxItem = items.length;
		}
		if(items){
			for (var i=0; i < maxItem; i+=cols) {
				newItems.push(items.slice(i, i+cols));
			}
		}
		return newItems;
	}
	// TODO: hotelsCtrl --|-- $scope.addToVirtual
	$scope.addToDbVirtual = function(newItem){
		var is_already_exist = false ;
		// animation loading 
		$ionicLoading.show();
		var virtual_items = []; 
		localforage.getItem("hotels_cart", function(err,dbVirtual){
			if(dbVirtual === null){
				virtual_items = [];
			}else{
				try{
					var last_items = JSON.parse(dbVirtual); 
				}catch(e){
					var last_items = [];
				}
				for(var z=0;z<last_items.length;z++){
					virtual_items.push(last_items[z]);
					if(newItem.hotel_categories_id ==  last_items[z].hotel_categories_id){
						is_already_exist = true;
					}
				}
			}
		}).then(function(value){
			if(is_already_exist === false){
				newItem["_qty"]=1;
				newItem["_sum"]=newItem.price;
				virtual_items.push(newItem);
			}
			localforage.setItem("hotels_cart",JSON.stringify(virtual_items));
			$timeout(function(){
				$ionicLoading.hide();
			},200);
		}).catch(function(err){
			virtual_items = [];
			virtual_items.push(newItem);
			localforage.setItem("hotels_cart",JSON.stringify(virtual_items));
			$timeout(function(){
				$ionicLoading.hide();
			},200);
		})
	};
	
	$scope.gmapOptions = {options: { scrollwheel: false }};
	
	var fetch_per_scroll = 2;
	// animation loading 
	$ionicLoading.show();
	
	
	// TODO: hotelsCtrl --|-- $scope.fetchURL
	$scope.fetchURL = "https://salasarbalaji.000webhostapp.com/rest-api.php?hotel_categories_id=1&json=hotels";
	// TODO: hotelsCtrl --|-- $scope.fetchURLp
	$scope.fetchURLp = "https://salasarbalaji.000webhostapp.com/rest-api.php?hotel_categories_id=1&json=hotels&callback=JSON_CALLBACK";
	// TODO: hotelsCtrl --|-- $scope.hashURL
	$scope.hashURL = md5.createHash( $scope.fetchURL.replace(targetQuery,raplaceWithQuery));
	
	
	$scope.noMoreItemsAvailable = false; //readmore status
	var lastPush = 0;
	var data_hotelss = [];
	
	localforage.getItem("data_hotelss_" + $scope.hashURL, function(err, get_hotelss){
		if(get_hotelss === null){
			data_hotelss =[];
		}else{
			data_hotelss = JSON.parse(get_hotelss);
			$scope.data_hotelss =JSON.parse( get_hotelss);
			$scope.hotelss = [];
			for(lastPush = 0; lastPush < 5; lastPush++) {
				if (angular.isObject(data_hotelss[lastPush])){
					$scope.hotelss.push(data_hotelss[lastPush]);
				};
			}
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
			},200);
		}
	}).then(function(value){
	}).catch(function (err){
	})
	if(data_hotelss === null ){
		data_hotelss =[];
	}
	if(data_hotelss.length === 0 ){
		$timeout(function() {
			var url_request = $scope.fetchURL.replace(targetQuery,raplaceWithQuery);
			// overwrite HTTP Header 
			http_header = {
				headers: {
				},
				params: http_params
			};
			// TODO: hotelsCtrl --|-- $http.get
			console.log("%cRetrieving JSON: %c" + url_request,"color:blue;font-size:18px","color:red;font-size:18px");
			$http.get(url_request,http_header).then(function(response) {
				data_hotelss = response.data;
				console.log("%cSuccessfully","color:blue;font-size:18px");
				console.dir(data_hotelss);
				$scope.data_hotelss = response.data;
				// TODO: hotelsCtrl --|---------- set:localforage
				localforage.setItem("data_hotelss_" + $scope.hashURL, JSON.stringify(data_hotelss));
				$scope.hotelss = [];
				for(lastPush = 0; lastPush < 5; lastPush++) {
					if (angular.isObject(data_hotelss[lastPush])){
						$scope.hotelss.push(data_hotelss[lastPush]);
					};
				}
			},function(response) {
			
				$timeout(function() {
					var url_request = $scope.fetchURLp.replace(targetQuery,raplaceWithQuery);
					// overwrite HTTP Header 
					http_header = {
						headers: {
						},
						params: http_params
					};
					console.log("%cRetrieving again: %c" + url_request,"color:blue;font-size:18px","color:red;font-size:18px");
					// TODO: hotelsCtrl --|------ $http.jsonp
					$http.jsonp(url_request,http_header).success(function(data){
						data_hotelss = data;
						$scope.data_hotelss = data;
						$ionicLoading.hide();
						// TODO: hotelsCtrl --|---------- set:localforage
						localforage.setItem("data_hotelss_" + $scope.hashURL,JSON.stringify(data_hotelss));
						controller_by_user();
						$scope.hotelss = [];
						for(lastPush = 0; lastPush < 5; lastPush++) {
							if (angular.isObject(data_hotelss[lastPush])){
								$scope.hotelss.push(data_hotelss[lastPush]);
							};
						}
					}).error(function(data){
					if(response.status ===401){
						// TODO: hotelsCtrl --|------------ error:Unauthorized
						$scope.showAuthentication();
					}else{
						// TODO: hotelsCtrl --|------------ error:Message
						var data = { statusText:response.statusText, status:response.status };
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
						$timeout(function() {
							alertPopup.close();
						}, 2000);
					}
					});
				}, 200);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
			}, 200);
		});
	
		}, 200);
	}
	
	
	// TODO: hotelsCtrl --|-- $scope.doRefresh
	$scope.doRefresh = function(){
		var url_request = $scope.fetchURL.replace(targetQuery,raplaceWithQuery);
		// retry retrieving data
		// overwrite http_header 
		http_header = {
			headers: {
			},
			params: http_params
		};
		// TODO: hotelsCtrl --|------ $http.get
		$http.get(url_request,http_header).then(function(response) {
			data_hotelss = response.data;
			$scope.data_hotelss = response.data;
			// TODO: hotelsCtrl --|---------- set:localforage
			localforage.setItem("data_hotelss_" + $scope.hashURL,JSON.stringify(data_hotelss));
			$scope.hotelss = [];
			for(lastPush = 0; lastPush < 5; lastPush++) {
				if (angular.isObject(data_hotelss[lastPush])){
					$scope.hotelss.push(data_hotelss[lastPush]);
				};
			}
		},function(response){
			
		// retrieving data with jsonp
			$timeout(function() {
			var url_request =$scope.fetchURLp.replace(targetQuery,raplaceWithQuery);
				// overwrite http_header 
				http_header = {
					headers: {
					},
					params: http_params
				};
				// TODO: hotelsCtrl --|---------- $http.jsonp
				$http.jsonp(url_request,http_header).success(function(data){
					data_hotelss = data;
					$scope.data_hotelss = data;
					$ionicLoading.hide();
					controller_by_user();
					// TODO: hotelsCtrl --|---------- set:localforage
					localforage.setItem("data_hotelss_"+ $scope.hashURL,JSON.stringify(data_hotelss));
					$scope.hotelss = [];
					for(lastPush = 0; lastPush < 5; lastPush++) {
						if (angular.isObject(data_hotelss[lastPush])){
							$scope.hotelss.push(data_hotelss[lastPush]);
						};
					}
				}).error(function(resp){
					if(response.status ===401){
					// TODO: hotelsCtrl --|------------ error:Unauthorized
					$scope.showAuthentication();
					}else{
						// TODO: hotelsCtrl --|------------ error:Message
						var data = { statusText:response.statusText, status:response.status };
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
					};
				});
			}, 200);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
			}, 500);
		});
	
	};
	if (data_hotelss === null){
		data_hotelss = [];
	};
	// animation readmore
	var fetchItems = function() {
		for(var z=0;z<fetch_per_scroll;z++){
			if (angular.isObject(data_hotelss[lastPush])){
				$scope.hotelss.push(data_hotelss[lastPush]);
				lastPush++;
			}else{;
				$scope.noMoreItemsAvailable = true;
			}
		}
		$scope.$broadcast("scroll.infiniteScrollComplete");
	};
	
	// event readmore
	$scope.onInfinite = function() {
		$timeout(fetchItems, 500);
	};
	
	// create animation fade slide in right (ionic-material)
	$scope.fireEvent = function(){
		ionicMaterialMotion.blinds();
		ionicMaterialInk.displayEffect();
	};
	// code 

	// TODO: hotelsCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
//debug: all data
//console.log(data_hotelss);
$ionicConfig.backButton.text("");
			
		} catch(e){
			console.log("%cerror: %cPage: `hotels` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: hotels_cartCtrl --|-- 
.controller("hotels_cartCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: hotels_cartCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "table (hotels)" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: hotels_cartCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: hotels_cartCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: hotels_cartCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: hotels_cartCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: hotels_cartCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: hotels_cartCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// TODO: hotels_cartCtrl --|-- Database for Cart/Bookmark
	// TODO: hotels_cartCtrl --|-- $scope.loadDbVirtualCabs
	 
	$scope.loadDbVirtualCabs = function(){
		$ionicLoading.show();
		//hotels_cart
		$scope.cabs_cart = []; 
		$scope.cabs_cost = 0;
		localforage.getItem("cabs_cart", function(err,dbVirtual){
			if(dbVirtual === null){
				$scope.cabs_cart = []; 
			}else{
				try{
					$scope.cabs_cart = JSON.parse(dbVirtual); 
		var total_cost = 0;
		angular.forEach($scope.cabs_cart, function(item, key) {
			var cost = item._qty * item.none;
			total_cost += cost;
		});
		$scope.cabs_cost = total_cost;
				}catch (e){
					$scope.cabs_cart = []; 
				}
			}
		}).then(function(value){
			$timeout(function(){
				$ionicLoading.hide();
			},200);
		}).catch(function(err){
			console.log(err);
			$timeout(function(){
				$ionicLoading.hide();
			},200);
		});
	};
	$scope.$on("$ionicView.enter", function (){
		$scope.loadDbVirtualCabs();
	});
	// TODO: hotels_cartCtrl --|-- $scope.clearDbVirtualCabs
	$scope.clearDbVirtualCabs = function(){
		localforage.setItem("cabs_cart",[]);
		$timeout(function(){
			$scope.loadDbVirtualCabs();
		},200);
	};
	// TODO: hotels_cartCtrl --|-- $scope.removeDbVirtualCabs
	$scope.removeDbVirtualCabs = function(itemId){
		var virtual_items = [];
		var last_items = $scope.cabs_cart;
		for(var z=0;z<last_items.length;z++){
			if(itemId!==last_items[z].id){
				virtual_items.push(last_items[z]);
			}
		}
		localforage.setItem("cabs_cart",JSON.stringify(virtual_items));
		$timeout(function(){
			$scope.loadDbVirtualCabs();
		},200);
	};
	// TODO: hotels_cartCtrl --|-- $scope.updateDbVirtual();
	$scope.updateDbVirtual = function(){
		var virtual_items = [];
		var total_cost = 0;
		angular.forEach($scope.cabs_cart, function(item, key) {
			var cost = item._qty * item.none;
			item._sum = cost;
			this.push(item);
			total_cost += cost;
		},virtual_items);
		localforage.setItem("cabs_cart",JSON.stringify(virtual_items));
		$scope.cabs_cost = total_cost;
	};
	
	// TODO: hotels_cartCtrl --|-- Database for Cart/Bookmark
	// TODO: hotels_cartCtrl --|-- $scope.loadDbVirtualHotels
	 
	$scope.loadDbVirtualHotels = function(){
		$ionicLoading.show();
		//hotels_cart
		$scope.hotels_cart = []; 
		$scope.hotels_cost = 0;
		localforage.getItem("hotels_cart", function(err,dbVirtual){
			if(dbVirtual === null){
				$scope.hotels_cart = []; 
			}else{
				try{
					$scope.hotels_cart = JSON.parse(dbVirtual); 
		var total_cost = 0;
		angular.forEach($scope.hotels_cart, function(item, key) {
			var cost = item._qty * item.price;
			total_cost += cost;
		});
		$scope.hotels_cost = total_cost;
				}catch (e){
					$scope.hotels_cart = []; 
				}
			}
		}).then(function(value){
			$timeout(function(){
				$ionicLoading.hide();
			},200);
		}).catch(function(err){
			console.log(err);
			$timeout(function(){
				$ionicLoading.hide();
			},200);
		});
	};
	$scope.$on("$ionicView.enter", function (){
		$scope.loadDbVirtualHotels();
	});
	// TODO: hotels_cartCtrl --|-- $scope.clearDbVirtualHotels
	$scope.clearDbVirtualHotels = function(){
		localforage.setItem("hotels_cart",[]);
		$timeout(function(){
			$scope.loadDbVirtualHotels();
		},200);
	};
	// TODO: hotels_cartCtrl --|-- $scope.removeDbVirtualHotels
	$scope.removeDbVirtualHotels = function(itemId){
		var virtual_items = [];
		var last_items = $scope.hotels_cart;
		for(var z=0;z<last_items.length;z++){
			if(itemId!==last_items[z].id){
				virtual_items.push(last_items[z]);
			}
		}
		localforage.setItem("hotels_cart",JSON.stringify(virtual_items));
		$timeout(function(){
			$scope.loadDbVirtualHotels();
		},200);
	};
	// TODO: hotels_cartCtrl --|-- $scope.updateDbVirtual();
	$scope.updateDbVirtual = function(){
		var virtual_items = [];
		var total_cost = 0;
		angular.forEach($scope.hotels_cart, function(item, key) {
			var cost = item._qty * item.price;
			item._sum = cost;
			this.push(item);
			total_cost += cost;
		},virtual_items);
		localforage.setItem("hotels_cart",JSON.stringify(virtual_items));
		$scope.hotels_cost = total_cost;
	};
	
	// TODO: hotels_cartCtrl --|-- Database for Cart/Bookmark
	// TODO: hotels_cartCtrl --|-- $scope.loadDbVirtualProducts
	 
	$scope.loadDbVirtualProducts = function(){
		$ionicLoading.show();
		//hotels_cart
		$scope.products_cart = []; 
		$scope.products_cost = 0;
		localforage.getItem("products_cart", function(err,dbVirtual){
			if(dbVirtual === null){
				$scope.products_cart = []; 
			}else{
				try{
					$scope.products_cart = JSON.parse(dbVirtual); 
		var total_cost = 0;
		angular.forEach($scope.products_cart, function(item, key) {
			var cost = item._qty * item.price;
			total_cost += cost;
		});
		$scope.products_cost = total_cost;
				}catch (e){
					$scope.products_cart = []; 
				}
			}
		}).then(function(value){
			$timeout(function(){
				$ionicLoading.hide();
			},200);
		}).catch(function(err){
			console.log(err);
			$timeout(function(){
				$ionicLoading.hide();
			},200);
		});
	};
	$scope.$on("$ionicView.enter", function (){
		$scope.loadDbVirtualProducts();
	});
	// TODO: hotels_cartCtrl --|-- $scope.clearDbVirtualProducts
	$scope.clearDbVirtualProducts = function(){
		localforage.setItem("products_cart",[]);
		$timeout(function(){
			$scope.loadDbVirtualProducts();
		},200);
	};
	// TODO: hotels_cartCtrl --|-- $scope.removeDbVirtualProducts
	$scope.removeDbVirtualProducts = function(itemId){
		var virtual_items = [];
		var last_items = $scope.products_cart;
		for(var z=0;z<last_items.length;z++){
			if(itemId!==last_items[z].id){
				virtual_items.push(last_items[z]);
			}
		}
		localforage.setItem("products_cart",JSON.stringify(virtual_items));
		$timeout(function(){
			$scope.loadDbVirtualProducts();
		},200);
	};
	// TODO: hotels_cartCtrl --|-- $scope.updateDbVirtual();
	$scope.updateDbVirtual = function(){
		var virtual_items = [];
		var total_cost = 0;
		angular.forEach($scope.products_cart, function(item, key) {
			var cost = item._qty * item.price;
			item._sum = cost;
			this.push(item);
			total_cost += cost;
		},virtual_items);
		localforage.setItem("products_cart",JSON.stringify(virtual_items));
		$scope.products_cost = total_cost;
	};
	
	// code 

	// TODO: hotels_cartCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
	$scope.gotoCheckout = function(){
		alert("you must create payment gateway manual");
	}
			
		} catch(e){
			console.log("%cerror: %cPage: `hotels_cart` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: hotels_singlesCtrl --|-- 
.controller("hotels_singlesCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: hotels_singlesCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "table (hotels)" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: hotels_singlesCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: hotels_singlesCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: hotels_singlesCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: hotels_singlesCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: hotels_singlesCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: hotels_singlesCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// TODO: hotels_singlesCtrl --|-- $scope.showAuthentication
	$scope.showAuthentication  = function(){
		var authPopup = $ionicPopup.show({
			template: ' This page required login',
			title: "Authorization",
			subTitle: "Authorization is required",
			scope: $scope,
			buttons: [
				{text:"Cancel",onTap: function(e){
					$state.go("salasar.home");
				}},
			],
		}).then(function(form){
		},function(err){
		},function(msg){
		});
	};
	// TODO: hotels_singlesCtrl --|-- $scope.addToVirtual(); //data single
	$scope.addToDbVirtual = function(newItem){
		var is_already_exist = false ;
		// animation loading 
		$ionicLoading.show();
		var virtual_items = []; 
		localforage.getItem("hotels_cart", function(err,dbVirtual){
			if(dbVirtual === null){
				virtual_items = [];
			}else{
				try{
					var last_items = JSON.parse(dbVirtual); 
				}catch(e){
					var last_items = [];
				}
				for(var z=0;z<last_items.length;z++){
					virtual_items.push(last_items[z]);
					if(newItem.id ==  last_items[z].id){
						is_already_exist = true;
					}
				}
			}
		}).then(function(value){
			if(is_already_exist === false){
				newItem["_qty"]=1;
				newItem["_sum"]=newItem.price;
				virtual_items.push(newItem);
			}
			localforage.setItem("hotels_cart",JSON.stringify(virtual_items));
			$timeout(function(){
				$ionicLoading.hide();
			},200);
		}).catch(function(err){
			virtual_items = [];
			virtual_items.push(newItem);
			localforage.setItem("hotels_cart",JSON.stringify(virtual_items));
			$timeout(function(){
				$ionicLoading.hide();
			},200);
		})
	};
	
	
	// set default parameter http
	var http_params = {};

	// set HTTP Header 
	var http_header = {
		headers: {
		},
		params: http_params
	};
	// animation loading 
	$ionicLoading.show();
	
	// Retrieving data
	var itemID = $stateParams.id;
	// TODO: hotels_singlesCtrl --|-- $scope.fetchURL
	$scope.fetchURL = "https://salasarbalaji.000webhostapp.com/rest-api.php?json=hotels&id=" + itemID;
	// TODO: hotels_singlesCtrl --|-- $scope.fetchURLp
	$scope.fetchURLp = "https://salasarbalaji.000webhostapp.com/rest-api.php?json=hotels&id=" + itemID + "&callback=JSON_CALLBACK";
	// TODO: hotels_singlesCtrl --|-- $scope.hashURL
	$scope.hashURL = md5.createHash($scope.fetchURL);
	
	var current_item = [];
	localforage.getItem("data_hotels_single_" + $scope.hashURL, function(err, get_datas){
		if(get_datas === null){
			current_item = [];
		}else{
			if(get_datas !== null){
				current_item = JSON.parse(get_datas);
				$timeout(function(){
					$ionicLoading.hide();
					$scope.hotels = current_item ;
					controller_by_user();
				}, 500);
			};
		};
	}).then(function(value){
	}).catch(function (err){
	})
	if( current_item.length === 0 ){
		var itemID = $stateParams.id;
		var current_item = [];
	
		// set HTTP Header 
		http_header = {
			headers: {
			},
			params: http_params
		};
		// TODO: hotels_singlesCtrl --|-- $http.get
		$http.get($scope.fetchURL,http_header).then(function(response) {
			// Get data single
			var datas = response.data;
			// TODO: hotels_singlesCtrl --|---------- set:localforage
			localforage.setItem("data_hotels_single_" + $scope.hashURL,JSON.stringify(datas));
			current_item = datas ;
		},function(data) {
					// Error message
					var alertPopup = $ionicPopup.alert({
						title: "Network Error" + " (" + data.status + ")",
						template: "An error occurred while collecting data.",
					});
					$timeout(function() {
						alertPopup.close();
					}, 2000);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				$scope.hotels = current_item ;
				controller_by_user();
			}, 500);
		});
	}
	
	
		// TODO: hotels_singlesCtrl --|-- $scope.doRefresh
	$scope.doRefresh = function(){
		// Retrieving data
		var itemID = $stateParams.id;
		var current_item = [];
		// overwrite http_header 
		http_header = {
			headers: {
			},
			params: http_params
		};
		// TODO: hotels_singlesCtrl --|------ $http.get
		$http.get($scope.fetchURL,http_header).then(function(response) {
			// Get data single
			var datas = response.data;
			// TODO: hotels_singlesCtrl --|---------- set:localforage
			localforage.setItem("data_hotels_single_" + $scope.hashURL,JSON.stringify(datas));
			current_item = datas ;
		},function(data) {
			// Error message
		// TODO: hotels_singlesCtrl --|---------- $http.jsonp
				$http.jsonp($scope.fetchURLp,http_header).success(function(response){
					// Get data single
					var datas = response;
			// TODO: hotels_singlesCtrl --|---------- set:localforage
			localforage.setItem("data_hotels_single_" + $scope.hashURL,JSON.stringify(datas));
					current_item = datas ;
						$scope.$broadcast("scroll.refreshComplete");
						// event done, hidden animation loading
						$timeout(function() {
							$ionicLoading.hide();
							$scope.hotels = current_item ;
							controller_by_user();
						}, 500);
					}).error(function(resp){
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
					});
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				$scope.hotels = current_item ;
				controller_by_user();
			}, 500);
		});
	};
	// code 

	// TODO: hotels_singlesCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
$ionicConfig.backButton.text("");			
		} catch(e){
			console.log("%cerror: %cPage: `hotels_singles` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: live_darshanCtrl --|-- 
.controller("live_darshanCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: live_darshanCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "table (livedarshan)" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: live_darshanCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: live_darshanCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: live_darshanCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: live_darshanCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: live_darshanCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: live_darshanCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// TODO: live_darshanCtrl --|-- $scope.showAuthentication
	$scope.showAuthentication  = function(){
		var authPopup = $ionicPopup.show({
			template: ' This page required login',
			title: "Authorization",
			subTitle: "Authorization is required",
			scope: $scope,
			buttons: [
				{text:"Cancel",onTap: function(e){
					$state.go("salasar.home");
				}},
			],
		}).then(function(form){
		},function(err){
		},function(msg){
		});
	};
	
	// set default parameter http
	var http_params = {};
	
	// set HTTP Header 
	var http_header = {
		headers: {
		},
		params: http_params
	};
	var targetQuery = ""; //default param
	var raplaceWithQuery = "";
	//fix url Live Darshan
	targetQuery = "json=livedarshan"; //default param
	raplaceWithQuery = "json=livedarshan";
	
	
	// TODO: live_darshanCtrl --|-- $scope.splitArray
	$scope.splitArray = function(items,cols,maxItem) {
		var newItems = [];
		if(maxItem == 0){
			maxItem = items.length;
		}
		if(items){
			for (var i=0; i < maxItem; i+=cols) {
				newItems.push(items.slice(i, i+cols));
			}
		}
		return newItems;
	}
	$scope.gmapOptions = {options: { scrollwheel: false }};
	
	var fetch_per_scroll = 1;
	// animation loading 
	$ionicLoading.show();
	
	
	// TODO: live_darshanCtrl --|-- $scope.fetchURL
	$scope.fetchURL = "https://salasarbalaji.000webhostapp.com/rest-api.php?json=livedarshan";
	// TODO: live_darshanCtrl --|-- $scope.fetchURLp
	$scope.fetchURLp = "https://salasarbalaji.000webhostapp.com/rest-api.php?json=livedarshan&callback=JSON_CALLBACK";
	// TODO: live_darshanCtrl --|-- $scope.hashURL
	$scope.hashURL = md5.createHash( $scope.fetchURL.replace(targetQuery,raplaceWithQuery));
	
	
	$scope.noMoreItemsAvailable = false; //readmore status
	var lastPush = 0;
	var data_livedarshans = [];
	
	localforage.getItem("data_livedarshans_" + $scope.hashURL, function(err, get_livedarshans){
		if(get_livedarshans === null){
			data_livedarshans =[];
		}else{
			data_livedarshans = JSON.parse(get_livedarshans);
			$scope.data_livedarshans =JSON.parse( get_livedarshans);
			$timeout(function() {
				$ionicLoading.hide();
				if(data_livedarshans){
					$scope.chunked_livedarshans = $scope.splitArray(data_livedarshans, $rootScope.grid128,0);
				}
				controller_by_user();
			},200);
		}
	}).then(function(value){
	}).catch(function (err){
	})
	if(data_livedarshans === null ){
		data_livedarshans =[];
	}
	if(data_livedarshans.length === 0 ){
		$timeout(function() {
			var url_request = $scope.fetchURL.replace(targetQuery,raplaceWithQuery);
			// overwrite HTTP Header 
			http_header = {
				headers: {
				},
				params: http_params
			};
			// TODO: live_darshanCtrl --|-- $http.get
			console.log("%cRetrieving JSON: %c" + url_request,"color:blue;font-size:18px","color:red;font-size:18px");
			$http.get(url_request,http_header).then(function(response) {
				data_livedarshans = response.data;
				console.log("%cSuccessfully","color:blue;font-size:18px");
				console.dir(data_livedarshans);
				$scope.data_livedarshans = response.data;
				// TODO: live_darshanCtrl --|---------- set:localforage
				localforage.setItem("data_livedarshans_" + $scope.hashURL, JSON.stringify(data_livedarshans));
			},function(response) {
			
				$timeout(function() {
					var url_request = $scope.fetchURLp.replace(targetQuery,raplaceWithQuery);
					// overwrite HTTP Header 
					http_header = {
						headers: {
						},
						params: http_params
					};
					console.log("%cRetrieving again: %c" + url_request,"color:blue;font-size:18px","color:red;font-size:18px");
					// TODO: live_darshanCtrl --|------ $http.jsonp
					$http.jsonp(url_request,http_header).success(function(data){
						data_livedarshans = data;
						$scope.data_livedarshans = data;
						$ionicLoading.hide();
						// TODO: live_darshanCtrl --|---------- set:localforage
						localforage.setItem("data_livedarshans_" + $scope.hashURL,JSON.stringify(data_livedarshans));
						controller_by_user();
					}).error(function(data){
					if(response.status ===401){
						// TODO: live_darshanCtrl --|------------ error:Unauthorized
						$scope.showAuthentication();
					}else{
						// TODO: live_darshanCtrl --|------------ error:Message
						var data = { statusText:response.statusText, status:response.status };
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
						$timeout(function() {
							alertPopup.close();
						}, 2000);
					}
					});
				}, 200);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
				$scope.chunked_livedarshans = $scope.splitArray(data_livedarshans, $rootScope.grid128,0);
			}, 200);
		});
	
		}, 200);
	}
	
	
	// TODO: live_darshanCtrl --|-- $scope.doRefresh
	$scope.doRefresh = function(){
		var url_request = $scope.fetchURL.replace(targetQuery,raplaceWithQuery);
		// retry retrieving data
		// overwrite http_header 
		http_header = {
			headers: {
			},
			params: http_params
		};
		// TODO: live_darshanCtrl --|------ $http.get
		$http.get(url_request,http_header).then(function(response) {
			data_livedarshans = response.data;
			$scope.data_livedarshans = response.data;
			// TODO: live_darshanCtrl --|---------- set:localforage
			localforage.setItem("data_livedarshans_" + $scope.hashURL,JSON.stringify(data_livedarshans));
		},function(response){
			
		// retrieving data with jsonp
			$timeout(function() {
			var url_request =$scope.fetchURLp.replace(targetQuery,raplaceWithQuery);
				// overwrite http_header 
				http_header = {
					headers: {
					},
					params: http_params
				};
				// TODO: live_darshanCtrl --|---------- $http.jsonp
				$http.jsonp(url_request,http_header).success(function(data){
					data_livedarshans = data;
					$scope.data_livedarshans = data;
					$ionicLoading.hide();
					controller_by_user();
					// TODO: live_darshanCtrl --|---------- set:localforage
					localforage.setItem("data_livedarshans_"+ $scope.hashURL,JSON.stringify(data_livedarshans));
				}).error(function(resp){
					if(response.status ===401){
					// TODO: live_darshanCtrl --|------------ error:Unauthorized
					$scope.showAuthentication();
					}else{
						// TODO: live_darshanCtrl --|------------ error:Message
						var data = { statusText:response.statusText, status:response.status };
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
					};
				});
			}, 200);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
				if(data_livedarshans){
					$scope.chunked_livedarshans = $scope.splitArray(data_livedarshans, $rootScope.grid128,0);
				}
			}, 500);
		});
	
	};
	// code 

	// TODO: live_darshanCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
//debug: all data
//console.log(data_livedarshans);
$ionicConfig.backButton.text("");
			
		} catch(e){
			console.log("%cerror: %cPage: `live_darshan` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: livedarshan_singlesCtrl --|-- 
.controller("livedarshan_singlesCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: livedarshan_singlesCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "table (livedarshan)" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: livedarshan_singlesCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: livedarshan_singlesCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: livedarshan_singlesCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: livedarshan_singlesCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: livedarshan_singlesCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: livedarshan_singlesCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// TODO: livedarshan_singlesCtrl --|-- $scope.showAuthentication
	$scope.showAuthentication  = function(){
		var authPopup = $ionicPopup.show({
			template: ' This page required login',
			title: "Authorization",
			subTitle: "Authorization is required",
			scope: $scope,
			buttons: [
				{text:"Cancel",onTap: function(e){
					$state.go("salasar.home");
				}},
			],
		}).then(function(form){
		},function(err){
		},function(msg){
		});
	};
	
	// set default parameter http
	var http_params = {};

	// set HTTP Header 
	var http_header = {
		headers: {
		},
		params: http_params
	};
	// animation loading 
	$ionicLoading.show();
	
	// Retrieving data
	var itemID = $stateParams.id;
	// TODO: livedarshan_singlesCtrl --|-- $scope.fetchURL
	$scope.fetchURL = "https://salasarbalaji.000webhostapp.com/rest-api.php?json=livedarshan" + itemID;
	// TODO: livedarshan_singlesCtrl --|-- $scope.fetchURLp
	$scope.fetchURLp = "https://salasarbalaji.000webhostapp.com/rest-api.php?json=livedarshan" + itemID + "&callback=JSON_CALLBACK";
	// TODO: livedarshan_singlesCtrl --|-- $scope.hashURL
	$scope.hashURL = md5.createHash($scope.fetchURL);
	
	var current_item = [];
	localforage.getItem("data_livedarshan_single_" + $scope.hashURL, function(err, get_datas){
		if(get_datas === null){
			current_item = [];
		}else{
			if(get_datas !== null){
				current_item = JSON.parse(get_datas);
				$timeout(function(){
					$ionicLoading.hide();
					$scope.livedarshan = current_item ;
					controller_by_user();
				}, 500);
			};
		};
	}).then(function(value){
	}).catch(function (err){
	})
	if( current_item.length === 0 ){
		var itemID = $stateParams.id;
		var current_item = [];
	
		// set HTTP Header 
		http_header = {
			headers: {
			},
			params: http_params
		};
		// TODO: livedarshan_singlesCtrl --|-- $http.get
		$http.get($scope.fetchURL,http_header).then(function(response) {
			// Get data single
			var datas = response.data;
			// TODO: livedarshan_singlesCtrl --|---------- set:localforage
			localforage.setItem("data_livedarshan_single_" + $scope.hashURL,JSON.stringify(datas));
			current_item = datas ;
		},function(data) {
					// Error message
					var alertPopup = $ionicPopup.alert({
						title: "Network Error" + " (" + data.status + ")",
						template: "An error occurred while collecting data.",
					});
					$timeout(function() {
						alertPopup.close();
					}, 2000);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				$scope.livedarshan = current_item ;
				controller_by_user();
			}, 500);
		});
	}
	
	
		// TODO: livedarshan_singlesCtrl --|-- $scope.doRefresh
	$scope.doRefresh = function(){
		// Retrieving data
		var itemID = $stateParams.id;
		var current_item = [];
		// overwrite http_header 
		http_header = {
			headers: {
			},
			params: http_params
		};
		// TODO: livedarshan_singlesCtrl --|------ $http.get
		$http.get($scope.fetchURL,http_header).then(function(response) {
			// Get data single
			var datas = response.data;
			// TODO: livedarshan_singlesCtrl --|---------- set:localforage
			localforage.setItem("data_livedarshan_single_" + $scope.hashURL,JSON.stringify(datas));
			current_item = datas ;
		},function(data) {
			// Error message
		// TODO: livedarshan_singlesCtrl --|---------- $http.jsonp
				$http.jsonp($scope.fetchURLp,http_header).success(function(response){
					// Get data single
					var datas = response;
			// TODO: livedarshan_singlesCtrl --|---------- set:localforage
			localforage.setItem("data_livedarshan_single_" + $scope.hashURL,JSON.stringify(datas));
					current_item = datas ;
						$scope.$broadcast("scroll.refreshComplete");
						// event done, hidden animation loading
						$timeout(function() {
							$ionicLoading.hide();
							$scope.livedarshan = current_item ;
							controller_by_user();
						}, 500);
					}).error(function(resp){
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
					});
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				$scope.livedarshan = current_item ;
				controller_by_user();
			}, 500);
		});
	};
	// code 

	// TODO: livedarshan_singlesCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
$ionicConfig.backButton.text("");			
		} catch(e){
			console.log("%cerror: %cPage: `livedarshan_singles` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: mantra_japCtrl --|-- 
.controller("mantra_japCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){

	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: mantra_japCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "menu" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: mantra_japCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: mantra_japCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: mantra_japCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: mantra_japCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: mantra_japCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: mantra_japCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// code 

	// TODO: mantra_japCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
			
		} catch(e){
			console.log("%cerror: %cPage: `mantra_jap` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: membershipCtrl --|-- 
.controller("membershipCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: membershipCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "menu" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: membershipCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: membershipCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: membershipCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: membershipCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: membershipCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: membershipCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// code 

	// TODO: membershipCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
			
		} catch(e){
			console.log("%cerror: %cPage: `membership` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: micro_cabsCtrl --|-- 
.controller("micro_cabsCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: micro_cabsCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "table (cabs)" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: micro_cabsCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: micro_cabsCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: micro_cabsCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: micro_cabsCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: micro_cabsCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: micro_cabsCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// code 

	// TODO: micro_cabsCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
//debug: all data
//console.log(data_cabss);
$ionicConfig.backButton.text("");
			
		} catch(e){
			console.log("%cerror: %cPage: `micro_cabs` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: minicabs_singlesCtrl --|-- 
.controller("minicabs_singlesCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: minicabs_singlesCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "table (minicabs)" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: minicabs_singlesCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: minicabs_singlesCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: minicabs_singlesCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: minicabs_singlesCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: minicabs_singlesCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: minicabs_singlesCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// TODO: minicabs_singlesCtrl --|-- $scope.showAuthentication
	$scope.showAuthentication  = function(){
		var authPopup = $ionicPopup.show({
			template: ' This page required login',
			title: "Authorization",
			subTitle: "Authorization is required",
			scope: $scope,
			buttons: [
				{text:"Cancel",onTap: function(e){
					$state.go("salasar.home");
				}},
			],
		}).then(function(form){
		},function(err){
		},function(msg){
		});
	};
	
	// set default parameter http
	var http_params = {};

	// set HTTP Header 
	var http_header = {
		headers: {
		},
		params: http_params
	};
	// animation loading 
	$ionicLoading.show();
	
	// Retrieving data
	var itemID = $stateParams.id;
	// TODO: minicabs_singlesCtrl --|-- $scope.fetchURL
	$scope.fetchURL = "https://salasarbalaji.000webhostapp.com/rest-api.php?json=cabs&id=" + itemID;
	// TODO: minicabs_singlesCtrl --|-- $scope.fetchURLp
	$scope.fetchURLp = "https://salasarbalaji.000webhostapp.com/rest-api.php?json=cabs&id=" + itemID + "&callback=JSON_CALLBACK";
	// TODO: minicabs_singlesCtrl --|-- $scope.hashURL
	$scope.hashURL = md5.createHash($scope.fetchURL);
	
	var current_item = [];
	localforage.getItem("data_minicabs_single_" + $scope.hashURL, function(err, get_datas){
		if(get_datas === null){
			current_item = [];
		}else{
			if(get_datas !== null){
				current_item = JSON.parse(get_datas);
				$timeout(function(){
					$ionicLoading.hide();
					$scope.minicabs = current_item ;
					controller_by_user();
				}, 500);
			};
		};
	}).then(function(value){
	}).catch(function (err){
	})
	if( current_item.length === 0 ){
		var itemID = $stateParams.id;
		var current_item = [];
	
		// set HTTP Header 
		http_header = {
			headers: {
			},
			params: http_params
		};
		// TODO: minicabs_singlesCtrl --|-- $http.get
		$http.get($scope.fetchURL,http_header).then(function(response) {
			// Get data single
			var datas = response.data;
			// TODO: minicabs_singlesCtrl --|---------- set:localforage
			localforage.setItem("data_minicabs_single_" + $scope.hashURL,JSON.stringify(datas));
			current_item = datas ;
		},function(data) {
					// Error message
					var alertPopup = $ionicPopup.alert({
						title: "Network Error" + " (" + data.status + ")",
						template: "An error occurred while collecting data.",
					});
					$timeout(function() {
						alertPopup.close();
					}, 2000);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				$scope.minicabs = current_item ;
				controller_by_user();
			}, 500);
		});
	}
	
	
		// TODO: minicabs_singlesCtrl --|-- $scope.doRefresh
	$scope.doRefresh = function(){
		// Retrieving data
		var itemID = $stateParams.id;
		var current_item = [];
		// overwrite http_header 
		http_header = {
			headers: {
			},
			params: http_params
		};
		// TODO: minicabs_singlesCtrl --|------ $http.get
		$http.get($scope.fetchURL,http_header).then(function(response) {
			// Get data single
			var datas = response.data;
			// TODO: minicabs_singlesCtrl --|---------- set:localforage
			localforage.setItem("data_minicabs_single_" + $scope.hashURL,JSON.stringify(datas));
			current_item = datas ;
		},function(data) {
			// Error message
		// TODO: minicabs_singlesCtrl --|---------- $http.jsonp
				$http.jsonp($scope.fetchURLp,http_header).success(function(response){
					// Get data single
					var datas = response;
			// TODO: minicabs_singlesCtrl --|---------- set:localforage
			localforage.setItem("data_minicabs_single_" + $scope.hashURL,JSON.stringify(datas));
					current_item = datas ;
						$scope.$broadcast("scroll.refreshComplete");
						// event done, hidden animation loading
						$timeout(function() {
							$ionicLoading.hide();
							$scope.minicabs = current_item ;
							controller_by_user();
						}, 500);
					}).error(function(resp){
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
					});
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				$scope.minicabs = current_item ;
				controller_by_user();
			}, 500);
		});
	};
	// code 

	// TODO: minicabs_singlesCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
$ionicConfig.backButton.text("");			
		} catch(e){
			console.log("%cerror: %cPage: `minicabs_singles` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: minicarCtrl --|-- 
.controller("minicarCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: minicarCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "table (cabs)" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: minicarCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: minicarCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: minicarCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: minicarCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: minicarCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: minicarCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// TODO: minicarCtrl --|-- $scope.showAuthentication
	$scope.showAuthentication  = function(){
		var authPopup = $ionicPopup.show({
			template: ' This page required login',
			title: "Authorization",
			subTitle: "Authorization is required",
			scope: $scope,
			buttons: [
				{text:"Cancel",onTap: function(e){
					$state.go("salasar.home");
				}},
			],
		}).then(function(form){
		},function(err){
		},function(msg){
		});
	};
	
	// set default parameter http
	var http_params = {};
	
	// set HTTP Header 
	var http_header = {
		headers: {
		},
		params: http_params
	};
	var targetQuery = ""; //default param
	var raplaceWithQuery = "";
	//fix url Mini Cabs
	targetQuery = "json=cabs"; //default param
	raplaceWithQuery = "json=cabs";
	
	
	// TODO: minicarCtrl --|-- $scope.splitArray
	$scope.splitArray = function(items,cols,maxItem) {
		var newItems = [];
		if(maxItem == 0){
			maxItem = items.length;
		}
		if(items){
			for (var i=0; i < maxItem; i+=cols) {
				newItems.push(items.slice(i, i+cols));
			}
		}
		return newItems;
	}
	// TODO: minicarCtrl --|-- $scope.addToVirtual
	$scope.addToDbVirtual = function(newItem){
		var is_already_exist = false ;
		// animation loading 
		$ionicLoading.show();
		var virtual_items = []; 
		localforage.getItem("cabs_cart", function(err,dbVirtual){
			if(dbVirtual === null){
				virtual_items = [];
			}else{
				try{
					var last_items = JSON.parse(dbVirtual); 
				}catch(e){
					var last_items = [];
				}
				for(var z=0;z<last_items.length;z++){
					virtual_items.push(last_items[z]);
					if(newItem.id ==  last_items[z].id){
						is_already_exist = true;
					}
				}
			}
		}).then(function(value){
			if(is_already_exist === false){
				newItem["_qty"]=1;
				newItem["_sum"]=newItem.none;
				virtual_items.push(newItem);
			}
			localforage.setItem("cabs_cart",JSON.stringify(virtual_items));
			$timeout(function(){
				$ionicLoading.hide();
			},200);
		}).catch(function(err){
			virtual_items = [];
			virtual_items.push(newItem);
			localforage.setItem("cabs_cart",JSON.stringify(virtual_items));
			$timeout(function(){
				$ionicLoading.hide();
			},200);
		})
	};
	
	$scope.gmapOptions = {options: { scrollwheel: false }};
	
	var fetch_per_scroll = 1;
	// animation loading 
	$ionicLoading.show();
	
	
	// TODO: minicarCtrl --|-- $scope.fetchURL
	$scope.fetchURL = "https://salasarbalaji.000webhostapp.com/rest-api.php?json=cabs&type=Minivans";
	// TODO: minicarCtrl --|-- $scope.fetchURLp
	$scope.fetchURLp = "https://salasarbalaji.000webhostapp.com/rest-api.php?json=cabs&type=Minivans&callback=JSON_CALLBACK";
	// TODO: minicarCtrl --|-- $scope.hashURL
	$scope.hashURL = md5.createHash( $scope.fetchURL.replace(targetQuery,raplaceWithQuery));
	
	
	$scope.noMoreItemsAvailable = false; //readmore status
	var lastPush = 0;
	var data_cabss = [];
	
	localforage.getItem("data_cabss_" + $scope.hashURL, function(err, get_cabss){
		if(get_cabss === null){
			data_cabss =[];
		}else{
			data_cabss = JSON.parse(get_cabss);
			$scope.data_cabss =JSON.parse( get_cabss);
			$scope.cabss = [];
			for(lastPush = 0; lastPush < 5; lastPush++) {
				if (angular.isObject(data_cabss[lastPush])){
					$scope.cabss.push(data_cabss[lastPush]);
				};
			}
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
			},200);
		}
	}).then(function(value){
	}).catch(function (err){
	})
	if(data_cabss === null ){
		data_cabss =[];
	}
	if(data_cabss.length === 0 ){
		$timeout(function() {
			var url_request = $scope.fetchURL.replace(targetQuery,raplaceWithQuery);
			// overwrite HTTP Header 
			http_header = {
				headers: {
				},
				params: http_params
			};
			// TODO: minicarCtrl --|-- $http.get
			console.log("%cRetrieving JSON: %c" + url_request,"color:blue;font-size:18px","color:red;font-size:18px");
			$http.get(url_request,http_header).then(function(response) {
				data_cabss = response.data;
				console.log("%cSuccessfully","color:blue;font-size:18px");
				console.dir(data_cabss);
				$scope.data_cabss = response.data;
				// TODO: minicarCtrl --|---------- set:localforage
				localforage.setItem("data_cabss_" + $scope.hashURL, JSON.stringify(data_cabss));
				$scope.cabss = [];
				for(lastPush = 0; lastPush < 5; lastPush++) {
					if (angular.isObject(data_cabss[lastPush])){
						$scope.cabss.push(data_cabss[lastPush]);
					};
				}
			},function(response) {
			
				$timeout(function() {
					var url_request = $scope.fetchURLp.replace(targetQuery,raplaceWithQuery);
					// overwrite HTTP Header 
					http_header = {
						headers: {
						},
						params: http_params
					};
					console.log("%cRetrieving again: %c" + url_request,"color:blue;font-size:18px","color:red;font-size:18px");
					// TODO: minicarCtrl --|------ $http.jsonp
					$http.jsonp(url_request,http_header).success(function(data){
						data_cabss = data;
						$scope.data_cabss = data;
						$ionicLoading.hide();
						// TODO: minicarCtrl --|---------- set:localforage
						localforage.setItem("data_cabss_" + $scope.hashURL,JSON.stringify(data_cabss));
						controller_by_user();
						$scope.cabss = [];
						for(lastPush = 0; lastPush < 5; lastPush++) {
							if (angular.isObject(data_cabss[lastPush])){
								$scope.cabss.push(data_cabss[lastPush]);
							};
						}
					}).error(function(data){
					if(response.status ===401){
						// TODO: minicarCtrl --|------------ error:Unauthorized
						$scope.showAuthentication();
					}else{
						// TODO: minicarCtrl --|------------ error:Message
						var data = { statusText:response.statusText, status:response.status };
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
						$timeout(function() {
							alertPopup.close();
						}, 2000);
					}
					});
				}, 200);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
			}, 200);
		});
	
		}, 200);
	}
	
	
	// TODO: minicarCtrl --|-- $scope.doRefresh
	$scope.doRefresh = function(){
		var url_request = $scope.fetchURL.replace(targetQuery,raplaceWithQuery);
		// retry retrieving data
		// overwrite http_header 
		http_header = {
			headers: {
			},
			params: http_params
		};
		// TODO: minicarCtrl --|------ $http.get
		$http.get(url_request,http_header).then(function(response) {
			data_cabss = response.data;
			$scope.data_cabss = response.data;
			// TODO: minicarCtrl --|---------- set:localforage
			localforage.setItem("data_cabss_" + $scope.hashURL,JSON.stringify(data_cabss));
			$scope.cabss = [];
			for(lastPush = 0; lastPush < 5; lastPush++) {
				if (angular.isObject(data_cabss[lastPush])){
					$scope.cabss.push(data_cabss[lastPush]);
				};
			}
		},function(response){
			
		// retrieving data with jsonp
			$timeout(function() {
			var url_request =$scope.fetchURLp.replace(targetQuery,raplaceWithQuery);
				// overwrite http_header 
				http_header = {
					headers: {
					},
					params: http_params
				};
				// TODO: minicarCtrl --|---------- $http.jsonp
				$http.jsonp(url_request,http_header).success(function(data){
					data_cabss = data;
					$scope.data_cabss = data;
					$ionicLoading.hide();
					controller_by_user();
					// TODO: minicarCtrl --|---------- set:localforage
					localforage.setItem("data_cabss_"+ $scope.hashURL,JSON.stringify(data_cabss));
					$scope.cabss = [];
					for(lastPush = 0; lastPush < 5; lastPush++) {
						if (angular.isObject(data_cabss[lastPush])){
							$scope.cabss.push(data_cabss[lastPush]);
						};
					}
				}).error(function(resp){
					if(response.status ===401){
					// TODO: minicarCtrl --|------------ error:Unauthorized
					$scope.showAuthentication();
					}else{
						// TODO: minicarCtrl --|------------ error:Message
						var data = { statusText:response.statusText, status:response.status };
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
					};
				});
			}, 200);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
			}, 500);
		});
	
	};
	if (data_cabss === null){
		data_cabss = [];
	};
	// animation readmore
	var fetchItems = function() {
		for(var z=0;z<fetch_per_scroll;z++){
			if (angular.isObject(data_cabss[lastPush])){
				$scope.cabss.push(data_cabss[lastPush]);
				lastPush++;
			}else{;
				$scope.noMoreItemsAvailable = true;
			}
		}
		$scope.$broadcast("scroll.infiniteScrollComplete");
	};
	
	// event readmore
	$scope.onInfinite = function() {
		$timeout(fetchItems, 500);
	};
	
	// create animation fade slide in right (ionic-material)
	$scope.fireEvent = function(){
		ionicMaterialMotion.ripple();
		ionicMaterialInk.displayEffect();
	};
	// TODO: minicarCtrl --|-- $scope.showAuthentication
	$scope.showAuthentication  = function(){
		var authPopup = $ionicPopup.show({
			template: ' This page required login',
			title: "Authorization",
			subTitle: "Authorization is required",
			scope: $scope,
			buttons: [
				{text:"Cancel",onTap: function(e){
					$state.go("salasar.home");
				}},
			],
		}).then(function(form){
		},function(err){
		},function(msg){
		});
	};
	
	// set default parameter http
	var http_params = {};
	
	// set HTTP Header 
	var http_header = {
		headers: {
		},
		params: http_params
	};
	var targetQuery = ""; //default param
	var raplaceWithQuery = "";
	//fix url Mini Cabs
	targetQuery = "json=cabs"; //default param
	raplaceWithQuery = "json=cabs";
	
	
	// TODO: minicarCtrl --|-- $scope.splitArray
	$scope.splitArray = function(items,cols,maxItem) {
		var newItems = [];
		if(maxItem == 0){
			maxItem = items.length;
		}
		if(items){
			for (var i=0; i < maxItem; i+=cols) {
				newItems.push(items.slice(i, i+cols));
			}
		}
		return newItems;
	}
	$scope.gmapOptions = {options: { scrollwheel: false }};
	
	var fetch_per_scroll = 1;
	// animation loading 
	$ionicLoading.show();
	
	
	// TODO: minicarCtrl --|-- $scope.fetchURL
	$scope.fetchURL = "https://salasarbalaji.000webhostapp.com/rest-api.php?json=cabs";
	// TODO: minicarCtrl --|-- $scope.fetchURLp
	$scope.fetchURLp = "https://salasarbalaji.000webhostapp.com/rest-api.php?json=cabs&callback=JSON_CALLBACK";
	// TODO: minicarCtrl --|-- $scope.hashURL
	$scope.hashURL = md5.createHash( $scope.fetchURL.replace(targetQuery,raplaceWithQuery));
	
	
	$scope.noMoreItemsAvailable = false; //readmore status
	var lastPush = 0;
	var data_minicabss = [];
	
	localforage.getItem("data_minicabss_" + $scope.hashURL, function(err, get_minicabss){
		if(get_minicabss === null){
			data_minicabss =[];
		}else{
			data_minicabss = JSON.parse(get_minicabss);
			$scope.data_minicabss =JSON.parse( get_minicabss);
			$scope.minicabss = [];
			for(lastPush = 0; lastPush < 50; lastPush++) {
				if (angular.isObject(data_minicabss[lastPush])){
					$scope.minicabss.push(data_minicabss[lastPush]);
				};
			}
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
			},200);
		}
	}).then(function(value){
	}).catch(function (err){
	})
	if(data_minicabss === null ){
		data_minicabss =[];
	}
	if(data_minicabss.length === 0 ){
		$timeout(function() {
			var url_request = $scope.fetchURL.replace(targetQuery,raplaceWithQuery);
			// overwrite HTTP Header 
			http_header = {
				headers: {
				},
				params: http_params
			};
			// TODO: minicarCtrl --|-- $http.get
			console.log("%cRetrieving JSON: %c" + url_request,"color:blue;font-size:18px","color:red;font-size:18px");
			$http.get(url_request,http_header).then(function(response) {
				data_minicabss = response.data;
				console.log("%cSuccessfully","color:blue;font-size:18px");
				console.dir(data_minicabss);
				$scope.data_minicabss = response.data;
				// TODO: minicarCtrl --|---------- set:localforage
				localforage.setItem("data_minicabss_" + $scope.hashURL, JSON.stringify(data_minicabss));
				$scope.minicabss = [];
				for(lastPush = 0; lastPush < 50; lastPush++) {
					if (angular.isObject(data_minicabss[lastPush])){
						$scope.minicabss.push(data_minicabss[lastPush]);
					};
				}
			},function(response) {
			
				$timeout(function() {
					var url_request = $scope.fetchURLp.replace(targetQuery,raplaceWithQuery);
					// overwrite HTTP Header 
					http_header = {
						headers: {
						},
						params: http_params
					};
					console.log("%cRetrieving again: %c" + url_request,"color:blue;font-size:18px","color:red;font-size:18px");
					// TODO: minicarCtrl --|------ $http.jsonp
					$http.jsonp(url_request,http_header).success(function(data){
						data_minicabss = data;
						$scope.data_minicabss = data;
						$ionicLoading.hide();
						// TODO: minicarCtrl --|---------- set:localforage
						localforage.setItem("data_minicabss_" + $scope.hashURL,JSON.stringify(data_minicabss));
						controller_by_user();
						$scope.minicabss = [];
						for(lastPush = 0; lastPush < 50; lastPush++) {
							if (angular.isObject(data_minicabss[lastPush])){
								$scope.minicabss.push(data_minicabss[lastPush]);
							};
						}
					}).error(function(data){
					if(response.status ===401){
						// TODO: minicarCtrl --|------------ error:Unauthorized
						$scope.showAuthentication();
					}else{
						// TODO: minicarCtrl --|------------ error:Message
						var data = { statusText:response.statusText, status:response.status };
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
						$timeout(function() {
							alertPopup.close();
						}, 2000);
					}
					});
				}, 200);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
			}, 200);
		});
	
		}, 200);
	}
	
	
	// TODO: minicarCtrl --|-- $scope.doRefresh
	$scope.doRefresh = function(){
		var url_request = $scope.fetchURL.replace(targetQuery,raplaceWithQuery);
		// retry retrieving data
		// overwrite http_header 
		http_header = {
			headers: {
			},
			params: http_params
		};
		// TODO: minicarCtrl --|------ $http.get
		$http.get(url_request,http_header).then(function(response) {
			data_minicabss = response.data;
			$scope.data_minicabss = response.data;
			// TODO: minicarCtrl --|---------- set:localforage
			localforage.setItem("data_minicabss_" + $scope.hashURL,JSON.stringify(data_minicabss));
			$scope.minicabss = [];
			for(lastPush = 0; lastPush < 50; lastPush++) {
				if (angular.isObject(data_minicabss[lastPush])){
					$scope.minicabss.push(data_minicabss[lastPush]);
				};
			}
		},function(response){
			
		// retrieving data with jsonp
			$timeout(function() {
			var url_request =$scope.fetchURLp.replace(targetQuery,raplaceWithQuery);
				// overwrite http_header 
				http_header = {
					headers: {
					},
					params: http_params
				};
				// TODO: minicarCtrl --|---------- $http.jsonp
				$http.jsonp(url_request,http_header).success(function(data){
					data_minicabss = data;
					$scope.data_minicabss = data;
					$ionicLoading.hide();
					controller_by_user();
					// TODO: minicarCtrl --|---------- set:localforage
					localforage.setItem("data_minicabss_"+ $scope.hashURL,JSON.stringify(data_minicabss));
					$scope.minicabss = [];
					for(lastPush = 0; lastPush < 50; lastPush++) {
						if (angular.isObject(data_minicabss[lastPush])){
							$scope.minicabss.push(data_minicabss[lastPush]);
						};
					}
				}).error(function(resp){
					if(response.status ===401){
					// TODO: minicarCtrl --|------------ error:Unauthorized
					$scope.showAuthentication();
					}else{
						// TODO: minicarCtrl --|------------ error:Message
						var data = { statusText:response.statusText, status:response.status };
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
					};
				});
			}, 200);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
			}, 500);
		});
	
	};
	if (data_minicabss === null){
		data_minicabss = [];
	};
	// animation readmore
	var fetchItems = function() {
		for(var z=0;z<fetch_per_scroll;z++){
			if (angular.isObject(data_minicabss[lastPush])){
				$scope.minicabss.push(data_minicabss[lastPush]);
				lastPush++;
			}else{;
				$scope.noMoreItemsAvailable = true;
			}
		}
		$scope.$broadcast("scroll.infiniteScrollComplete");
	};
	
	// event readmore
	$scope.onInfinite = function() {
		$timeout(fetchItems, 500);
	};
	
	// create animation fade slide in right (ionic-material)
	$scope.fireEvent = function(){
		ionicMaterialMotion.ripple();
		ionicMaterialInk.displayEffect();
	};
	// code 

	// TODO: minicarCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
//debug: all data
//console.log(data_cabss);
$ionicConfig.backButton.text("");
			
		} catch(e){
			console.log("%cerror: %cPage: `minicar` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: privacy_policyCtrl --|-- 
.controller("privacy_policyCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: privacy_policyCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "page_builder" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: privacy_policyCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: privacy_policyCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: privacy_policyCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: privacy_policyCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: privacy_policyCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: privacy_policyCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// code 

	// TODO: privacy_policyCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
			
		} catch(e){
			console.log("%cerror: %cPage: `privacy_policy` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: productsCtrl --|-- 
.controller("productsCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: productsCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "table (products)" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: productsCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: productsCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: productsCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: productsCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: productsCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: productsCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// TODO: productsCtrl --|-- $scope.showAuthentication
	$scope.showAuthentication  = function(){
		var authPopup = $ionicPopup.show({
			template: ' This page required login',
			title: "Authorization",
			subTitle: "Authorization is required",
			scope: $scope,
			buttons: [
				{text:"Cancel",onTap: function(e){
					$state.go("salasar.home");
				}},
			],
		}).then(function(form){
		},function(err){
		},function(msg){
		});
	};
	
	// set default parameter http
	var http_params = {};
	
	// set HTTP Header 
	var http_header = {
		headers: {
		},
		params: http_params
	};
	var targetQuery = ""; //default param
	var raplaceWithQuery = "";
	//fix url Products
	targetQuery = "brand_id=1"; //default param
	raplaceWithQuery = "brand_id=1";
		
	$scope.first_param = {};
	$scope.first_param.brand_id = "1";
	if(typeof $stateParams.brand_id !== 'undefined'){
		raplaceWithQuery = "brand_id=" + $stateParams.brand_id;
		$scope.first_param.brand_id = $stateParams.brand_id;
	}
	if(typeof $rootScope.productsQueryParam !== "undefined"){
		raplaceWithQuery = "brand_id=" +  $rootScope.productsQueryParam ;
	}
	if($scope.first_param.brand_id=="-1"){
		$scope.first_param.brand_id = "";
	}

	
	// TODO: productsCtrl --|-- $scope.splitArray
	$scope.splitArray = function(items,cols,maxItem) {
		var newItems = [];
		if(maxItem == 0){
			maxItem = items.length;
		}
		if(items){
			for (var i=0; i < maxItem; i+=cols) {
				newItems.push(items.slice(i, i+cols));
			}
		}
		return newItems;
	}
	// TODO: productsCtrl --|-- $scope.addToVirtual
	$scope.addToDbVirtual = function(newItem){
		var is_already_exist = false ;
		// animation loading 
		$ionicLoading.show();
		var virtual_items = []; 
		localforage.getItem("products_cart", function(err,dbVirtual){
			if(dbVirtual === null){
				virtual_items = [];
			}else{
				try{
					var last_items = JSON.parse(dbVirtual); 
				}catch(e){
					var last_items = [];
				}
				for(var z=0;z<last_items.length;z++){
					virtual_items.push(last_items[z]);
					if(newItem.brand_id ==  last_items[z].brand_id){
						is_already_exist = true;
					}
				}
			}
		}).then(function(value){
			if(is_already_exist === false){
				newItem["_qty"]=1;
				newItem["_sum"]=newItem.price;
				virtual_items.push(newItem);
			}
			localforage.setItem("products_cart",JSON.stringify(virtual_items));
			$timeout(function(){
				$ionicLoading.hide();
			},200);
		}).catch(function(err){
			virtual_items = [];
			virtual_items.push(newItem);
			localforage.setItem("products_cart",JSON.stringify(virtual_items));
			$timeout(function(){
				$ionicLoading.hide();
			},200);
		})
	};
	
	$scope.gmapOptions = {options: { scrollwheel: false }};
	
	var fetch_per_scroll = 4;
	// animation loading 
	$ionicLoading.show();
	
	
	// TODO: productsCtrl --|-- $scope.fetchURL
	$scope.fetchURL = "https://salasarbalaji.000webhostapp.com/rest-api.php?brand_id=1&json=products";
	// TODO: productsCtrl --|-- $scope.fetchURLp
	$scope.fetchURLp = "https://salasarbalaji.000webhostapp.com/rest-api.php?brand_id=1&json=products&callback=JSON_CALLBACK";
	// TODO: productsCtrl --|-- $scope.hashURL
	$scope.hashURL = md5.createHash( $scope.fetchURL.replace(targetQuery,raplaceWithQuery));
	
	
	$scope.noMoreItemsAvailable = false; //readmore status
	var lastPush = 0;
	var data_productss = [];
	
	localforage.getItem("data_productss_" + $scope.hashURL, function(err, get_productss){
		if(get_productss === null){
			data_productss =[];
		}else{
			data_productss = JSON.parse(get_productss);
			$scope.data_productss =JSON.parse( get_productss);
			$timeout(function() {
				$ionicLoading.hide();
				if(data_productss){
					$scope.chunked_productss = $scope.splitArray(data_productss, $rootScope.grid128,0);
				}
				controller_by_user();
			},200);
		}
	}).then(function(value){
	}).catch(function (err){
	})
	if(data_productss === null ){
		data_productss =[];
	}
	if(data_productss.length === 0 ){
		$timeout(function() {
			var url_request = $scope.fetchURL.replace(targetQuery,raplaceWithQuery);
			// overwrite HTTP Header 
			http_header = {
				headers: {
				},
				params: http_params
			};
			// TODO: productsCtrl --|-- $http.get
			console.log("%cRetrieving JSON: %c" + url_request,"color:blue;font-size:18px","color:red;font-size:18px");
			$http.get(url_request,http_header).then(function(response) {
				data_productss = response.data;
				console.log("%cSuccessfully","color:blue;font-size:18px");
				console.dir(data_productss);
				$scope.data_productss = response.data;
				// TODO: productsCtrl --|---------- set:localforage
				localforage.setItem("data_productss_" + $scope.hashURL, JSON.stringify(data_productss));
			},function(response) {
			
				$timeout(function() {
					var url_request = $scope.fetchURLp.replace(targetQuery,raplaceWithQuery);
					// overwrite HTTP Header 
					http_header = {
						headers: {
						},
						params: http_params
					};
					console.log("%cRetrieving again: %c" + url_request,"color:blue;font-size:18px","color:red;font-size:18px");
					// TODO: productsCtrl --|------ $http.jsonp
					$http.jsonp(url_request,http_header).success(function(data){
						data_productss = data;
						$scope.data_productss = data;
						$ionicLoading.hide();
						// TODO: productsCtrl --|---------- set:localforage
						localforage.setItem("data_productss_" + $scope.hashURL,JSON.stringify(data_productss));
						controller_by_user();
					}).error(function(data){
					if(response.status ===401){
						// TODO: productsCtrl --|------------ error:Unauthorized
						$scope.showAuthentication();
					}else{
						// TODO: productsCtrl --|------------ error:Message
						var data = { statusText:response.statusText, status:response.status };
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
						$timeout(function() {
							alertPopup.close();
						}, 2000);
					}
					});
				}, 200);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
				$scope.chunked_productss = $scope.splitArray(data_productss, $rootScope.grid128,0);
			}, 200);
		});
	
		}, 200);
	}
	
	
	// TODO: productsCtrl --|-- $scope.doRefresh
	$scope.doRefresh = function(){
		var url_request = $scope.fetchURL.replace(targetQuery,raplaceWithQuery);
		// retry retrieving data
		// overwrite http_header 
		http_header = {
			headers: {
			},
			params: http_params
		};
		// TODO: productsCtrl --|------ $http.get
		$http.get(url_request,http_header).then(function(response) {
			data_productss = response.data;
			$scope.data_productss = response.data;
			// TODO: productsCtrl --|---------- set:localforage
			localforage.setItem("data_productss_" + $scope.hashURL,JSON.stringify(data_productss));
		},function(response){
			
		// retrieving data with jsonp
			$timeout(function() {
			var url_request =$scope.fetchURLp.replace(targetQuery,raplaceWithQuery);
				// overwrite http_header 
				http_header = {
					headers: {
					},
					params: http_params
				};
				// TODO: productsCtrl --|---------- $http.jsonp
				$http.jsonp(url_request,http_header).success(function(data){
					data_productss = data;
					$scope.data_productss = data;
					$ionicLoading.hide();
					controller_by_user();
					// TODO: productsCtrl --|---------- set:localforage
					localforage.setItem("data_productss_"+ $scope.hashURL,JSON.stringify(data_productss));
				}).error(function(resp){
					if(response.status ===401){
					// TODO: productsCtrl --|------------ error:Unauthorized
					$scope.showAuthentication();
					}else{
						// TODO: productsCtrl --|------------ error:Message
						var data = { statusText:response.statusText, status:response.status };
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
					};
				});
			}, 200);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
				if(data_productss){
					$scope.chunked_productss = $scope.splitArray(data_productss, $rootScope.grid128,0);
				}
			}, 500);
		});
	
	};
	// code 

	// TODO: productsCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
//debug: all data
//console.log(data_productss);
$ionicConfig.backButton.text("");
			
		} catch(e){
			console.log("%cerror: %cPage: `products` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: products_cartCtrl --|-- 
.controller("products_cartCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: products_cartCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "table (products)" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: products_cartCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: products_cartCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: products_cartCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: products_cartCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: products_cartCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: products_cartCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// TODO: products_cartCtrl --|-- Database for Cart/Bookmark
	// TODO: products_cartCtrl --|-- $scope.loadDbVirtualCabs
	 
	$scope.loadDbVirtualCabs = function(){
		$ionicLoading.show();
		//products_cart
		$scope.cabs_cart = []; 
		$scope.cabs_cost = 0;
		localforage.getItem("cabs_cart", function(err,dbVirtual){
			if(dbVirtual === null){
				$scope.cabs_cart = []; 
			}else{
				try{
					$scope.cabs_cart = JSON.parse(dbVirtual); 
		var total_cost = 0;
		angular.forEach($scope.cabs_cart, function(item, key) {
			var cost = item._qty * item.none;
			total_cost += cost;
		});
		$scope.cabs_cost = total_cost;
				}catch (e){
					$scope.cabs_cart = []; 
				}
			}
		}).then(function(value){
			$timeout(function(){
				$ionicLoading.hide();
			},200);
		}).catch(function(err){
			console.log(err);
			$timeout(function(){
				$ionicLoading.hide();
			},200);
		});
	};
	$scope.$on("$ionicView.enter", function (){
		$scope.loadDbVirtualCabs();
	});
	// TODO: products_cartCtrl --|-- $scope.clearDbVirtualCabs
	$scope.clearDbVirtualCabs = function(){
		localforage.setItem("cabs_cart",[]);
		$timeout(function(){
			$scope.loadDbVirtualCabs();
		},200);
	};
	// TODO: products_cartCtrl --|-- $scope.removeDbVirtualCabs
	$scope.removeDbVirtualCabs = function(itemId){
		var virtual_items = [];
		var last_items = $scope.cabs_cart;
		for(var z=0;z<last_items.length;z++){
			if(itemId!==last_items[z].id){
				virtual_items.push(last_items[z]);
			}
		}
		localforage.setItem("cabs_cart",JSON.stringify(virtual_items));
		$timeout(function(){
			$scope.loadDbVirtualCabs();
		},200);
	};
	// TODO: products_cartCtrl --|-- $scope.updateDbVirtual();
	$scope.updateDbVirtual = function(){
		var virtual_items = [];
		var total_cost = 0;
		angular.forEach($scope.cabs_cart, function(item, key) {
			var cost = item._qty * item.none;
			item._sum = cost;
			this.push(item);
			total_cost += cost;
		},virtual_items);
		localforage.setItem("cabs_cart",JSON.stringify(virtual_items));
		$scope.cabs_cost = total_cost;
	};
	
	// TODO: products_cartCtrl --|-- Database for Cart/Bookmark
	// TODO: products_cartCtrl --|-- $scope.loadDbVirtualHotels
	 
	$scope.loadDbVirtualHotels = function(){
		$ionicLoading.show();
		//products_cart
		$scope.hotels_cart = []; 
		$scope.hotels_cost = 0;
		localforage.getItem("hotels_cart", function(err,dbVirtual){
			if(dbVirtual === null){
				$scope.hotels_cart = []; 
			}else{
				try{
					$scope.hotels_cart = JSON.parse(dbVirtual); 
		var total_cost = 0;
		angular.forEach($scope.hotels_cart, function(item, key) {
			var cost = item._qty * item.price;
			total_cost += cost;
		});
		$scope.hotels_cost = total_cost;
				}catch (e){
					$scope.hotels_cart = []; 
				}
			}
		}).then(function(value){
			$timeout(function(){
				$ionicLoading.hide();
			},200);
		}).catch(function(err){
			console.log(err);
			$timeout(function(){
				$ionicLoading.hide();
			},200);
		});
	};
	$scope.$on("$ionicView.enter", function (){
		$scope.loadDbVirtualHotels();
	});
	// TODO: products_cartCtrl --|-- $scope.clearDbVirtualHotels
	$scope.clearDbVirtualHotels = function(){
		localforage.setItem("hotels_cart",[]);
		$timeout(function(){
			$scope.loadDbVirtualHotels();
		},200);
	};
	// TODO: products_cartCtrl --|-- $scope.removeDbVirtualHotels
	$scope.removeDbVirtualHotels = function(itemId){
		var virtual_items = [];
		var last_items = $scope.hotels_cart;
		for(var z=0;z<last_items.length;z++){
			if(itemId!==last_items[z].id){
				virtual_items.push(last_items[z]);
			}
		}
		localforage.setItem("hotels_cart",JSON.stringify(virtual_items));
		$timeout(function(){
			$scope.loadDbVirtualHotels();
		},200);
	};
	// TODO: products_cartCtrl --|-- $scope.updateDbVirtual();
	$scope.updateDbVirtual = function(){
		var virtual_items = [];
		var total_cost = 0;
		angular.forEach($scope.hotels_cart, function(item, key) {
			var cost = item._qty * item.price;
			item._sum = cost;
			this.push(item);
			total_cost += cost;
		},virtual_items);
		localforage.setItem("hotels_cart",JSON.stringify(virtual_items));
		$scope.hotels_cost = total_cost;
	};
	
	// TODO: products_cartCtrl --|-- Database for Cart/Bookmark
	// TODO: products_cartCtrl --|-- $scope.loadDbVirtualProducts
	 
	$scope.loadDbVirtualProducts = function(){
		$ionicLoading.show();
		//products_cart
		$scope.products_cart = []; 
		$scope.products_cost = 0;
		localforage.getItem("products_cart", function(err,dbVirtual){
			if(dbVirtual === null){
				$scope.products_cart = []; 
			}else{
				try{
					$scope.products_cart = JSON.parse(dbVirtual); 
		var total_cost = 0;
		angular.forEach($scope.products_cart, function(item, key) {
			var cost = item._qty * item.price;
			total_cost += cost;
		});
		$scope.products_cost = total_cost;
				}catch (e){
					$scope.products_cart = []; 
				}
			}
		}).then(function(value){
			$timeout(function(){
				$ionicLoading.hide();
			},200);
		}).catch(function(err){
			console.log(err);
			$timeout(function(){
				$ionicLoading.hide();
			},200);
		});
	};
	$scope.$on("$ionicView.enter", function (){
		$scope.loadDbVirtualProducts();
	});
	// TODO: products_cartCtrl --|-- $scope.clearDbVirtualProducts
	$scope.clearDbVirtualProducts = function(){
		localforage.setItem("products_cart",[]);
		$timeout(function(){
			$scope.loadDbVirtualProducts();
		},200);
	};
	// TODO: products_cartCtrl --|-- $scope.removeDbVirtualProducts
	$scope.removeDbVirtualProducts = function(itemId){
		var virtual_items = [];
		var last_items = $scope.products_cart;
		for(var z=0;z<last_items.length;z++){
			if(itemId!==last_items[z].id){
				virtual_items.push(last_items[z]);
			}
		}
		localforage.setItem("products_cart",JSON.stringify(virtual_items));
		$timeout(function(){
			$scope.loadDbVirtualProducts();
		},200);
	};
	// TODO: products_cartCtrl --|-- $scope.updateDbVirtual();
	$scope.updateDbVirtual = function(){
		var virtual_items = [];
		var total_cost = 0;
		angular.forEach($scope.products_cart, function(item, key) {
			var cost = item._qty * item.price;
			item._sum = cost;
			this.push(item);
			total_cost += cost;
		},virtual_items);
		localforage.setItem("products_cart",JSON.stringify(virtual_items));
		$scope.products_cost = total_cost;
	};
	
	// code 

	// TODO: products_cartCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
	$scope.gotoCheckout = function(){
		alert("you must create payment gateway manual");
	}
			
		} catch(e){
			console.log("%cerror: %cPage: `products_cart` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: products_singlesCtrl --|-- 
.controller("products_singlesCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: products_singlesCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "table (products)" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: products_singlesCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: products_singlesCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: products_singlesCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: products_singlesCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: products_singlesCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: products_singlesCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// TODO: products_singlesCtrl --|-- $scope.showAuthentication
	$scope.showAuthentication  = function(){
		var authPopup = $ionicPopup.show({
			template: ' This page required login',
			title: "Authorization",
			subTitle: "Authorization is required",
			scope: $scope,
			buttons: [
				{text:"Cancel",onTap: function(e){
					$state.go("salasar.home");
				}},
			],
		}).then(function(form){
		},function(err){
		},function(msg){
		});
	};
	// TODO: products_singlesCtrl --|-- $scope.addToVirtual(); //data single
	$scope.addToDbVirtual = function(newItem){
		var is_already_exist = false ;
		// animation loading 
		$ionicLoading.show();
		var virtual_items = []; 
		localforage.getItem("products_cart", function(err,dbVirtual){
			if(dbVirtual === null){
				virtual_items = [];
			}else{
				try{
					var last_items = JSON.parse(dbVirtual); 
				}catch(e){
					var last_items = [];
				}
				for(var z=0;z<last_items.length;z++){
					virtual_items.push(last_items[z]);
					if(newItem.id ==  last_items[z].id){
						is_already_exist = true;
					}
				}
			}
		}).then(function(value){
			if(is_already_exist === false){
				newItem["_qty"]=1;
				newItem["_sum"]=newItem.price;
				virtual_items.push(newItem);
			}
			localforage.setItem("products_cart",JSON.stringify(virtual_items));
			$timeout(function(){
				$ionicLoading.hide();
			},200);
		}).catch(function(err){
			virtual_items = [];
			virtual_items.push(newItem);
			localforage.setItem("products_cart",JSON.stringify(virtual_items));
			$timeout(function(){
				$ionicLoading.hide();
			},200);
		})
	};
	
	
	// set default parameter http
	var http_params = {};

	// set HTTP Header 
	var http_header = {
		headers: {
		},
		params: http_params
	};
	// animation loading 
	$ionicLoading.show();
	
	// Retrieving data
	var itemID = $stateParams.id;
	// TODO: products_singlesCtrl --|-- $scope.fetchURL
	$scope.fetchURL = "https://salasarbalaji.000webhostapp.com/rest-api.php?json=products&id=" + itemID;
	// TODO: products_singlesCtrl --|-- $scope.fetchURLp
	$scope.fetchURLp = "https://salasarbalaji.000webhostapp.com/rest-api.php?json=products&id=" + itemID + "&callback=JSON_CALLBACK";
	// TODO: products_singlesCtrl --|-- $scope.hashURL
	$scope.hashURL = md5.createHash($scope.fetchURL);
	
	var current_item = [];
	localforage.getItem("data_products_single_" + $scope.hashURL, function(err, get_datas){
		if(get_datas === null){
			current_item = [];
		}else{
			if(get_datas !== null){
				current_item = JSON.parse(get_datas);
				$timeout(function(){
					$ionicLoading.hide();
					$scope.products = current_item ;
					controller_by_user();
				}, 500);
			};
		};
	}).then(function(value){
	}).catch(function (err){
	})
	if( current_item.length === 0 ){
		var itemID = $stateParams.id;
		var current_item = [];
	
		// set HTTP Header 
		http_header = {
			headers: {
			},
			params: http_params
		};
		// TODO: products_singlesCtrl --|-- $http.get
		$http.get($scope.fetchURL,http_header).then(function(response) {
			// Get data single
			var datas = response.data;
			// TODO: products_singlesCtrl --|---------- set:localforage
			localforage.setItem("data_products_single_" + $scope.hashURL,JSON.stringify(datas));
			current_item = datas ;
		},function(data) {
					// Error message
					var alertPopup = $ionicPopup.alert({
						title: "Network Error" + " (" + data.status + ")",
						template: "An error occurred while collecting data.",
					});
					$timeout(function() {
						alertPopup.close();
					}, 2000);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				$scope.products = current_item ;
				controller_by_user();
			}, 500);
		});
	}
	
	
		// TODO: products_singlesCtrl --|-- $scope.doRefresh
	$scope.doRefresh = function(){
		// Retrieving data
		var itemID = $stateParams.id;
		var current_item = [];
		// overwrite http_header 
		http_header = {
			headers: {
			},
			params: http_params
		};
		// TODO: products_singlesCtrl --|------ $http.get
		$http.get($scope.fetchURL,http_header).then(function(response) {
			// Get data single
			var datas = response.data;
			// TODO: products_singlesCtrl --|---------- set:localforage
			localforage.setItem("data_products_single_" + $scope.hashURL,JSON.stringify(datas));
			current_item = datas ;
		},function(data) {
			// Error message
		// TODO: products_singlesCtrl --|---------- $http.jsonp
				$http.jsonp($scope.fetchURLp,http_header).success(function(response){
					// Get data single
					var datas = response;
			// TODO: products_singlesCtrl --|---------- set:localforage
			localforage.setItem("data_products_single_" + $scope.hashURL,JSON.stringify(datas));
					current_item = datas ;
						$scope.$broadcast("scroll.refreshComplete");
						// event done, hidden animation loading
						$timeout(function() {
							$ionicLoading.hide();
							$scope.products = current_item ;
							controller_by_user();
						}, 500);
					}).error(function(resp){
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
					});
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				$scope.products = current_item ;
				controller_by_user();
			}, 500);
		});
	};
	// code 

	// TODO: products_singlesCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
$ionicConfig.backButton.text("");			
		} catch(e){
			console.log("%cerror: %cPage: `products_singles` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: profileCtrl --|-- 
.controller("profileCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: profileCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "page_builder" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: profileCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: profileCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: profileCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: profileCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: profileCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: profileCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// code 

	// TODO: profileCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			

    
$scope.current_user = {} ;
$scope.$on("$ionicView.afterEnter", function (){  
    
    $ionicLoading.show({
		template: '<div class="loader"><svg class="circular"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div>'
	});
    
    $http.get("https://salasarbalaji.000webhostapp.com/rest-api.php?json=me").then(function(resp){
        if(resp.data.data.status == 401){
       	    $ionicHistory.nextViewOptions({
        		disableAnimate: true,
        		disableBack: true
        	});
			$state.go("salasar.form_login");
        }else{
            $scope.current_user = resp.data.me ;
        }
        
        
        
    },function(resp){   
        
		$timeout(function() {
			$ionicLoading.hide();
		}, 500); 
        
		var alertPopup = $ionicPopup.alert({
			title: "Ops, error!",
			template: "Problem with crossdomain or Invalid URL.",
		});
        
        alertPopup.then(function(res){
              $state.go("salasar.form_login");
        });
        
    }).finally(function(){   
		$timeout(function() {
			$ionicLoading.hide();
		}, 500);            
    });      
    
});   

$scope.updateUser = function(){
  		
    // animation loading 
    $ionicLoading.show({
    	template: '<div class="loader"><svg class="circular"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div>'
    });
	
		var $messages, $title = null;
		$http({
				method:"POST",
				url: "https://salasarbalaji.000webhostapp.com/rest-api.php?json=submit&form=me",
				data: $httpParamSerializer($scope.current_user),   
				headers: {"Content-Type":"application/x-www-form-urlencoded"}  
			})
			.then(function(response) {
				$messages = response.data.message;
				$title = response.data.title;
			},function(response){
				$messages = response.statusText;
				$title = response.status;
			}).finally(function(){
				// event done, hidden animation loading
				$timeout(function() {
					$ionicLoading.hide();
					if($messages !== null){
						// message
    					var alertPopup = $ionicPopup.alert({
    						title: $title,
    						template: $messages,
    					});
					}
			     }, 500);
		  });
}
 
    			
		} catch(e){
			console.log("%cerror: %cPage: `profile` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: road_mapCtrl --|-- 
.controller("road_mapCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: road_mapCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "table (road_map)" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: road_mapCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: road_mapCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: road_mapCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: road_mapCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: road_mapCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: road_mapCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// TODO: road_mapCtrl --|-- $scope.showAuthentication
	$scope.showAuthentication  = function(){
		var authPopup = $ionicPopup.show({
			template: ' This page required login',
			title: "Authorization",
			subTitle: "Authorization is required",
			scope: $scope,
			buttons: [
				{text:"Cancel",onTap: function(e){
					$state.go("salasar.home");
				}},
			],
		}).then(function(form){
		},function(err){
		},function(msg){
		});
	};
	
	// set default parameter http
	var http_params = {};
	
	// set HTTP Header 
	var http_header = {
		headers: {
		},
		params: http_params
	};
	var targetQuery = ""; //default param
	var raplaceWithQuery = "";
	
	// TODO: road_mapCtrl --|-- $scope.splitArray
	$scope.splitArray = function(items,cols,maxItem) {
		var newItems = [];
		if(maxItem == 0){
			maxItem = items.length;
		}
		if(items){
			for (var i=0; i < maxItem; i+=cols) {
				newItems.push(items.slice(i, i+cols));
			}
		}
		return newItems;
	}
	$scope.gmapOptions = {options: { scrollwheel: false }};
	
	var fetch_per_scroll = 1;
	// animation loading 
	$ionicLoading.show();
	
	
	// TODO: road_mapCtrl --|-- $scope.fetchURL
	$scope.fetchURL = "";
	// TODO: road_mapCtrl --|-- $scope.fetchURLp
	$scope.fetchURLp = "?callback=JSON_CALLBACK";
	// TODO: road_mapCtrl --|-- $scope.hashURL
	$scope.hashURL = md5.createHash( $scope.fetchURL.replace(targetQuery,raplaceWithQuery));
	
	
	$scope.noMoreItemsAvailable = false; //readmore status
	var lastPush = 0;
	var data_road_maps = [];
	
	localforage.getItem("data_road_maps_" + $scope.hashURL, function(err, get_road_maps){
		if(get_road_maps === null){
			data_road_maps =[];
		}else{
			data_road_maps = JSON.parse(get_road_maps);
			$scope.data_road_maps =JSON.parse( get_road_maps);
			$scope.road_maps = [];
			for(lastPush = 0; lastPush < 50; lastPush++) {
				if (angular.isObject(data_road_maps[lastPush])){
					$scope.road_maps.push(data_road_maps[lastPush]);
				};
			}
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
			},200);
		}
	}).then(function(value){
	}).catch(function (err){
	})
	if(data_road_maps === null ){
		data_road_maps =[];
	}
	if(data_road_maps.length === 0 ){
		$timeout(function() {
			var url_request = $scope.fetchURL.replace(targetQuery,raplaceWithQuery);
			// overwrite HTTP Header 
			http_header = {
				headers: {
				},
				params: http_params
			};
			// TODO: road_mapCtrl --|-- $http.get
			console.log("%cRetrieving JSON: %c" + url_request,"color:blue;font-size:18px","color:red;font-size:18px");
			$http.get(url_request,http_header).then(function(response) {
				data_road_maps = response.data;
				console.log("%cSuccessfully","color:blue;font-size:18px");
				console.dir(data_road_maps);
				$scope.data_road_maps = response.data;
				// TODO: road_mapCtrl --|---------- set:localforage
				localforage.setItem("data_road_maps_" + $scope.hashURL, JSON.stringify(data_road_maps));
				$scope.road_maps = [];
				for(lastPush = 0; lastPush < 50; lastPush++) {
					if (angular.isObject(data_road_maps[lastPush])){
						$scope.road_maps.push(data_road_maps[lastPush]);
					};
				}
			},function(response) {
			
				$timeout(function() {
					var url_request = $scope.fetchURLp.replace(targetQuery,raplaceWithQuery);
					// overwrite HTTP Header 
					http_header = {
						headers: {
						},
						params: http_params
					};
					console.log("%cRetrieving again: %c" + url_request,"color:blue;font-size:18px","color:red;font-size:18px");
					// TODO: road_mapCtrl --|------ $http.jsonp
					$http.jsonp(url_request,http_header).success(function(data){
						data_road_maps = data;
						$scope.data_road_maps = data;
						$ionicLoading.hide();
						// TODO: road_mapCtrl --|---------- set:localforage
						localforage.setItem("data_road_maps_" + $scope.hashURL,JSON.stringify(data_road_maps));
						controller_by_user();
						$scope.road_maps = [];
						for(lastPush = 0; lastPush < 50; lastPush++) {
							if (angular.isObject(data_road_maps[lastPush])){
								$scope.road_maps.push(data_road_maps[lastPush]);
							};
						}
					}).error(function(data){
					if(response.status ===401){
						// TODO: road_mapCtrl --|------------ error:Unauthorized
						$scope.showAuthentication();
					}else{
						// TODO: road_mapCtrl --|------------ error:Message
						var data = { statusText:response.statusText, status:response.status };
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
						$timeout(function() {
							alertPopup.close();
						}, 2000);
					}
					});
				}, 200);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
			}, 200);
		});
	
		}, 200);
	}
	
	
	// TODO: road_mapCtrl --|-- $scope.doRefresh
	$scope.doRefresh = function(){
		var url_request = $scope.fetchURL.replace(targetQuery,raplaceWithQuery);
		// retry retrieving data
		// overwrite http_header 
		http_header = {
			headers: {
			},
			params: http_params
		};
		// TODO: road_mapCtrl --|------ $http.get
		$http.get(url_request,http_header).then(function(response) {
			data_road_maps = response.data;
			$scope.data_road_maps = response.data;
			// TODO: road_mapCtrl --|---------- set:localforage
			localforage.setItem("data_road_maps_" + $scope.hashURL,JSON.stringify(data_road_maps));
			$scope.road_maps = [];
			for(lastPush = 0; lastPush < 50; lastPush++) {
				if (angular.isObject(data_road_maps[lastPush])){
					$scope.road_maps.push(data_road_maps[lastPush]);
				};
			}
		},function(response){
			
		// retrieving data with jsonp
			$timeout(function() {
			var url_request =$scope.fetchURLp.replace(targetQuery,raplaceWithQuery);
				// overwrite http_header 
				http_header = {
					headers: {
					},
					params: http_params
				};
				// TODO: road_mapCtrl --|---------- $http.jsonp
				$http.jsonp(url_request,http_header).success(function(data){
					data_road_maps = data;
					$scope.data_road_maps = data;
					$ionicLoading.hide();
					controller_by_user();
					// TODO: road_mapCtrl --|---------- set:localforage
					localforage.setItem("data_road_maps_"+ $scope.hashURL,JSON.stringify(data_road_maps));
					$scope.road_maps = [];
					for(lastPush = 0; lastPush < 50; lastPush++) {
						if (angular.isObject(data_road_maps[lastPush])){
							$scope.road_maps.push(data_road_maps[lastPush]);
						};
					}
				}).error(function(resp){
					if(response.status ===401){
					// TODO: road_mapCtrl --|------------ error:Unauthorized
					$scope.showAuthentication();
					}else{
						// TODO: road_mapCtrl --|------------ error:Message
						var data = { statusText:response.statusText, status:response.status };
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
					};
				});
			}, 200);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
			}, 500);
		});
	
	};
	if (data_road_maps === null){
		data_road_maps = [];
	};
	// animation readmore
	var fetchItems = function() {
		for(var z=0;z<fetch_per_scroll;z++){
			if (angular.isObject(data_road_maps[lastPush])){
				$scope.road_maps.push(data_road_maps[lastPush]);
				lastPush++;
			}else{;
				$scope.noMoreItemsAvailable = true;
			}
		}
		$scope.$broadcast("scroll.infiniteScrollComplete");
	};
	
	// event readmore
	$scope.onInfinite = function() {
		$timeout(fetchItems, 500);
	};
	
	// code 

	// TODO: road_mapCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			

$scope.road_map = [];
$ionicModal.fromTemplateUrl("road_map-single.html",{scope: $scope,animation:"slide-in-up"}).then(function(modal){
    $scope.modal = modal;
});
$scope.openModal = function() {
    $scope.road_map = [];
    var itemID = this.id;
	for (var i = 0; i < data_road_maps.length; i++) {
		if((data_road_maps[i].id ===  parseInt(itemID)) || (data_road_maps[i].id === itemID.toString())) {
			$scope.road_map = data_road_maps[i] ;
		}
	}    
    $scope.modal.show();
};
$scope.closeModal = function() {
    $scope.modal.hide();
};
$scope.$on("$destroy", function() {
    $scope.modal.remove();
});
//debug: all data
//console.log(data_road_maps);
$ionicConfig.backButton.text("");
			
		} catch(e){
			console.log("%cerror: %cPage: `road_map` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	controller_by_user();
})

// TODO: share_appCtrl --|-- 
.controller("share_appCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: share_appCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "menu" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: share_appCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: share_appCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: share_appCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: share_appCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: share_appCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: share_appCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// code 

	// TODO: share_appCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
			
		} catch(e){
			console.log("%cerror: %cPage: `share_app` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: shoppingCtrl --|-- 
.controller("shoppingCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: shoppingCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "table (brands)" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: shoppingCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: shoppingCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: shoppingCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: shoppingCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: shoppingCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: shoppingCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// TODO: shoppingCtrl --|-- $scope.showAuthentication
	$scope.showAuthentication  = function(){
		var authPopup = $ionicPopup.show({
			template: ' This page required login',
			title: "Authorization",
			subTitle: "Authorization is required",
			scope: $scope,
			buttons: [
				{text:"Cancel",onTap: function(e){
					$state.go("salasar.home");
				}},
			],
		}).then(function(form){
		},function(err){
		},function(msg){
		});
	};
	
	// set default parameter http
	var http_params = {};
	
	// set HTTP Header 
	var http_header = {
		headers: {
		},
		params: http_params
	};
	var targetQuery = ""; //default param
	var raplaceWithQuery = "";
	//fix url Shopping
	targetQuery = "json=brands"; //default param
	raplaceWithQuery = "json=brands";
	
	
	// TODO: shoppingCtrl --|-- $scope.splitArray
	$scope.splitArray = function(items,cols,maxItem) {
		var newItems = [];
		if(maxItem == 0){
			maxItem = items.length;
		}
		if(items){
			for (var i=0; i < maxItem; i+=cols) {
				newItems.push(items.slice(i, i+cols));
			}
		}
		return newItems;
	}
	$scope.gmapOptions = {options: { scrollwheel: false }};
	
	var fetch_per_scroll = 4;
	// animation loading 
	$ionicLoading.show();
	
	
	// TODO: shoppingCtrl --|-- $scope.fetchURL
	$scope.fetchURL = "https://salasarbalaji.000webhostapp.com/rest-api.php?json=brands";
	// TODO: shoppingCtrl --|-- $scope.fetchURLp
	$scope.fetchURLp = "https://salasarbalaji.000webhostapp.com/rest-api.php?json=brands&callback=JSON_CALLBACK";
	// TODO: shoppingCtrl --|-- $scope.hashURL
	$scope.hashURL = md5.createHash( $scope.fetchURL.replace(targetQuery,raplaceWithQuery));
	
	
	$scope.noMoreItemsAvailable = false; //readmore status
	var lastPush = 0;
	var data_brandss = [];
	
	localforage.getItem("data_brandss_" + $scope.hashURL, function(err, get_brandss){
		if(get_brandss === null){
			data_brandss =[];
		}else{
			data_brandss = JSON.parse(get_brandss);
			$scope.data_brandss =JSON.parse( get_brandss);
			$scope.brandss = [];
			for(lastPush = 0; lastPush < 6; lastPush++) {
				if (angular.isObject(data_brandss[lastPush])){
					$scope.brandss.push(data_brandss[lastPush]);
				};
			}
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
			},200);
		}
	}).then(function(value){
	}).catch(function (err){
	})
	if(data_brandss === null ){
		data_brandss =[];
	}
	if(data_brandss.length === 0 ){
		$timeout(function() {
			var url_request = $scope.fetchURL.replace(targetQuery,raplaceWithQuery);
			// overwrite HTTP Header 
			http_header = {
				headers: {
				},
				params: http_params
			};
			// TODO: shoppingCtrl --|-- $http.get
			console.log("%cRetrieving JSON: %c" + url_request,"color:blue;font-size:18px","color:red;font-size:18px");
			$http.get(url_request,http_header).then(function(response) {
				data_brandss = response.data;
				console.log("%cSuccessfully","color:blue;font-size:18px");
				console.dir(data_brandss);
				$scope.data_brandss = response.data;
				// TODO: shoppingCtrl --|---------- set:localforage
				localforage.setItem("data_brandss_" + $scope.hashURL, JSON.stringify(data_brandss));
				$scope.brandss = [];
				for(lastPush = 0; lastPush < 6; lastPush++) {
					if (angular.isObject(data_brandss[lastPush])){
						$scope.brandss.push(data_brandss[lastPush]);
					};
				}
			},function(response) {
			
				$timeout(function() {
					var url_request = $scope.fetchURLp.replace(targetQuery,raplaceWithQuery);
					// overwrite HTTP Header 
					http_header = {
						headers: {
						},
						params: http_params
					};
					console.log("%cRetrieving again: %c" + url_request,"color:blue;font-size:18px","color:red;font-size:18px");
					// TODO: shoppingCtrl --|------ $http.jsonp
					$http.jsonp(url_request,http_header).success(function(data){
						data_brandss = data;
						$scope.data_brandss = data;
						$ionicLoading.hide();
						// TODO: shoppingCtrl --|---------- set:localforage
						localforage.setItem("data_brandss_" + $scope.hashURL,JSON.stringify(data_brandss));
						controller_by_user();
						$scope.brandss = [];
						for(lastPush = 0; lastPush < 6; lastPush++) {
							if (angular.isObject(data_brandss[lastPush])){
								$scope.brandss.push(data_brandss[lastPush]);
							};
						}
					}).error(function(data){
					if(response.status ===401){
						// TODO: shoppingCtrl --|------------ error:Unauthorized
						$scope.showAuthentication();
					}else{
						// TODO: shoppingCtrl --|------------ error:Message
						var data = { statusText:response.statusText, status:response.status };
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
						$timeout(function() {
							alertPopup.close();
						}, 2000);
					}
					});
				}, 200);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
			}, 200);
		});
	
		}, 200);
	}
	
	
	// TODO: shoppingCtrl --|-- $scope.doRefresh
	$scope.doRefresh = function(){
		var url_request = $scope.fetchURL.replace(targetQuery,raplaceWithQuery);
		// retry retrieving data
		// overwrite http_header 
		http_header = {
			headers: {
			},
			params: http_params
		};
		// TODO: shoppingCtrl --|------ $http.get
		$http.get(url_request,http_header).then(function(response) {
			data_brandss = response.data;
			$scope.data_brandss = response.data;
			// TODO: shoppingCtrl --|---------- set:localforage
			localforage.setItem("data_brandss_" + $scope.hashURL,JSON.stringify(data_brandss));
			$scope.brandss = [];
			for(lastPush = 0; lastPush < 6; lastPush++) {
				if (angular.isObject(data_brandss[lastPush])){
					$scope.brandss.push(data_brandss[lastPush]);
				};
			}
		},function(response){
			
		// retrieving data with jsonp
			$timeout(function() {
			var url_request =$scope.fetchURLp.replace(targetQuery,raplaceWithQuery);
				// overwrite http_header 
				http_header = {
					headers: {
					},
					params: http_params
				};
				// TODO: shoppingCtrl --|---------- $http.jsonp
				$http.jsonp(url_request,http_header).success(function(data){
					data_brandss = data;
					$scope.data_brandss = data;
					$ionicLoading.hide();
					controller_by_user();
					// TODO: shoppingCtrl --|---------- set:localforage
					localforage.setItem("data_brandss_"+ $scope.hashURL,JSON.stringify(data_brandss));
					$scope.brandss = [];
					for(lastPush = 0; lastPush < 6; lastPush++) {
						if (angular.isObject(data_brandss[lastPush])){
							$scope.brandss.push(data_brandss[lastPush]);
						};
					}
				}).error(function(resp){
					if(response.status ===401){
					// TODO: shoppingCtrl --|------------ error:Unauthorized
					$scope.showAuthentication();
					}else{
						// TODO: shoppingCtrl --|------------ error:Message
						var data = { statusText:response.statusText, status:response.status };
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
					};
				});
			}, 200);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
			}, 500);
		});
	
	};
	if (data_brandss === null){
		data_brandss = [];
	};
	// animation readmore
	var fetchItems = function() {
		for(var z=0;z<fetch_per_scroll;z++){
			if (angular.isObject(data_brandss[lastPush])){
				$scope.brandss.push(data_brandss[lastPush]);
				lastPush++;
			}else{;
				$scope.noMoreItemsAvailable = true;
			}
		}
		$scope.$broadcast("scroll.infiniteScrollComplete");
	};
	
	// event readmore
	$scope.onInfinite = function() {
		$timeout(fetchItems, 500);
	};
	
	// create animation fade slide in right (ionic-material)
	$scope.fireEvent = function(){
		ionicMaterialMotion.fadeSlideIn();
		ionicMaterialInk.displayEffect();
	};
	// code 

	// TODO: shoppingCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
//debug: all data
//console.log(data_brandss);
$ionicConfig.backButton.text("");
			
		} catch(e){
			console.log("%cerror: %cPage: `shopping` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: slide_tab_menuCtrl --|-- 
.controller("slide_tab_menuCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: slide_tab_menuCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "menu" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: slide_tab_menuCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: slide_tab_menuCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: slide_tab_menuCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: slide_tab_menuCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: slide_tab_menuCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: slide_tab_menuCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// code 

	// TODO: slide_tab_menuCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
			
		} catch(e){
			console.log("%cerror: %cPage: `slide_tab_menu` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})

// TODO: suv_cabsCtrl --|-- 
.controller("suv_cabsCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: suv_cabsCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "table (cabs)" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: suv_cabsCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: suv_cabsCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: suv_cabsCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: suv_cabsCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: suv_cabsCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: suv_cabsCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// code 

	// TODO: suv_cabsCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
//debug: all data
//console.log(data_cabss);
$ionicConfig.backButton.text("");
			
		} catch(e){
			console.log("%cerror: %cPage: `suv_cabs` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})


// TODO: tourCtrl --|-- 
.controller("tourCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: tourCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "table (packages)" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: tourCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: tourCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: tourCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: tourCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: tourCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: tourCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// TODO: tourCtrl --|-- $scope.showAuthentication
	$scope.showAuthentication  = function(){
		var authPopup = $ionicPopup.show({
			template: ' This page required login',
			title: "Authorization",
			subTitle: "Authorization is required",
			scope: $scope,
			buttons: [
				{text:"Cancel",onTap: function(e){
					$state.go("salasar.home");
				}},
			],
		}).then(function(form){
		},function(err){
		},function(msg){
		});
	};
	
	// set default parameter http
	var http_params = {};
	
	// set HTTP Header 
	var http_header = {
		headers: {
		},
		params: http_params
	};
	var targetQuery = ""; //default param
	var raplaceWithQuery = "";
	//fix url Tour
	targetQuery = "json=packages"; //default param
	raplaceWithQuery = "json=packages";
	
	
	// TODO: tourCtrl --|-- $scope.splitArray
	$scope.splitArray = function(items,cols,maxItem) {
		var newItems = [];
		if(maxItem == 0){
			maxItem = items.length;
		}
		if(items){
			for (var i=0; i < maxItem; i+=cols) {
				newItems.push(items.slice(i, i+cols));
			}
		}
		return newItems;
	}
	// TODO: tourCtrl --|-- $scope.addToVirtual
	$scope.addToDbVirtual = function(newItem){
		var is_already_exist = false ;
		// animation loading 
		$ionicLoading.show();
		var virtual_items = []; 
		localforage.getItem("packages_cart", function(err,dbVirtual){
			if(dbVirtual === null){
				virtual_items = [];
			}else{
				try{
					var last_items = JSON.parse(dbVirtual); 
				}catch(e){
					var last_items = [];
				}
				for(var z=0;z<last_items.length;z++){
					virtual_items.push(last_items[z]);
					if(newItem.id ==  last_items[z].id){
						is_already_exist = true;
					}
				}
			}
		}).then(function(value){
			if(is_already_exist === false){
				newItem["_qty"]=1;
				newItem["_sum"]=newItem.price;
				virtual_items.push(newItem);
			}
			localforage.setItem("packages_cart",JSON.stringify(virtual_items));
			$timeout(function(){
				$ionicLoading.hide();
			},200);
		}).catch(function(err){
			virtual_items = [];
			virtual_items.push(newItem);
			localforage.setItem("packages_cart",JSON.stringify(virtual_items));
			$timeout(function(){
				$ionicLoading.hide();
			},200);
		})
	};
	
	$scope.gmapOptions = {options: { scrollwheel: false }};
	
	var fetch_per_scroll = 3;
	// animation loading 
	$ionicLoading.show();
	
	
	// TODO: tourCtrl --|-- $scope.fetchURL
	$scope.fetchURL = "https://salasarbalaji.000webhostapp.com/rest-api.php?json=packages";
	// TODO: tourCtrl --|-- $scope.fetchURLp
	$scope.fetchURLp = "https://salasarbalaji.000webhostapp.com/rest-api.php?json=packages&callback=JSON_CALLBACK";
	// TODO: tourCtrl --|-- $scope.hashURL
	$scope.hashURL = md5.createHash( $scope.fetchURL.replace(targetQuery,raplaceWithQuery));
	
	
	$scope.noMoreItemsAvailable = false; //readmore status
	var lastPush = 0;
	var data_packagess = [];
	
	localforage.getItem("data_packagess_" + $scope.hashURL, function(err, get_packagess){
		if(get_packagess === null){
			data_packagess =[];
		}else{
			data_packagess = JSON.parse(get_packagess);
			$scope.data_packagess =JSON.parse( get_packagess);
			$scope.packagess = [];
			for(lastPush = 0; lastPush < 10; lastPush++) {
				if (angular.isObject(data_packagess[lastPush])){
					$scope.packagess.push(data_packagess[lastPush]);
				};
			}
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
			},200);
		}
	}).then(function(value){
	}).catch(function (err){
	})
	if(data_packagess === null ){
		data_packagess =[];
	}
	if(data_packagess.length === 0 ){
		$timeout(function() {
			var url_request = $scope.fetchURL.replace(targetQuery,raplaceWithQuery);
			// overwrite HTTP Header 
			http_header = {
				headers: {
				},
				params: http_params
			};
			// TODO: tourCtrl --|-- $http.get
			console.log("%cRetrieving JSON: %c" + url_request,"color:blue;font-size:18px","color:red;font-size:18px");
			$http.get(url_request,http_header).then(function(response) {
				data_packagess = response.data;
				console.log("%cSuccessfully","color:blue;font-size:18px");
				console.dir(data_packagess);
				$scope.data_packagess = response.data;
				// TODO: tourCtrl --|---------- set:localforage
				localforage.setItem("data_packagess_" + $scope.hashURL, JSON.stringify(data_packagess));
				$scope.packagess = [];
				for(lastPush = 0; lastPush < 10; lastPush++) {
					if (angular.isObject(data_packagess[lastPush])){
						$scope.packagess.push(data_packagess[lastPush]);
					};
				}
			},function(response) {
			
				$timeout(function() {
					var url_request = $scope.fetchURLp.replace(targetQuery,raplaceWithQuery);
					// overwrite HTTP Header 
					http_header = {
						headers: {
						},
						params: http_params
					};
					console.log("%cRetrieving again: %c" + url_request,"color:blue;font-size:18px","color:red;font-size:18px");
					// TODO: tourCtrl --|------ $http.jsonp
					$http.jsonp(url_request,http_header).success(function(data){
						data_packagess = data;
						$scope.data_packagess = data;
						$ionicLoading.hide();
						// TODO: tourCtrl --|---------- set:localforage
						localforage.setItem("data_packagess_" + $scope.hashURL,JSON.stringify(data_packagess));
						controller_by_user();
						$scope.packagess = [];
						for(lastPush = 0; lastPush < 10; lastPush++) {
							if (angular.isObject(data_packagess[lastPush])){
								$scope.packagess.push(data_packagess[lastPush]);
							};
						}
					}).error(function(data){
					if(response.status ===401){
						// TODO: tourCtrl --|------------ error:Unauthorized
						$scope.showAuthentication();
					}else{
						// TODO: tourCtrl --|------------ error:Message
						var data = { statusText:response.statusText, status:response.status };
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
						$timeout(function() {
							alertPopup.close();
						}, 2000);
					}
					});
				}, 200);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
			}, 200);
		});
	
		}, 200);
	}
	
	
	// TODO: tourCtrl --|-- $scope.doRefresh
	$scope.doRefresh = function(){
		var url_request = $scope.fetchURL.replace(targetQuery,raplaceWithQuery);
		// retry retrieving data
		// overwrite http_header 
		http_header = {
			headers: {
			},
			params: http_params
		};
		// TODO: tourCtrl --|------ $http.get
		$http.get(url_request,http_header).then(function(response) {
			data_packagess = response.data;
			$scope.data_packagess = response.data;
			// TODO: tourCtrl --|---------- set:localforage
			localforage.setItem("data_packagess_" + $scope.hashURL,JSON.stringify(data_packagess));
			$scope.packagess = [];
			for(lastPush = 0; lastPush < 10; lastPush++) {
				if (angular.isObject(data_packagess[lastPush])){
					$scope.packagess.push(data_packagess[lastPush]);
				};
			}
		},function(response){
			
		// retrieving data with jsonp
			$timeout(function() {
			var url_request =$scope.fetchURLp.replace(targetQuery,raplaceWithQuery);
				// overwrite http_header 
				http_header = {
					headers: {
					},
					params: http_params
				};
				// TODO: tourCtrl --|---------- $http.jsonp
				$http.jsonp(url_request,http_header).success(function(data){
					data_packagess = data;
					$scope.data_packagess = data;
					$ionicLoading.hide();
					controller_by_user();
					// TODO: tourCtrl --|---------- set:localforage
					localforage.setItem("data_packagess_"+ $scope.hashURL,JSON.stringify(data_packagess));
					$scope.packagess = [];
					for(lastPush = 0; lastPush < 10; lastPush++) {
						if (angular.isObject(data_packagess[lastPush])){
							$scope.packagess.push(data_packagess[lastPush]);
						};
					}
				}).error(function(resp){
					if(response.status ===401){
					// TODO: tourCtrl --|------------ error:Unauthorized
					$scope.showAuthentication();
					}else{
						// TODO: tourCtrl --|------------ error:Message
						var data = { statusText:response.statusText, status:response.status };
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
					};
				});
			}, 200);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				controller_by_user();
			}, 500);
		});
	
	};
	if (data_packagess === null){
		data_packagess = [];
	};
	// animation readmore
	var fetchItems = function() {
		for(var z=0;z<fetch_per_scroll;z++){
			if (angular.isObject(data_packagess[lastPush])){
				$scope.packagess.push(data_packagess[lastPush]);
				lastPush++;
			}else{;
				$scope.noMoreItemsAvailable = true;
			}
		}
		$scope.$broadcast("scroll.infiniteScrollComplete");
	};
	
	// event readmore
	$scope.onInfinite = function() {
		$timeout(fetchItems, 500);
	};
	
	// create animation fade slide in right (ionic-material)
	$scope.fireEvent = function(){
		ionicMaterialMotion.fadeSlideInRight();
		ionicMaterialInk.displayEffect();
	};
	// code 

	// TODO: tourCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
//debug: all data
//console.log(data_packagess);
$ionicConfig.backButton.text("");
			
		} catch(e){
			console.log("%cerror: %cPage: `tour` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})




// TODO: user_singlesCtrl --|-- 
.controller("user_singlesCtrl", function($ionicConfig,$scope,$rootScope,$state,$location,$ionicScrollDelegate,$ionicListDelegate,$http,$httpParamSerializer,$stateParams,$timeout,$interval,$ionicLoading,$ionicPopup,$ionicPopover,$ionicActionSheet,$ionicSlideBoxDelegate,$ionicHistory,ionicMaterialInk,ionicMaterialMotion,$window,$ionicModal,base64,md5,$document,$sce,$ionicGesture){
	
	$rootScope.headerExists = true;
	$rootScope.ionWidth = $document[0].body.querySelector(".view-container").offsetWidth || 412;
	$rootScope.grid64 = parseInt($rootScope.ionWidth / 64) ;
	$rootScope.grid80 = parseInt($rootScope.ionWidth / 80) ;
	$rootScope.grid128 = parseInt($rootScope.ionWidth / 128) ;
	$rootScope.grid256 = parseInt($rootScope.ionWidth / 256) ;
	// TODO: user_singlesCtrl --|-- $rootScope.mapEnable
	if(typeof google == "undefined"){
		$rootScope.mapEnable = false;
	}else{
		$rootScope.mapEnable = true;
	}
	$rootScope.last_edit = "table (user)" ;
	$scope.$on("$ionicView.afterEnter", function (){
		var page_id = $state.current.name ;
		$rootScope.page_id = page_id.replace(".","-") ;
	});
	$scope.$on("$ionicView.enter", function (){
		$scope.scrollTop();
	});
	// TODO: user_singlesCtrl --|-- $scope.scrollTop
	$rootScope.scrollTop = function(){
		$timeout(function(){
			$ionicScrollDelegate.$getByHandle("top").scrollTop();
		},100);
	};
	// TODO: user_singlesCtrl --|-- $scope.openURL
	// open external browser 
	$scope.openURL = function($url){
		window.open($url,"_system","location=yes");
	};
	// TODO: user_singlesCtrl --|-- $scope.openAppBrowser
	// open AppBrowser
	$scope.openAppBrowser = function($url){
		var appBrowser = window.open($url,"_blank","hardwareback=Done");
		appBrowser.addEventListener("loadstart",function(){
			appBrowser.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: user_singlesCtrl --|-- $scope.openWebView
	// open WebView
	$scope.openWebView = function($url){
		var appWebview = window.open($url,"_blank","location=no");
		appWebview.addEventListener("loadstart",function(){
			appWebview.insertCSS({
				code: "body{background:#100;color:#fff;font-size:72px;}body:after{content:'loading...';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});
	};
	
	
	// TODO: user_singlesCtrl --|-- $scope.toggleGroup
	$scope.toggleGroup = function(group) {
		if ($scope.isGroupShown(group)) {
			$scope.shownGroup = null;
		} else {
			$scope.shownGroup = group;
		}
	};
	
	$scope.isGroupShown = function(group) {
		return $scope.shownGroup === group;
	};
	
	// TODO: user_singlesCtrl --|-- $scope.redirect
	// redirect
	$scope.redirect = function($url){
		$window.location.href = $url;
	};
	
	// Set Motion
	$timeout(function(){
		ionicMaterialMotion.slideUp({
			selector: ".slide-up"
		});
	}, 300);
	// TODO: user_singlesCtrl --|-- $scope.showAuthentication
	$scope.showAuthentication  = function(){
		var authPopup = $ionicPopup.show({
			template: ' This page required login',
			title: "Authorization",
			subTitle: "Authorization is required",
			scope: $scope,
			buttons: [
				{text:"Cancel",onTap: function(e){
					$state.go("salasar.home");
				}},
			],
		}).then(function(form){
		},function(err){
		},function(msg){
		});
	};
	
	// set default parameter http
	var http_params = {};

	// set HTTP Header 
	var http_header = {
		headers: {
		},
		params: http_params
	};
	// animation loading 
	$ionicLoading.show();
	
	// Retrieving data
	var itemID = $stateParams.id;
	// TODO: user_singlesCtrl --|-- $scope.fetchURL
	$scope.fetchURL = "https://salasarbalaji.000webhostapp.com/rest-api.phpform=user_register&json=submit";
	// TODO: user_singlesCtrl --|-- $scope.fetchURLp
	$scope.fetchURLp = "https://salasarbalaji.000webhostapp.com/rest-api.phpform=user_register&json=submit?callback=JSON_CALLBACK";
	// TODO: user_singlesCtrl --|-- $scope.hashURL
	$scope.hashURL = md5.createHash($scope.fetchURL);
	
	var current_item = [];
	localforage.getItem("data_users_" + $scope.hashURL, function(err, get_datas){
		if(get_datas === null){
			current_item = [];
		}else{
			if(get_datas !== null){
				var datas = JSON.parse(get_datas);
				for (var i = 0; i < datas.length; i++) {
					if((datas[i].id ===  parseInt(itemID)) || (datas[i].id === itemID.toString())) {
						current_item = datas[i] ;
					}
				}
			}
			// event done, hidden animation loading
			$timeout(function(){
				$ionicLoading.hide();
				$scope.user = current_item ;
				controller_by_user();
			}, 100);
		};
	}).then(function(value){
	}).catch(function (err){
	})
	if( current_item.length === 0 ){
		var itemID = $stateParams.id;
		var current_item = [];
	
		// set HTTP Header 
		http_header = {
			headers: {
			},
			params: http_params
		};
		// TODO: user_singlesCtrl --|-- $http.get
		$http.get($scope.fetchURL,http_header).then(function(response) {
			// Get data single
			var datas = response.data;
			// TODO: user_singlesCtrl --|---------- set:localforage
			localforage.setItem("data_users_"+ $scope.hashURL,JSON.stringify(datas));
			for (var i = 0; i < datas.length; i++) {
				if((datas[i].id ===  parseInt(itemID)) || (datas[i].id === itemID.toString())) {
					current_item = datas[i] ;
				}
			}
		},function(data) {
					// Error message
					var alertPopup = $ionicPopup.alert({
						title: "Network Error" + " (" + data.status + ")",
						template: "An error occurred while collecting data.",
					});
					$timeout(function() {
						alertPopup.close();
					}, 2000);
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				$scope.user = current_item ;
				controller_by_user();
			}, 500);
		});
	}
	
	
		// TODO: user_singlesCtrl --|-- $scope.doRefresh
	$scope.doRefresh = function(){
		// Retrieving data
		var itemID = $stateParams.id;
		var current_item = [];
		// overwrite http_header 
		http_header = {
			headers: {
			},
			params: http_params
		};
		// TODO: user_singlesCtrl --|------ $http.get
		$http.get($scope.fetchURL,http_header).then(function(response) {
			// Get data single
			var datas = response.data;
			// TODO: user_singlesCtrl --|---------- set:localforage
			localforage.setItem("data_users_"+ $scope.hashURL,JSON.stringify(datas));
			for (var i = 0; i < datas.length; i++) {
				if((datas[i].id ===  parseInt(itemID)) || (datas[i].id === itemID.toString())) {
					current_item = datas[i] ;
				}
			}
		},function(data) {
			// Error message
		// TODO: user_singlesCtrl --|---------- $http.jsonp
				$http.jsonp($scope.fetchURLp,http_header).success(function(response){
					// Get data single
					var datas = response;
			// TODO: user_singlesCtrl --|---------- set:localforage
			localforage.setItem("data_users_"+ $scope.hashURL,JSON.stringify(datas));
					for (var i = 0; i < datas.length; i++) {
						if((datas[i].id ===  parseInt(itemID)) || (datas[i].id === itemID.toString())) {
							current_item = datas[i] ;
						}
					}
						$scope.$broadcast("scroll.refreshComplete");
						// event done, hidden animation loading
						$timeout(function() {
							$ionicLoading.hide();
							$scope.user = current_item ;
							controller_by_user();
						}, 500);
					}).error(function(resp){
						var alertPopup = $ionicPopup.alert({
							title: "Network Error" + " (" + data.status + ")",
							template: "An error occurred while collecting data.",
						});
					});
		}).finally(function() {
			$scope.$broadcast("scroll.refreshComplete");
			// event done, hidden animation loading
			$timeout(function() {
				$ionicLoading.hide();
				$scope.user = current_item ;
				controller_by_user();
			}, 500);
		});
	};
	// code 

	// TODO: user_singlesCtrl --|-- controller_by_user
	// controller by user 
	function controller_by_user(){
		try {
			
$ionicConfig.backButton.text("");			
		} catch(e){
			console.log("%cerror: %cPage: `user_singles` and field: `Custom Controller`","color:blue;font-size:18px","color:red;font-size:18px");
			console.dir(e);
		}
	}
	$scope.rating = {};
	$scope.rating.max = 5;
	
	// animation ink (ionic-material)
	ionicMaterialInk.displayEffect();
	controller_by_user();
})
