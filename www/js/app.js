angular.module("salasar", ["ngCordova","ionic","ionMdInput","ionic-material","ion-datetime-picker","ionic.rating","utf8-base64","angular-md5","chart.js","ionicLazyLoad","ngMap","salasar.controllers", "salasar.services"])
	.run(function($ionicPlatform,$window,$interval,$timeout,$ionicHistory,$ionicPopup,$state,$rootScope){

		$rootScope.appName = "Salasar" ;
		$rootScope.appLogo = "data/images/icon/welcome_logo-Bazaar.png" ;
		$rootScope.appVersion = "1.0" ;

		$ionicPlatform.ready(function() {
			//required: cordova plugin add ionic-plugin-keyboard --save
			if(window.cordova && window.cordova.plugins.Keyboard) {
				cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
				cordova.plugins.Keyboard.disableScroll(true);
			}

			//required: cordova plugin add cordova-plugin-statusbar --save
			if(window.StatusBar) {
				StatusBar.styleDefault();
			}
			// this will create a banner on startup
			//required: cordova plugin add cordova-plugin-admobpro --save
			if (typeof AdMob !== "undefined"){
				var admobid = {};
				admobid = {
					banner: "ca-app-pub-2818903853781722/6100873450",
					interstitial: "ca-app-pub-2818903853781722/2708423354",
					rewardvideo: "ca-app-pub-2818903853781722/9820626617"
				};
				$timeout(function(){
					
					AdMob.createBanner({
						adId: admobid.banner,
						overlap: false,
						autoShow: true,
						offsetTopBar: false,
						position: AdMob.AD_POSITION.BOTTOM_CENTER,
						bgColor: "black"
					});
					
					AdMob.prepareInterstitial({
						adId: admobid.interstitial,
						autoShow: true,
					});
					
					AdMob.prepareRewardVideoAd({
						adId: admobid.rewardvideo,
						autoShow: true,
					});
					
				}, 1000);;
			}

			localforage.config({
				driver : [localforage.WEBSQL,localforage.INDEXEDDB,localforage.LOCALSTORAGE],
				name : "salasar",
				storeName : "salasar",
				description : "The offline datastore for Salasar app"
			});


			//required: cordova plugin add cordova-plugin-network-information --save
			$interval(function(){
				if ( typeof navigator == "object" && typeof navigator.connection != "undefined"){
					var networkState = navigator.connection.type;
					if (networkState == "none") {
						$window.location = "retry.html";
					}
				}
			}, 5000);

			//required: cordova plugin add onesignal-cordova-plugin --save
			if(window.plugins && window.plugins.OneSignal){
				window.plugins.OneSignal.enableNotificationsWhenActive(true);
				var notificationOpenedCallback = function(jsonData){
					try {
						$interval(function(){
							$window.location = "#/salasar/" + jsonData.notification.payload.additionalData.page ;
						},200);
					} catch(e){
						console.log("onesignal:" + e);
					}
				}
				window.plugins.OneSignal.startInit("88a3751f-f3e5-44a1-8cb9-416ddcd26b41").handleNotificationOpened(notificationOpenedCallback).endInit();
			}    


		});
		$ionicPlatform.registerBackButtonAction(function (e){
			if($ionicHistory.backView()){
				$ionicHistory.goBack();
			}else{
				var confirmPopup = $ionicPopup.confirm({
					title: "Confirm Exit",
					template: "Are you sure you want to exit?"
				});
				confirmPopup.then(function (close){
					if(close){
						ionic.Platform.exitApp();
					}
				});
			}
			e.preventDefault();
			return false;
		},101);
	})


	.filter("to_trusted", ["$sce", function($sce){
		return function(text) {
			return $sce.trustAsHtml(text);
		};
	}])

	.filter("trustUrl", function($sce) {
		return function(url) {
			return $sce.trustAsResourceUrl(url);
		};
	})

	.filter("trustJs", ["$sce", function($sce){
		return function(text) {
			return $sce.trustAsJs(text);
		};
	}])

	.filter("strExplode", function() {
		return function($string,$delimiter) {
			if(!$string.length ) return;
			var $_delimiter = $delimiter || "|";
			return $string.split($_delimiter);
		};
	})

	.filter("strDate", function(){
		return function (input) {
			return new Date(input);
		}
	})
	.filter("strHTML", ["$sce", function($sce){
		return function(text) {
			return $sce.trustAsHtml(text);
		};
	}])
	.filter("strEscape",function(){
		return window.encodeURIComponent;
	})
	.filter("strUnscape", ["$sce", function($sce) {
		var div = document.createElement("div");
		return function(text) {
			div.innerHTML = text;
			return $sce.trustAsHtml(div.textContent);
		};
	}])

	.filter("objLabel", function(){
		return function (obj) {
			var new_item = [];
			angular.forEach(obj, function(child) {
				new_item = [];
				var indeks = 0;
				angular.forEach(child, function(v,l) {
					if (indeks !== 0) {
					new_item.push(l);
				}
				indeks++;
				});
			});
			return new_item;
		}
	})
	.filter("objArray", function(){
		return function (obj) {
			var new_items = [];
			angular.forEach(obj, function(child) {
				var new_item = [];
				var indeks = 0;
				angular.forEach(child, function(v){
						if (indeks !== 0){
							new_item.push(v);
						}
						indeks++;
					});
					new_items.push(new_item);
				});
			return new_items;
		}
	})




.config(function($stateProvider, $urlRouterProvider,$sceDelegateProvider,$httpProvider,$ionicConfigProvider){
	try{
		// Domain Whitelist
		$sceDelegateProvider.resourceUrlWhitelist([
			"self",
			new RegExp('^(http[s]?):\/\/(w{3}.)?youtube\.com/.+$'),
			new RegExp('^(http[s]?):\/\?/salasarbalaji\.org/.+$'),
			new RegExp('^(http[s]?):\/\?/sms.hspsms\.com/.+$'),
		]);
	}catch(err){
		console.log("%cerror: %cdomain whitelist","color:blue;font-size:16px;","color:red;font-size:16px;");
	}
	$stateProvider
	.state("salasar",{
		url: "/salasar",
			abstract: true,
			templateUrl: "templates/salasar-side_menus.html",
			controller: "side_menusCtrl",
	})

	.state("salasar.about_us", {
		url: "/about_us",
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-about_us.html",
						controller: "about_usCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.albums", {
		url: "/albums",
		cache:false,
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-albums.html",
						controller: "albumsCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.articles_singles", {
		url: "/articles_singles/:id",
		cache:false,
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-articles_singles.html",
						controller: "articles_singlesCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.brands", {
		url: "/brands",
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-brands.html",
						controller: "brandsCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.cabs", {
		url: "/cabs",
		cache:false,
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-cabs.html",
						controller: "cabsCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.cabs_cart", {
		url: "/cabs_cart",
		cache:false,
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-cabs_cart.html",
						controller: "cabs_cartCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.cabs_singles", {
		url: "/cabs_singles/:id",
		cache:false,
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-cabs_singles.html",
						controller: "cabs_singlesCtrl"
					},
			"fabButtonUp" : {
						template: '<button id="fab-up-button" ng-click="scrollTop()" class="button button-fab button-fab-bottom-right button-energized-900 spin"><i class="icon ion-arrow-up-a"></i></button>',
						controller: function ($timeout) {
							$timeout(function () {
								document.getElementById("fab-up-button").classList.toggle("on");
							}, 900);
						}
					},
		}
	})

	.state("salasar.category", {
		url: "/category",
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-category.html",
						controller: "categoryCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.contact_us", {
		url: "/contact_us",
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-contact_us.html",
						controller: "contact_usCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.daily_schedule", {
		url: "/daily_schedule",
		cache:false,
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-daily_schedule.html",
						controller: "daily_scheduleCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.dashboard", {
		url: "/dashboard",
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-dashboard.html",
						controller: "dashboardCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.facility", {
		url: "/facility",
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-facility.html",
						controller: "facilityCtrl"
					},
			"fabButtonUp" : {
						template: '<button id="fab-up-button" ng-click="scrollTop()" class="button button-fab button-fab-bottom-right button-energized-900 spin"><i class="icon ion-arrow-up-a"></i></button>',
						controller: function ($timeout) {
							$timeout(function () {
								document.getElementById("fab-up-button").classList.toggle("on");
							}, 900);
						}
					},
		}
	})

	.state("salasar.form_book_pandit", {
		url: "/form_book_pandit",
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-form_book_pandit.html",
						controller: "form_book_panditCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.form_contact_us", {
		url: "/form_contact_us",
		cache:false,
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-form_contact_us.html",
						controller: "form_contact_usCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.form_login", {
		url: "/form_login",
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-form_login.html",
						controller: "form_loginCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})


	.state("salasar.form_otp", {
		url: "/form_otp",
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-form_otp.html",
						controller: "form_otpCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})


	.state("salasar.verify_otp", {
		url: "/verify_otp",
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-verify_otp.html",
						controller: "verify_otpCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.form_order_form", {
		url: "/form_order_form",
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-form_order_form.html",
						controller: "form_order_formCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.form_user", {
		url: "/form_user",
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-form_user.html",
						controller: "form_userCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.form_user_register", {
		url: "/form_user_register",
		cache:false,
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-form_user_register.html",
						controller: "form_user_registerCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.galleries_singles", {
		url: "/galleries_singles/:id",
		cache:false,
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-galleries_singles.html",
						controller: "galleries_singlesCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.gallery", {
		url: "/gallery/:album_id",
		cache:false,
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-gallery.html",
						controller: "galleryCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.history", {
		url: "/history",
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-history.html",
						controller: "historyCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.home", {
		url: "/home",
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-home.html",
						controller: "homeCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.hotel_cats", {
		url: "/hotel_cats",
		cache:false,
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-hotel_cats.html",
						controller: "hotel_catsCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.hotels", {
		url: "/hotels/:hotel_categories_id",
		cache:false,
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-hotels.html",
						controller: "hotelsCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.hotels_cart", {
		url: "/hotels_cart",
		cache:false,
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-hotels_cart.html",
						controller: "hotels_cartCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.hotels_singles", {
		url: "/hotels_singles/:id",
		cache:false,
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-hotels_singles.html",
						controller: "hotels_singlesCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.live_darshan", {
		url: "/live_darshan",
		cache:false,
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-live_darshan.html",
						controller: "live_darshanCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.livedarshan_singles", {
		url: "/livedarshan_singles/:id",
		cache:false,
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-livedarshan_singles.html",
						controller: "livedarshan_singlesCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.mantra_jap", {
		url: "/mantra_jap",
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-mantra_jap.html",
						controller: "mantra_japCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.membership", {
		url: "/membership",
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-membership.html",
						controller: "membershipCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.packages_cart", {
		url: "/packages_cart",
		cache:false,
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-packages_cart.html",
						controller: "packages_cartCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.packages_singles", {
		url: "/packages_singles/:id",
		cache:false,
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-packages_singles.html",
						controller: "packages_singlesCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.pandit_book", {
		url: "/pandit_book",
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-pandit_book.html",
						controller: "pandit_bookCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.pandit_booking", {
		url: "/pandit_booking",
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-pandit_booking.html",
						controller: "pandit_bookingCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.privacy_policy", {
		url: "/privacy_policy",
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-privacy_policy.html",
						controller: "privacy_policyCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.products", {
		url: "/products/:brand_id",
		cache:false,
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-products.html",
						controller: "productsCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.products_cart", {
		url: "/products_cart",
		cache:false,
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-products_cart.html",
						controller: "products_cartCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.products_singles", {
		url: "/products_singles/:id",
		cache:false,
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-products_singles.html",
						controller: "products_singlesCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.profile", {
		url: "/profile",
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-profile.html",
						controller: "profileCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.road_map", {
		url: "/road_map",
		cache:false,
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-road_map.html",
						controller: "road_mapCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.share_app", {
		url: "/share_app",
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-share_app.html",
						controller: "share_appCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.shopping", {
		url: "/shopping",
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-shopping.html",
						controller: "shoppingCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.slide_tab_menu", {
		url: "/slide_tab_menu",
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-slide_tab_menu.html",
						controller: "slide_tab_menuCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.tour", {
		url: "/tour",
		cache:false,
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-tour.html",
						controller: "tourCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.upcoming_festivals", {
		url: "/upcoming_festivals",
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-upcoming_festivals.html",
						controller: "upcoming_festivalsCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.user_register_singles", {
		url: "/user_register_singles/:id",
		cache:false,
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-user_register_singles.html",
						controller: "user_register_singlesCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})

	.state("salasar.user_singles", {
		url: "/user_singles/:id",
		cache:false,
		views: {
			"salasar-side_menus" : {
						templateUrl:"templates/salasar-user_singles.html",
						controller: "user_singlesCtrl"
					},
			"fabButtonUp" : {
						template: '',
					},
		}
	})


// router by user


	$urlRouterProvider.otherwise("/salasar/home");
});
